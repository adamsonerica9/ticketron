package main

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	kafkaconsumer "opencitylabs.it/ticketron/kafkaConsumer"
	"opencitylabs.it/ticketron/kafkaEventHandlers"
	"opencitylabs.it/ticketron/kafkaMessageHandler"
	kafkaproducer "opencitylabs.it/ticketron/kafkaProducer"
	"opencitylabs.it/ticketron/logger"
	"opencitylabs.it/ticketron/sentryutils"
)

func main() {

	logger, err := logger.GetLoggerConfig().Build()
	if err != nil {
		logger = zap.NewNop()
	}
	defer logger.Sync()

	config := config.NewConfig(logger)
	err = sentryutils.SentryInit(config)
	if err != nil {
		logger.Error("sentry.Init: ", zap.Error(err))
		sentry.CaptureException(err)
	}
	ctx := context.Background()

	//kafka application consumer init
	applicationKafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.KafkaConsumer.KafkaServer, config.KafkaConsumer.ApplicationTopic.KafkaTopic, config.KafkaConsumer.ConsumerGroup, config.KafkaConsumer.ApplicationTopic.TopicOffset)
	defer applicationKafkaConsumer.CloseKafkaReader()
	applicationKafkaProducer := kafkaproducer.NewKafkaProducer(logger, config, config.KafkaProducer.KafkaServer, config.KafkaProducer.RetryQueueTopic.KafkaTopic, config.KafkaProducer.RetryQueueTopic.ConsumerGroup)
	defer applicationKafkaProducer.CloseKafkaWriter()

	//kafka users consumer init
	usersKafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.KafkaConsumer.KafkaServer, config.KafkaConsumer.UsersTopic.KafkaTopic, config.KafkaConsumer.ConsumerGroup, config.KafkaConsumer.UsersTopic.TopicOffset)
	defer usersKafkaConsumer.CloseKafkaReader()
	usersKafkaProducer := kafkaproducer.NewKafkaProducer(logger, config, config.KafkaProducer.KafkaServer, config.KafkaProducer.RetryQueueTopic.KafkaTopic, config.KafkaProducer.RetryQueueTopic.ConsumerGroup)
	defer usersKafkaProducer.CloseKafkaWriter()

	// kafka messages consumer init
	messagesKafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.KafkaConsumer.KafkaServer, config.KafkaConsumer.MessagesTopic.KafkaTopic, config.KafkaConsumer.ConsumerGroup, config.KafkaConsumer.MessagesTopic.TopicOffset)
	defer messagesKafkaConsumer.CloseKafkaReader()
	messagesKafkaProducer := kafkaproducer.NewKafkaProducer(logger, config, config.KafkaProducer.KafkaServer, config.KafkaProducer.RetryQueueTopic.KafkaTopic, config.KafkaProducer.RetryQueueTopic.ConsumerGroup)
	defer messagesKafkaProducer.CloseKafkaWriter()

	//kafka czrm-updates consumer init
	czrmUpdatesKafkaConsumer := kafkaconsumer.NewKafkaConsumer(logger, config, config.KafkaConsumer.KafkaServer, config.KafkaConsumer.CzrmUpdatesTopic.KafkaTopic, config.KafkaConsumer.ConsumerGroup, config.KafkaConsumer.CzrmUpdatesTopic.TopicOffset)
	defer czrmUpdatesKafkaConsumer.CloseKafkaReader()
	czrmUpdatesKafkaProducer := kafkaproducer.NewKafkaProducer(logger, config, config.KafkaProducer.KafkaServer, config.KafkaProducer.RetryQueueTopic.KafkaTopic, config.KafkaProducer.RetryQueueTopic.ConsumerGroup)
	defer czrmUpdatesKafkaProducer.CloseKafkaWriter()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	//the following 3 lines handle  application events
	applicationkafkaMessagehandler := kafkaMessageHandler.NewKafkaApplicationMessageHandler(logger)
	applicationEventHandler := kafkaEventHandlers.NewApplicationEventHandler(logger, config)
	go applicationListener(ctx, logger, config, applicationKafkaConsumer, applicationKafkaProducer, applicationkafkaMessagehandler, applicationEventHandler)

	//the following 3 lines handle  Users events
	userskafkaMessagehandler := kafkaMessageHandler.NewKafkaUsersMessageHandler(logger)
	userEventHandler := kafkaEventHandlers.NewUserEventHandler(logger, config)
	go usersListener(ctx, logger, config, usersKafkaConsumer, usersKafkaProducer, userskafkaMessagehandler, userEventHandler)

	//the following 3 lines handle  Messages events
	messageskafkaMessagehandler := kafkaMessageHandler.NewKafkaMessagesMessageHandler(logger)
	messagesEventHandler := kafkaEventHandlers.NewMessageEventHandler(logger, config)
	go messagesListener(ctx, logger, config, messagesKafkaConsumer, messagesKafkaProducer, messageskafkaMessagehandler, messagesEventHandler)

	//the following 3 lines handle czrmUpdates events
	czrmUpdatesKafkaMessageHandler := kafkaMessageHandler.NewkafkaCzrmUpdateMessageHandler(logger)
	czrmUpdatesEventHandler := kafkaEventHandlers.NewCzrmUpdatesEventHandler(logger, config)
	go czrmUpdatesListener(ctx, logger, config, czrmUpdatesKafkaConsumer, czrmUpdatesKafkaProducer, czrmUpdatesKafkaMessageHandler, czrmUpdatesEventHandler)

	<-done
	// TODO rendere timeout configurabile. tenere alto: anche 2-3 minuti. sempre meglio che siano gli altri a chiuderci la porta anziché noi se non siamo costretti .
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	logger.Debug("/n Server gracefully stopped")
}

func applicationListener(ctx context.Context, logger *zap.Logger, conf *config.Config, KafkaConsumer *kafkaconsumer.KafkaConsumer, kafkaProducer *kafkaproducer.KafkaProducer, kafkaMessagehandler kafkaMessageHandler.KafkaApplicationMessageHandler, applicationEventHandler kafkaEventHandlers.IApplicationEventHandler) {
	sentryHub := sentryutils.InitLocalSentryhub()
	applicationEventHandler.SetSentryHub(sentryHub)
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("APPLICATION -> error reading application message from kafka: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("APPLICATION -> error reading application message from kafka"))
			continue
		}

		kafkaStructuredMessage, err := kafkaMessagehandler.GetStructuredMessage(m)
		if err != nil {
			logger.Sugar().Debug("APPLICATION -> error getting the structured application message. key: ", string(m.Key))
			continue
		}
		applicationEventHandler.SetStructuredMessageFromApplicationEvent(kafkaStructuredMessage)
		logger.Sugar().Debug("APPLICATION -> KEY id: ", string(m.Key))
		logger.Sugar().Debug("APPLICATION -> EVENT id: ", kafkaStructuredMessage.EventId)
		err = applicationEventHandler.ProcessMessage()
		if err != nil {
			logger.Debug("APPLICATION -> failed to Process application message: ")
		} else {
			err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
			if err != nil {
				logger.Error("APPLICATION ->failed to commit application messages: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("APPLICATION ->failed to commit application messages"))
			} else {
				logger.Debug("APPLICATION -> application message committed")
			}
		}
		if applicationEventHandler.ShouldPutMessagetoRetryQueue() {
			message := kafkaProducer.BuildMessageWithRetryMetaField(m.Value, conf.KafkaConsumer.ApplicationTopic.KafkaTopic)
			err = kafkaProducer.ProduceMessage(string(m.Key), message, ctx)
			if err != nil {
				logger.Error("APPLICATION -> failed to produce event: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("APPLICATION -> failed to produce event"))
			}
			logger.Debug("APPLICATION -> message produced")
			logger.Sugar().Info("APPLICATION -> failed to process message. eventId: "+kafkaStructuredMessage.EventId+" . added to retry topic: ", conf.KafkaProducer.RetryQueueTopic.KafkaTopic)
		}
	}
}

func usersListener(ctx context.Context, logger *zap.Logger, conf *config.Config, KafkaConsumer *kafkaconsumer.KafkaConsumer, kafkaProducer *kafkaproducer.KafkaProducer, kafkaMessagehandler kafkaMessageHandler.IKafkaUsersMessageHandler, userEventHandler kafkaEventHandlers.IUserEventHandler) {
	sentryHub := sentryutils.InitLocalSentryhub()
	userEventHandler.SetSentryHub(sentryHub)
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("USERS-> error reading user message from kafka: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("USERS-> error reading user message from kafka"))
			continue
		}

		kafkaStructuredMessage, err := kafkaMessagehandler.GetStructuredMessage(m)
		if err != nil {
			logger.Sugar().Debug("USERS -> error getting the structured application message. key: ", string(m.Key))
			continue
		}
		userEventHandler.SetStructuredMessageFromUserEvent(kafkaStructuredMessage)
		logger.Sugar().Debug("USERS -> KEY event id: ", string(m.Key))
		logger.Sugar().Debug("USERS -> EVENT id: ", kafkaStructuredMessage.EventId)
		_ = userEventHandler.ProcessMessage()

		err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
		if err != nil {
			logger.Error("USERS-> failed to commit user messages: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("USERS-> failed to commit user messages"))
		} else {
			logger.Debug("USERS-> message committed")
		}
		if userEventHandler.ShouldPutMessagetoRetryQueue() {
			message := kafkaProducer.BuildMessageWithRetryMetaField(m.Value, conf.KafkaConsumer.UsersTopic.KafkaTopic)
			err = kafkaProducer.ProduceMessage(string(m.Key), message, ctx)
			if err != nil {
				logger.Error("USERS -> failed to produce event: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("USERS -> failed to produce event"))
			}
			logger.Debug("USERS -> message produced")
			logger.Sugar().Info("USERS -> failed to process message. eventId: "+kafkaStructuredMessage.EventId+" . added to retry topic: ", conf.KafkaProducer.RetryQueueTopic.KafkaTopic)
		}

	}
}

func messagesListener(ctx context.Context, logger *zap.Logger, conf *config.Config, KafkaConsumer *kafkaconsumer.KafkaConsumer, kafkaProducer *kafkaproducer.KafkaProducer, kafkaMessagehandler kafkaMessageHandler.IKafkaMessagesMessageHandler, eventHandler kafkaEventHandlers.IMessageEventHandler) {
	sentryHub := sentryutils.InitLocalSentryhub()
	eventHandler.SetSentryHub(sentryHub)
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("MESSAGES -> error reading message from kafka: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("USERS -> failed to produce event"))
			continue
		}

		kafkaStructuredMessage, err := kafkaMessagehandler.GetStructuredMessage(m)
		if err != nil {
			logger.Sugar().Debug("MESSAGES -> error getting the structured message. key: ", string(m.Key))
		}
		eventHandler.SetStructuredMessageFromMessageEvent(kafkaStructuredMessage)
		logger.Sugar().Debug("MESSAGES -> KEY event id: ", string(m.Key))
		logger.Sugar().Debug("MESSAGES -> EVENT id: ", kafkaStructuredMessage.EventId)
		err = eventHandler.ProcessMessage()
		if err != nil {
			logger.Error("MESSAGES -> failed to Process message: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("MESSAGES -> failed to Process message"))
		}

		err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
		if err != nil {
			logger.Error("MESSAGES -> failed to commit messages: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("MESSAGES -> failed to commit messages"))
		} else {
			logger.Debug("MESSAGES -> message committed")
		}

		if eventHandler.ShouldPutMessagetoRetryQueue() {
			message := kafkaProducer.BuildMessageWithRetryMetaField(m.Value, conf.KafkaConsumer.MessagesTopic.KafkaTopic)
			err = kafkaProducer.ProduceMessage(string(m.Key), message, ctx)
			if err != nil {
				logger.Error("MESSAGES -> failed to produce event: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("MESSAGES -> failed to produce event"))
			}
			logger.Debug("MESSAGES -> message produced")
			logger.Sugar().Info("MESSAGES -> failed to process message. eventId: "+kafkaStructuredMessage.EventId+" . added to retry topic: ", conf.KafkaProducer.RetryQueueTopic.KafkaTopic)
		}

	}
}

func czrmUpdatesListener(ctx context.Context, logger *zap.Logger, conf *config.Config, KafkaConsumer *kafkaconsumer.KafkaConsumer, kafkaProducer *kafkaproducer.KafkaProducer, kafkaMessagehandler kafkaMessageHandler.IKafkaCzrmUpdatesMessageHandler, czrmUpdatesEventHandler kafkaEventHandlers.ICzrmUpdatesEventHandler) {
	sentryHub := sentryutils.InitLocalSentryhub()
	czrmUpdatesEventHandler.SetSentryHub(sentryHub)
	for {
		m, err := KafkaConsumer.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			logger.Error("CZRM-UPDATES-> error reading user message from kafka: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("CZRM-UPDATES-> error reading user message from kafka"))
			continue
		}
		kafkaStructuredMessage, err := kafkaMessagehandler.GetStructuredMessage(m)
		if err != nil {
			logger.Sugar().Debug("CZRM-UPDATES -> error getting the structured message. key: ", string(m.Key))
			continue
		}
		czrmUpdatesEventHandler.SetStructuredMessageFromCzrmUpdatesEvent(kafkaStructuredMessage)
		logger.Sugar().Debug("CZRM-UPDATES -> KEY event id: ", string(m.Key))
		logger.Sugar().Debug("CZRM-UPDATES -> EVENT id: ", kafkaStructuredMessage.EventId)
		err = czrmUpdatesEventHandler.ProcessMessage()
		if err != nil {
			logger.Error("CZRM-UPDATES-> failed to Process user message: ", zap.Error(err))
			sentryHub.CaptureException(errors.New("CZRM-UPDATES-> failed to Process user message"))
		} else {
			err = KafkaConsumer.KafkaReader.CommitMessages(ctx, m)
			if err != nil {
				logger.Error("CZRM-UPDATES-> failed to commit user messages: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("CZRM-UPDATES-> failed to commit user messages"))
			} else {
				logger.Debug("CZRM-UPDATES-> message committed")
			}
		}

		if czrmUpdatesEventHandler.ShouldPutMessagetoRetryQueue() {
			message := kafkaProducer.BuildMessageWithRetryMetaField(m.Value, conf.KafkaConsumer.CzrmUpdatesTopic.KafkaTopic)
			err = kafkaProducer.ProduceMessage(string(m.Key), message, ctx)
			if err != nil {
				logger.Error("CZRM-UPDATES -> failed to produce event: ", zap.Error(err))
				sentryHub.CaptureException(errors.New("CZRM-UPDATES -> failed to produce event"))
			}
			logger.Debug("CZRM-UPDATES -> message produced")
			logger.Sugar().Info("CZRM-UPDATES -> failed to process message. eventId: "+kafkaStructuredMessage.EventId+" . added to retry topic: ", conf.KafkaProducer.RetryQueueTopic.KafkaTopic)
		}

	}
}
