package config

import (
	"os"
	"strconv"
	"strings"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
)

type Config struct {
	logger        *zap.Logger
	KafkaConsumer struct {
		KafkaServer      []string
		ConsumerGroup    string
		ApplicationTopic struct {
			KafkaTopic  string
			TopicOffset int64
		}
		UsersTopic struct {
			KafkaTopic  string
			TopicOffset int64
		}
		MessagesTopic struct {
			KafkaTopic string

			TopicOffset int64
		}
		CzrmUpdatesTopic struct {
			KafkaTopic  string
			TopicOffset int64
		}
	}
	KafkaProducer struct {
		KafkaServer   []string
		ConsumerGroup string

		RetryQueueTopic struct {
			KafkaTopic    string
			ConsumerGroup string
		}
	}
	Sentry struct {
		Dsn string
	}
	Czrm struct {
		CzrmBaseName          string
		ApiVersion            string
		CzrmAuthType          string
		AuthTokenEndpoint     string
		AuthTokenUser         string
		AuthTokenPassword     string
		AuthTokenGrantType    string
		AuthTokenClientId     string
		AuthTokenClientSecret string
	}
	Sdc struct {
		SdcBaseName        string
		AuthTokenUser      string
		AuthTokenPassword  string
		CategoriesBaseName string
	}
	ServicesIdToprocess []string
	TenantsIdToProcess  []string
}

func NewConfig(log *zap.Logger) *Config {
	c := &Config{
		logger: log,
	}
	c.init()
	return c
}

func (c *Config) init() {
	c.setKakfaServerList()
	//application topic configuration
	c.KafkaConsumer.ApplicationTopic.KafkaTopic = c.getFromEnv("KAFKA_APPLICATIONS_TOPIC", "test")
	c.KafkaConsumer.ConsumerGroup = c.getFromEnv("KAFKA_APPLICATION_CONSUMER_GROUP", "test")
	c.KafkaConsumer.ApplicationTopic.TopicOffset = int64(c.getIntFromEnv("KAFKA_TOPIC_OFFSET", 0))
	//Users topic configuration
	c.KafkaConsumer.UsersTopic.KafkaTopic = c.getFromEnv("KAFKA_USERS_TOPIC", "test")
	c.KafkaConsumer.ConsumerGroup = c.getFromEnv("KAFKA_USERS_CONSUMER_GROUP", "test")
	c.KafkaConsumer.UsersTopic.TopicOffset = int64(c.getIntFromEnv("KAFKA_TOPIC_OFFSET", 0))
	//Messages topic configuration
	c.KafkaConsumer.MessagesTopic.KafkaTopic = c.getFromEnv("KAFKA_MESSAGES_TOPIC", "test")
	c.KafkaConsumer.ConsumerGroup = c.getFromEnv("KAFKA_MESSAGES_CONSUMER_GROUP", "test")
	c.KafkaConsumer.MessagesTopic.TopicOffset = int64(c.getIntFromEnv("KAFKA_TOPIC_OFFSET", 0))
	//czrm-updates topic configuration
	c.KafkaConsumer.CzrmUpdatesTopic.KafkaTopic = c.getFromEnv("KAFKA_CZRMUPDATE_TOPIC", "test")
	c.KafkaConsumer.ConsumerGroup = c.getFromEnv("KAFKA_CZRMUPDATE_CONSUMER_GROUP", "test")
	c.KafkaConsumer.CzrmUpdatesTopic.TopicOffset = int64(c.getIntFromEnv("KAFKA_TOPIC_OFFSET", 0))
	//retry-orchestrator
	c.KafkaProducer.RetryQueueTopic.KafkaTopic = c.getFromEnv("KAFKA_RETRY_QUEUE_TOPIC", "test")
	c.KafkaProducer.RetryQueueTopic.ConsumerGroup = c.getFromEnv("KAFKA_RETRY_QUEUE_CONSUMER_GROUP", "test")

	c.Sentry.Dsn = c.getFromEnv("SENTRY_DSN", "test")

	//czrm configuration
	c.Czrm.CzrmBaseName = c.getFromEnv("CZRM_BASENAME", "test")
	c.Czrm.ApiVersion = c.getFromEnv("CZRM_API_VERSION", "00")
	c.Czrm.CzrmAuthType = c.getFromEnv("CZRM_AUTH_TYPE", "SF")
	c.Czrm.AuthTokenEndpoint = c.getFromEnv("CZRM_AUTH_TOKEN_ENDPOINT", "test")
	c.Czrm.AuthTokenUser = c.getFromEnv("CZRM_AUTH_TOKEN_USER", "test")
	c.Czrm.AuthTokenPassword = c.getFromEnv("CZRM_AUTH_TOKEN_PASSWORD", "test")
	c.Czrm.AuthTokenGrantType = c.getFromEnv("CZRM_AUTH_TOKEN_GRANT_TYPE", "test")
	c.Czrm.AuthTokenClientId = c.getFromEnv("CZRM_AUTH_TOKEN_CLIENT_ID", "test")
	c.Czrm.AuthTokenClientSecret = c.getFromEnv("CZRM_AUTH_TOKEN_CLIENT_SECRET", "test")

	//sdc configuration
	c.Sdc.SdcBaseName = c.getFromEnv("SDC_BASENAME", "test")
	c.Sdc.AuthTokenUser = c.getFromEnv("SDC_AUTH_TOKEN_USER", "test")
	c.Sdc.AuthTokenPassword = c.getFromEnv("SDC_AUTH_TOKEN_PASSWORD", "test")
	c.Sdc.CategoriesBaseName = c.getFromEnv("SDC_CATEGORIES_BASENAME", "test")
	c.getServicesIdToProcess()
	c.getTenantsIdToProcess()
}

func (c *Config) getFromEnv(name string, defaultValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		c.logger.Sugar().Debug("found env var : ", name, ": ", val)
		return val
	}
	c.logger.Sugar().Debug("using default value for environment var: ", name)
	return defaultValue
}

func (c *Config) getIntFromEnv(name string, defaultValue int) int {
	stringVal := c.getFromEnv(name, strconv.Itoa(defaultValue))
	intVal, err := strconv.Atoi(stringVal)
	if err != nil {
		c.logger.Fatal("Error converting parameter "+name+" to int: %s\n", zap.Error(err))
		sentry.CaptureException(err)
	}
	return intVal
}

func (c *Config) getServicesIdToProcess() {
	//Segnalazione interna disservizio,Segnalazione scuola del comune di Genova
	serviceIDs := c.getFromEnv("SERVICE_IDS_TO_PROCESS", "fabe6333-cd8c-4574-9fe8-f908134254a7,e27a69f0-7ba5-4412-93d5-56e7cfc802cc")
	c.ServicesIdToprocess = strings.Split(serviceIDs, ",")
}
func (c *Config) getTenantsIdToProcess() {
	//Segnalazione interna disservizio,Segnalazione scuola del comune di Genova
	serviceIDs := c.getFromEnv("TENANTS_IDS_TO_PROCESS", "60e35f02-1509-408c-b101-3b1a28109329,9108fb0e-7d6c-4a91-9b4e-bd8c6d75d021")
	c.TenantsIdToProcess = strings.Split(serviceIDs, ",")
}

func (c *Config) setKakfaServerList() {
	//Segnalazione interna disservizio,Segnalazione scuola del comune di Genova
	kafkaServer := c.getFromEnv("KAFKA_SERVER", "kafka:9092")
	serverList := strings.Split(kafkaServer, ",")
	c.KafkaConsumer.KafkaServer = serverList
}
