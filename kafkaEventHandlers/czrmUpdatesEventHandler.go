package kafkaEventHandlers

import (
	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	czrmupdatesentities "opencitylabs.it/ticketron/kafkaEventHandlers/czrmUpdatesEntities"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
)

type ICzrmUpdatesEventHandler interface {
	SetStructuredMessageFromCzrmUpdatesEvent(message kafkaMessagesStruct.CzrmUpdatesMessageStruct)
	ProcessMessage() error
	ShouldPutMessagetoRetryQueue() bool
	SetSentryHub(s *sentry.Hub)
}

type czrmUpdatesEventHandler struct {
	logger                       *zap.Logger
	conf                         *config.Config
	czrmApi                      czrmapi.ICzrmApi
	message                      kafkaMessagesStruct.CzrmUpdatesMessageStruct
	sdcApi                       sdcapi.ISdcApi
	accountEntityHandler         czrmupdatesentities.IAccountEntity
	caseEntityHandler            czrmupdatesentities.ICaseEntity
	commentEnityHandler          czrmupdatesentities.ICommentEntity
	shouldPutMessagetoRetryQueue bool
	sentryHub                    *sentry.Hub
}

func NewCzrmUpdatesEventHandler(log *zap.Logger, config *config.Config) ICzrmUpdatesEventHandler {
	ev := &czrmUpdatesEventHandler{
		logger:                       log,
		conf:                         config,
		czrmApi:                      czrmapi.NewCzrmApi(log, config),
		sdcApi:                       sdcapi.NewSdcApi(log, config),
		shouldPutMessagetoRetryQueue: false,
	}
	ev.accountEntityHandler = czrmupdatesentities.NewAccountEntity(log, config, ev.czrmApi, ev.sdcApi)
	ev.caseEntityHandler = czrmupdatesentities.NewCaseEntity(log, config, ev.czrmApi, ev.sdcApi)
	ev.commentEnityHandler = czrmupdatesentities.NewCommentEntity(log, config, ev.czrmApi, ev.sdcApi)
	return ev
}

func (ev *czrmUpdatesEventHandler) SetStructuredMessageFromCzrmUpdatesEvent(message kafkaMessagesStruct.CzrmUpdatesMessageStruct) {
	ev.message = message
}
func (ev *czrmUpdatesEventHandler) SetSentryHub(s *sentry.Hub) {
	ev.sentryHub = s
}

func (ev *czrmUpdatesEventHandler) ProcessMessage() error {
	ev.shouldPutMessagetoRetryQueue = false
	switch ev.message.Entity {
	case "Account":
		ev.accountEntityHandler.SetSentryHub(ev.sentryHub)
		err := ev.accountEntityHandler.CreateOrUpdateAccountOnSdc(ev.message)
		if err != nil {
			ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error processing the event ")
			ev.shouldPutMessagetoRetryQueue = true
			return err
		}
	case "Commento__c":
		ev.commentEnityHandler.SetSentryHub(ev.sentryHub)
		err := ev.commentEnityHandler.AddMessageOnApplication(ev.message)
		if err != nil {
			ev.logger.Error("CZRM-UPDATES-CASE -> error processing the event ")
			ev.shouldPutMessagetoRetryQueue = true
			return err
		} //disabled case updated
	case "Case-disabled":
		err := ev.caseEntityHandler.ProcessCaseEvent(ev.message)
		if err != nil {
			ev.logger.Error("CZRM-UPDATES-CASE -> error processing the event ")
			return err
		}
	case "contentVersion-disabled":
		err := ev.caseEntityHandler.ProcessCaseEvent(ev.message)
		if err != nil {
			ev.logger.Error("CZRM-UPDATES-CASE -> error processing the event ")
			return err
		}
	}
	ev.cleanResources()
	return nil
}

func (ev *czrmUpdatesEventHandler) ShouldPutMessagetoRetryQueue() bool {
	return ev.shouldPutMessagetoRetryQueue
}

func (ev *czrmUpdatesEventHandler) cleanResources() {
	ev.message = kafkaMessagesStruct.CzrmUpdatesMessageStruct{}
}
