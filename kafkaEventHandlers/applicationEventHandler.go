package kafkaEventHandlers

import (
	"errors"
	"net/http"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
)

type IApplicationEventHandler interface {
	SetStructuredMessageFromApplicationEvent(message kafkaMessagesStruct.ApplicationMessageStruct)
	ProcessMessage() error
	sendSegnalazioneToCzrm()
	getCzrmAccountId(token string) (string, error)
	GetCzrmAccountBySdcAccountIdRequest(token string) (string, int)
	retryGetCzrmAccountBySdcAccountIdRequest() string
	createAccountRequest(token string) (string, error)
	retryCreateAccountRequest() (string, error)
	getCaseByApplicationId(token string) (string, int)
	retryGetCaseRequest() string
	createCaseRequest(string, string, string) (string, error)
	shouldProcesEvent() bool
	shouldProcesThisTenants() bool
	ShouldPutMessagetoRetryQueue() bool
	SetSentryHub(*sentry.Hub)
}

type applicationEventHandler struct {
	logger                       *zap.Logger
	conf                         *config.Config
	czrmApi                      czrmapi.ICzrmApi
	sdcapi                       sdcapi.ISdcApi
	message                      kafkaMessagesStruct.ApplicationMessageStruct
	shouldPutMessagetoRetryQueue bool
	sentryHub                    *sentry.Hub
}

func NewApplicationEventHandler(log *zap.Logger, config *config.Config) IApplicationEventHandler {
	ch := &applicationEventHandler{
		logger:                       log,
		conf:                         config,
		czrmApi:                      czrmapi.NewCzrmApi(log, config),
		sdcapi:                       sdcapi.NewSdcApi(log, config),
		shouldPutMessagetoRetryQueue: false,
	}
	return ch
}
func (ch *applicationEventHandler) SetStructuredMessageFromApplicationEvent(message kafkaMessagesStruct.ApplicationMessageStruct) {
	ch.message = message
}
func (ch *applicationEventHandler) SetSentryHub(s *sentry.Hub) {
	ch.sentryHub = s
}
func (ch *applicationEventHandler) ShouldPutMessagetoRetryQueue() bool {
	return ch.shouldPutMessagetoRetryQueue
}
func (ch *applicationEventHandler) ProcessMessage() error {
	ch.shouldPutMessagetoRetryQueue = false

	if !ch.shouldProcesEvent() || ch.message.ExternalId != "" /*and oppure or?? ch.message.Status != 1900 */ {
		ch.logger.Debug("APPLICATION -> This application shouldn't be processed")
		return nil
	}
	ch.sendSegnalazioneToCzrm()
	ch.cleanResources()
	return nil
}

func (ch *applicationEventHandler) sendSegnalazioneToCzrm() {
	var czrmAccountId string

	token, err := ch.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> AccessAuthTokenRequest returned an error", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> AccessAuthTokenRequest returned an error"))
		ch.shouldPutMessagetoRetryQueue = true
		return

	}
	czrmAccountId, _ = ch.getCzrmAccountId(token)

	if czrmAccountId == "" {
		ch.logger.Sugar().Debug("APPLICATION -> Trying to get account by fiscal code ", czrmAccountId)
		czrmAccountId, _, _ = ch.czrmApi.AccessGetCzrmAccountByFiscalCodeRequest().GetAccountByFiscalCodeRequest(token, ch.message.Data.ApplicantDataFiscalCodeDataFiscalCode)
		ch.logger.Sugar().Debug("APPLICATION -> getAccountByFiscalCode. accountid: ", czrmAccountId)

	}
	if czrmAccountId == "" {
		ch.logger.Error("APPLICATION -> impossible to retrieve or create accountId")
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> impossible to retrieve or create accountId"))
		ch.shouldPutMessagetoRetryQueue = true
		return

	}
	personAccountId, _ := ch.getPersonContactId(token, czrmAccountId)
	ch.logger.Sugar().Debug("APPLICATION -> czrmAccountId: ", czrmAccountId)
	caseId, err := ch.getOrCreateCase(token, czrmAccountId, personAccountId)
	if err != nil {
		ch.shouldPutMessagetoRetryQueue = true
		return

	}

	if ch.shouldUpdateFiscalCode(czrmAccountId, token) {
		//aggiorniamo il codice fiscale perchè se viene creato tramite l'evento dal topic user non viene settato il codice fiscale.
		//questo succede per gli utenti anonimi. Quindi sul CzRM glielo settiamo prendendo il valore del codice fiscale dall'application
		ch.logger.Debug("APPLICATION -> account already created on CzRM, we need to update the fiscal code because it is empty")
		ch.czrmApi.AccessUpdateAccountFiscalCodeRequest().SetInputValues(ch.getUpdateAccountFiscalCodeInputValues())
		ch.czrmApi.AccessUpdateAccountFiscalCodeRequest().UpdateAccountFiscalCode(token, czrmAccountId)
	}
	if caseId != "" {
		err = ch.linkAttachmentsToCase(token, caseId)
		if err != nil {
			ch.shouldPutMessagetoRetryQueue = true
			return

		}
		err := ch.setExternalIdOnApplication(caseId)
		if err != nil {
			ch.shouldPutMessagetoRetryQueue = true
			return

		}

	} else {
		ch.logger.Sugar().Info("APPLICATION -> event id: " + ch.message.EventId + ", failed to create case")
		ch.logger.Sugar().Error("APPLICATION -> event id: " + ch.message.EventId + ", failed to create case")
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> event id: " + ch.message.EventId + ", failed to create case"))
		ch.shouldPutMessagetoRetryQueue = true
		return
	}
	ch.logger.Sugar().Info("APPLICATION -> event id: "+ch.message.EventId+", processed case with following ID: ", caseId)
	// cleaning resources
	ch.message.Data.Docs = []kafkaMessagesStruct.Doc{}
	ch.message.Data.Images = []kafkaMessagesStruct.Image{}
	caseId = ""
}
func (ch *applicationEventHandler) getCzrmAccountId(token string) (string, error) {
	var czrmAccountId string
	var responseStatus int
	var err error
	czrmAccountId, responseStatus = ch.GetCzrmAccountBySdcAccountIdRequest(token)
	if responseStatus == http.StatusNotFound {
		czrmAccountId, err = ch.createAccountRequest(token)
		if err != nil {
			ch.logger.Error("APPLICATION -> error creating new account")
			ch.sentryHub.CaptureException(errors.New("APPLICATION -> error creating new account"))
			return "", err
		}
	}
	return czrmAccountId, nil
}

func (ch *applicationEventHandler) getPersonContactId(token, czrmAccountId string) (string, error) {
	ch.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().SetInputValues(ch.getGetCzrmAccountByCzrmIdInputValues(czrmAccountId))
	czrmAccountInfo, _, err := ch.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().GetAccountByCzrmIdRequest(token)
	if err != nil {
		ch.logger.Error("APPLICATION -> GetAccountByCzrmIdRequest error performing the request", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> GetAccountByCzrmIdRequest error performing the request"))
		return "", err
	}
	return czrmAccountInfo.PersonContactID, nil
}

func (ch *applicationEventHandler) GetCzrmAccountBySdcAccountIdRequest(token string) (string, int) {
	czrmAccountId, responseStatus, err := ch.czrmApi.AccessGetCzrmAccountBySdcAccountIdRequest().GetCzrmAccountIdBySdcAccountIdRequest(token, ch.message.User)
	if err != nil {
		ch.logger.Error("APPLICATION -> GetCzrmAccountBySdcAccountIdRequest error performing the request", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> GetCzrmAccountBySdcAccountIdRequest error performing the request"))
	}
	if responseStatus == http.StatusUnauthorized {
		ch.logger.Debug("APPLICATION -> GetCzrmAccountBySdcAccountIdRequest not authorized. retrying...")
		czrmAccountId = ch.retryGetCzrmAccountBySdcAccountIdRequest()

	}
	return czrmAccountId, responseStatus
}
func (ch *applicationEventHandler) retryGetCzrmAccountBySdcAccountIdRequest() string {
	aToken, err := ch.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> retry AccessAuthTokenRequest returned an error", zap.Error(err))
		return ""
	}
	czrmAccountId, responseStatus, err := ch.czrmApi.AccessGetCzrmAccountBySdcAccountIdRequest().GetCzrmAccountIdBySdcAccountIdRequest(aToken, ch.message.User)
	if err != nil || responseStatus != http.StatusOK {
		ch.logger.Error("APPLICATION -> retry AccessAuthTokenRequest returned an error", zap.Error(err))
		return ""
	}
	return czrmAccountId
}
func (ch *applicationEventHandler) createAccountRequest(token string) (string, error) {
	ch.logger.Debug("APPLICATION -> account not present on CzRM. we need to create it...")
	ch.czrmApi.AccessCreateAccountRequest().SetInputValues(ch.getCreateAccountInputValues())
	czrmAccountId, responseStatus, err := ch.czrmApi.AccessCreateAccountRequest().CreateNewAccount(token)
	if responseStatus == http.StatusUnauthorized {
		ch.logger.Debug("APPLICATION -> CreateNewAccount not authorized. retrying...")
		czrmAccountId, err = ch.retryCreateAccountRequest()
		if err != nil {
			ch.logger.Error("APPLICATION -> RetryCreateNewAccount returned an error", zap.Error(err))
			return "", err
		}

	}
	if err != nil {
		ch.logger.Error("APPLICATION -> CreateNewAccount returned an error", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> CreateNewAccount returned an error"))
		return "", err
	}

	return czrmAccountId, nil
}
func (ch *applicationEventHandler) retryCreateAccountRequest() (string, error) {
	aToken, err := ch.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> retry AccessAuthTokenRequest returned an error", zap.Error(err))
		return "", err
	}
	ch.czrmApi.AccessCreateAccountRequest().SetInputValues(ch.getCreateAccountInputValues())
	czrmAccountId, responseStatus, err := ch.czrmApi.AccessCreateAccountRequest().CreateNewAccount(aToken)
	if err != nil || responseStatus != http.StatusOK {
		ch.logger.Error("APPLICATION -> retry CreateNewAccount returned an error", zap.Error(err))
		return "", err
	}
	return czrmAccountId, nil
}
func (ch *applicationEventHandler) shouldUpdateFiscalCode(accountId, token string) bool {
	czrmAccountId, _, err := ch.czrmApi.AccessGetCzrmAccountBySdcAccountIdRequest().GetCzrmAccountBySdcAccountIdRequest(token, accountId)
	if err != nil {
		ch.logger.Error("APPLICATION -> GetCzrmAccountBySdcAccountIdRequest error performing the request", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> GetCzrmAccountBySdcAccountIdRequest error performing the request"))
		return false
	}
	if czrmAccountId.CodiceFiscaleC == "" && czrmAccountId.CodiceFiscalePc == "" {
		return true
	}
	return false
}
func (ch *applicationEventHandler) getOrCreateCase(token, czrmAccountId, personAccountId string) (string, error) {
	var err error
	caseId, responseStatus := ch.getCaseByApplicationId(token)
	if responseStatus == http.StatusNotFound {
		caseId, err = ch.createCaseRequest(token, czrmAccountId, personAccountId)
		if err != nil {
			ch.logger.Error("APPLICATION -> not able to createCaseRequest error ", zap.Error(err))
			ch.sentryHub.CaptureException(errors.New("APPLICATION -> not able to createCaseRequest error"))
			return "", err
		}
		ch.logger.Sugar().Debug("APPLICATION -> new case created. ID: ", caseId)
	} else {
		ch.logger.Sugar().Debug("APPLICATION -> found existing case. ID: ", caseId)
	}
	return caseId, nil
}
func (ch *applicationEventHandler) getCaseByApplicationId(token string) (string, int) {
	caseId, responseStatus, err := ch.czrmApi.AccessGetCaseByApplicationIdRequest().GetCaseByApplicationId(token, ch.message.Id)
	if err != nil {
		ch.logger.Error("APPLICATION -> GetCase error performing the request", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> GetCase error performing the request"))
	}
	if responseStatus == http.StatusUnauthorized {
		ch.logger.Debug("APPLICATION -> GetCase not authorized. retrying...")
		caseId = ch.retryGetCaseRequest()

	}
	return caseId, responseStatus
}
func (ch *applicationEventHandler) retryGetCaseRequest() string {
	aToken, err := ch.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> retry AccessAuthTokenRequest returned an error", zap.Error(err))
		return ""
	}
	caseId, responseStatus, err := ch.czrmApi.AccessGetCaseByApplicationIdRequest().GetCaseByApplicationId(aToken, ch.message.Id)
	if err != nil || responseStatus != http.StatusOK {
		ch.logger.Error("APPLICATION -> retry CreateNewAccount returned an error", zap.Error(err))
		return ""
	}
	return caseId
}
func (ch *applicationEventHandler) createCaseRequest(token, czrmAccountId, personAccountId string) (string, error) {
	ch.logger.Debug("APPLICATION -> case not present on CzRM. we need to create it...")
	ch.czrmApi.AccessCreateCaseRequest().SetInputValues(ch.getCreateCaseInputValues(personAccountId))
	caseId, _, err := ch.czrmApi.AccessCreateCaseRequest().CreateNewCase(token, czrmAccountId)
	if err != nil {
		ch.logger.Error("APPLICATION -> CreateCase returned an error", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> CreateCase returned an error"))
		return "", err
	}
	return caseId, nil
}

func (ch *applicationEventHandler) getGetCzrmAccountByCzrmIdInputValues(czrmAccountId string) czrmrequeststructs.GetAccountByCzrmIdInputValues {
	return czrmrequeststructs.GetAccountByCzrmIdInputValues{
		User: czrmAccountId,
	}
}
func (ch *applicationEventHandler) getCreateCaseInputValues(PersonContactId string) czrmrequeststructs.CreateCaseinputValues {
	return czrmrequeststructs.CreateCaseinputValues{
		Origin:              "SegnalaCi",
		Subject:             ch.message.Data.Subject,
		Description:         ch.message.Data.Details,
		PrivacyC:            true,
		CategoriaR:          ch.czrmApi.GetCategoriaOperator(ch.message.Data.MicromacrocategoryLabel),
		CategoriaCittadino:  ch.getItaCategoryById(ch.message.Data.TypeValue),
		PostCode:            ch.message.Data.AddressAddressPostcode,
		Address:             ch.message.Data.AddressAddressRoad,
		City:                ch.message.Data.AddressAddressTown,
		IndirizzoLatitudeS:  ch.czrmApi.GetLat(ch.message.Data.AddressLat),
		IndirizzoLongitudeS: ch.czrmApi.GetLon(ch.message.Data.AddressLon),
		ApplicationId:       ch.message.Id,
		PdfLink:             ch.message.Data.PdfLink,
		Municipio:           ch.czrmApi.GetMunicipioName(ch.message.GeographicAreas),
		CreatedAt:           ch.message.SubmittedAt,
		ContactId:           PersonContactId,
	}
}
func (ch *applicationEventHandler) getItaCategoryById(categoryTypeValue string) string {

	categoriesList, err := ch.sdcapi.AccessGetCategories().GetCategories()
	if err != nil {
		ch.logger.Error("APPLICATION -> GetCategories request failed: ", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> GetCategories request failed"))
		return ""
	}
	for _, category := range categoriesList {
		if category.Value == categoryTypeValue {
			return category.Label
		}
	}
	ch.logger.Error("APPLICATION -> categoy having following value not found: " + ch.message.Data.TypeValue)
	return ""
}

func (ch *applicationEventHandler) shouldProcesEvent() bool {
	if !ch.shouldProcesThisTenants() {
		return false
	}

	for _, serviceID := range ch.conf.ServicesIdToprocess {
		if serviceID == ch.message.ServiceID {
			return true
		}
	}
	return false
}
func (ch *applicationEventHandler) shouldProcesThisTenants() bool {

	for _, tenantID := range ch.conf.TenantsIdToProcess {
		if tenantID == ch.message.TenantID {
			return true
		}
	}
	return false
}

func (ch *applicationEventHandler) linkAttachmentsToCase(czrmToken, caseId string) error {
	sdcToken, err := ch.sdcapi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> error getting sdc token", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> error getting sdc token"))
		return err
	}
	if len(ch.message.Data.Docs) != 0 {
		_ = ch.handleDocsAttchments(sdcToken, czrmToken, caseId, ch.message.Data.Docs)

	}
	if len(ch.message.Data.Images) != 0 {
		_ = ch.handleImagesAttchments(sdcToken, czrmToken, caseId, ch.message.Data.Images)

	}
	return nil
}

func (ch *applicationEventHandler) handleDocsAttchments(sdcToken, czrmToken, caseId string, attachments []kafkaMessagesStruct.Doc) error {
	for _, doc := range ch.message.Data.Docs {
		if ch.shouldAddThisAttachment(doc.ID) {
			attachment, err := ch.sdcapi.AccessGetAttachment().GetAttachment(sdcToken, doc.URL)
			if err != nil {
				ch.logger.Error("APPLICATION -> error getting docs attachment", zap.Error(err))
				ch.sentryHub.CaptureException(errors.New("APPLICATION -> error getting docs attachment"))
				return err
			}

			ch.logger.Sugar().Debug("APPLICATION -> new doc, we need to add it on the czrm ")
			ch.czrmApi.AccessAddAttachmentRequest().SetInputValues(ch.getAddDocsAttachmentInputValues(doc, caseId, attachment))
			externId, err := ch.czrmApi.AccessAddAttachmentRequest().AddAttachmentToCase(czrmToken)
			if err != nil {
				ch.logger.Error("APPLICATION -> error adding attachment to czrm", zap.Error(err))
				ch.sentryHub.CaptureException(errors.New("APPLICATION -> error adding attachment to czrm"))
				return err
			}
			ch.logger.Sugar().Debug("APPLICATION -> added attachment to case ", caseId)

			if externId != "" {
				ch.sdcapi.AccessUpdateApplicationAttachment().SetInputValues(externId)
				ch.sdcapi.AccessUpdateApplicationAttachment().UpdateApplicationAttachment(sdcToken, ch.message.Id, doc.ID)
				if err != nil {
					ch.logger.Error("APPLICATION -> error updating attachment to sdc", zap.Error(err))
					ch.sentryHub.CaptureException(errors.New("APPLICATION -> error updating attachment to sdc"))
					return err
				}
				ch.logger.Sugar().Debug("APPLICATION -> updated doc attachment on sdc application ", ch.message.Id)
			}

		}
	}
	return nil
}

func (ch *applicationEventHandler) handleImagesAttchments(sdcToken, czrmToken, caseId string, attachments []kafkaMessagesStruct.Image) error {
	for _, image := range ch.message.Data.Images {
		if ch.shouldAddThisAttachment(image.ID) {
			attachment, err := ch.sdcapi.AccessGetAttachment().GetAttachment(sdcToken, image.URL)
			if err != nil {
				ch.logger.Error("APPLICATION -> error getting Images attachment", zap.Error(err))
				ch.sentryHub.CaptureException(errors.New("APPLICATION -> error getting Images attachment"))
				return err
			}
			ch.logger.Sugar().Debug("APPLICATION -> new image, we need to add it on the czrm ")
			ch.czrmApi.AccessAddAttachmentRequest().SetInputValues(ch.getAddImagesAttachmentInputValues(image, caseId, attachment))
			externId, err := ch.czrmApi.AccessAddAttachmentRequest().AddAttachmentToCase(czrmToken)
			if err != nil {
				ch.logger.Error("APPLICATION -> error adding attachment to czrm", zap.Error(err))
				ch.sentryHub.CaptureException(errors.New("APPLICATION -> error adding attachment to czrm"))
				return err
			}
			if externId != "" {
				ch.sdcapi.AccessUpdateApplicationAttachment().SetInputValues(externId)
				ch.sdcapi.AccessUpdateApplicationAttachment().UpdateApplicationAttachment(sdcToken, ch.message.Id, image.ID)
				if err != nil {
					ch.logger.Error("APPLICATION -> error updating attachment to sdc", zap.Error(err))
					ch.sentryHub.CaptureException(errors.New("APPLICATION -> error updating attachment to sdc"))
					return err
				}
				ch.logger.Sugar().Debug("APPLICATION -> updated image attachment on sdc application ", ch.message.Id)
			}

		}
	}

	return nil
}

func (ch *applicationEventHandler) shouldAddThisAttachment(attachmentId string) bool {
	ch.logger.Sugar().Debug("checking if attachment ", attachmentId, " should be uploaded to case")
	for _, attachment := range ch.message.Attachments {
		if attachmentId == attachment.ID {
			if attachment.ExternalID != "" {
				return false
			}
		}
	}
	return true
}

func (ch *applicationEventHandler) getAddDocsAttachmentInputValues(doc kafkaMessagesStruct.Doc, caseId, attachement string) czrmrequeststructs.AddAttachmentInputValues {
	return czrmrequeststructs.AddAttachmentInputValues{
		Title:                  doc.OriginalName,
		PathOnClient:           doc.OriginalName,
		FirstPublishLocationid: caseId,
		VersionData:            attachement,
	}

}

func (ch *applicationEventHandler) getAddImagesAttachmentInputValues(img kafkaMessagesStruct.Image, caseId, attachement string) czrmrequeststructs.AddAttachmentInputValues {
	return czrmrequeststructs.AddAttachmentInputValues{
		Title:                  img.OriginalName,
		PathOnClient:           img.OriginalName,
		FirstPublishLocationid: caseId,
		VersionData:            attachement,
	}

}

func (ch *applicationEventHandler) setExternalIdOnApplication(caseId string) error {
	sdcToken, err := ch.sdcapi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ch.logger.Error("APPLICATION -> error getting sdc token", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> error getting sdc token"))
		return err
	}
	err = ch.sdcapi.AccessUpdateExternaIdApplication().UpdateExternalIdApplication(sdcToken, ch.message.Id, caseId)
	if err != nil {
		ch.logger.Error("APPLICATION -> updating externalId on aplication", zap.Error(err))
		ch.sentryHub.CaptureException(errors.New("APPLICATION -> updating externalId on aplication"))
		return err
	}
	return nil
}

func (ch *applicationEventHandler) getCreateAccountInputValues() czrmrequeststructs.CreateAccountInputValues {
	return czrmrequeststructs.CreateAccountInputValues{
		FirstName:    ch.message.Data.ApplicantDataCompletenameDataName,
		LastName:     ch.message.Data.ApplicantDataCompletenameDataSurname,
		Fiscalcode:   ch.czrmApi.GetValidOrNullfiscalCode(ch.message.Data.ApplicantDataFiscalCodeDataFiscalCode),
		Email:        ch.message.Data.ApplicantDataEmailAddress,
		SdcAccountId: ch.message.User,
		Phone:        ch.message.Data.ApplicantDataPhoneNumber,
	}
}

func (ch *applicationEventHandler) getUpdateAccountFiscalCodeInputValues() czrmrequeststructs.UpdateAccountInputValues {
	return czrmrequeststructs.UpdateAccountInputValues{
		Fiscalcode: ch.czrmApi.GetValidOrNullfiscalCode(ch.message.Data.ApplicantDataFiscalCodeDataFiscalCode),
	}
}
func (ch *applicationEventHandler) cleanResources() {
	ch.message = kafkaMessagesStruct.ApplicationMessageStruct{}
}
