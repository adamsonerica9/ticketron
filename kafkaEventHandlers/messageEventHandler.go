package kafkaEventHandlers

import (
	"errors"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IMessageEventHandler interface {
	SetStructuredMessageFromMessageEvent(message kafkaMessagesStruct.MessageMessageStruct)
	ProcessMessage() error
	ShouldPutMessagetoRetryQueue() bool
	shouldProcesEvent() bool
	shouldProcesThisTenants() bool
	SetSentryHub(*sentry.Hub)
}

type messageEventHandler struct {
	logger                       *zap.Logger
	conf                         *config.Config
	czrmApi                      czrmapi.ICzrmApi
	sdcApi                       sdcapi.ISdcApi
	message                      kafkaMessagesStruct.MessageMessageStruct
	shouldPutMessagetoRetryQueue bool
	sentryHub                    *sentry.Hub
}

func NewMessageEventHandler(log *zap.Logger, config *config.Config) IMessageEventHandler {
	mv := &messageEventHandler{
		logger:                       log,
		conf:                         config,
		czrmApi:                      czrmapi.NewCzrmApi(log, config),
		sdcApi:                       sdcapi.NewSdcApi(log, config),
		shouldPutMessagetoRetryQueue: false,
	}
	return mv
}

func (em *messageEventHandler) SetStructuredMessageFromMessageEvent(message kafkaMessagesStruct.MessageMessageStruct) {
	em.message = message
}
func (em *messageEventHandler) SetSentryHub(s *sentry.Hub) {
	em.sentryHub = s
}
func (em *messageEventHandler) ProcessMessage() error {
	em.shouldPutMessagetoRetryQueue = false
	if !em.shouldProcesEvent() {
		em.logger.Debug("MESSAGES -> this message shouldn't be processed")
		return nil
	}

	em.addCommentToCzrmCase()
	em.cleanResources()
	return nil
}

func (em *messageEventHandler) ShouldPutMessagetoRetryQueue() bool {
	return em.shouldPutMessagetoRetryQueue
}
func (em *messageEventHandler) addCommentToCzrmCase() {

	token, err := em.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES-> error retrieving sdc auth token", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error retrieving sdc auth token"))
		em.shouldPutMessagetoRetryQueue = true
		return
	}
	em.logger.Sugar().Debug("MESSAGES-> em.message.RelatedEntityId: ", em.message.RelatedEntityId)
	em.sdcApi.AccessGetApplicationByApplicationIdRequest().SetInputValues(em.getGetApplicationByApplicationIdInputvalues())

	application, err := em.sdcApi.AccessGetApplicationByApplicationIdRequest().GetApplicationByApplicationId(token)

	if err != nil {
		em.logger.Error("MESSAGES-> error performing GetApplicationByApplicationId", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error performing GetApplicationByApplicationId"))
	}

	if !em.shouldProcessThisService(application.ServiceID) {
		em.logger.Sugar().Debug("MESSAGES-> this service shouldn't be processed :", application.ServiceID)
		return
	}

	caseid, err := em.GetCaseApplicationId(em.message.RelatedEntityId)
	if err != nil {
		em.logger.Error("MESSAGES-> error performing GetCaseApplicationId: ", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error performing GetCaseApplicationId"))
		em.shouldPutMessagetoRetryQueue = true
		return

	}

	em.logger.Sugar().Debug("MESSAGES-> caseId from GetCaseApplicationId: ", caseid)
	commentCzrmId, err := em.createCommentOnCzrm(caseid, application.UserName)
	if err != nil {
		em.logger.Error("MESSAGES-> error performing createCommentOnCzrm: ", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error performing createCommentOnCzrm"))
		em.shouldPutMessagetoRetryQueue = true
		return
	}
	em.updateSdcCommentWithExternalID(em.message.RelatedEntityId, em.message.ID, commentCzrmId)
	if err != nil {
		em.logger.Error("MESSAGES-> error update SdcComment With ExternalID: ", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error update SdcComment With ExternalID"))
		em.shouldPutMessagetoRetryQueue = true
		return
	}
	if len(em.message.Attachments) != 0 {
		em.logger.Sugar().Debug("MESSAGES-> this message contains ", len(em.message.Attachments), " attachments")
		err = em.sendMessageAttachmentsToCzrm(em.message.Attachments, caseid)
		if err != nil {
			em.shouldPutMessagetoRetryQueue = true
			return
		}

	}
	em.logger.Sugar().Info("MESSAGES -> processed event: "+em.message.EventId+" . added new message from application: "+application.ID+" , to case: ", caseid)
}

func (em *messageEventHandler) getGetApplicationByApplicationIdInputvalues() sdcapistructs.GetApplicationByApplicationIdInputValues {
	return sdcapistructs.GetApplicationByApplicationIdInputValues{
		ApplicationId: em.message.RelatedEntityId,
	}
}

func (em *messageEventHandler) GetCaseApplicationId(applicationId string) (string, error) {
	token, err := em.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES-> AccessAuthTokenRequest from CzRM returned an error", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> AccessAuthTokenRequest from CzRM returned an error"))
		return "", err
	}
	caseId, _, err := em.czrmApi.AccessGetCaseByApplicationIdRequest().GetCaseByApplicationId(token, applicationId)
	if err != nil {
		em.logger.Error("MESSAGES-> GetCaseApplicationId error performing the request", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> GetCaseApplicationId error performing the request"))
	}
	return caseId, err
}

func (em *messageEventHandler) createCommentOnCzrm(caseid, autore string) (string, error) {
	token, err := em.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES -> AccessAuthTokenRequest from CzRM returned an error", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES -> AccessAuthTokenRequest from CzRM returned an error"))
		return "", err
	}
	em.czrmApi.AccessCreateCommentRequest().SetInputValues(em.getInputValuesforCreateComment(caseid, autore))
	commentCzrmId, err := em.czrmApi.AccessCreateCommentRequest().CreateNewComment(token)
	if err != nil {
		em.logger.Error("MESSAGES-> CreateNewComment error performing the request", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> CreateNewComment error performing the request"))
	}
	return commentCzrmId, nil
}

func (em *messageEventHandler) getInputValuesforCreateComment(caseId, czrmAccountid string) czrmrequeststructs.CreateCommentInputValues {
	return czrmrequeststructs.CreateCommentInputValues{
		CaseIDC:           caseId,
		DescriptionC:      em.message.Message,
		AutoreCommento__c: czrmAccountid,
	}
}

func (em *messageEventHandler) shouldProcesEvent() bool {
	return em.shouldProcesThisTenants() && em.message.ExternalID == "" && em.message.TransmissionType == "inbound"
}
func (em *messageEventHandler) shouldProcesThisTenants() bool {

	for _, tenantID := range em.conf.TenantsIdToProcess {
		if tenantID == em.message.TenantID {
			return true
		}
	}
	return false
}
func (em *messageEventHandler) shouldProcessThisService(currentService string) bool {

	for _, serviceID := range em.conf.ServicesIdToprocess {
		if serviceID == currentService {
			return true
		}
	}
	return false
}

func (em *messageEventHandler) updateSdcCommentWithExternalID(applicationid, messageId, externalid string) error {
	token, err := em.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES-> error retrieving sdc auth token", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error retrieving sdc auth token"))
		return err
	}
	err = em.sdcApi.AccessUpdateExternalIdComment().UpdateExternalIdComment(token, applicationid, messageId, externalid)
	if err != nil {
		em.logger.Error("MESSAGES-> error updating external id on comment", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error updating external id on comment"))
		return err
	}
	em.logger.Sugar().Debug("MESSAGES-> external Id comment has been updated correctly")
	return nil
}

func (em *messageEventHandler) sendMessageAttachmentsToCzrm(Attachments []kafkaMessagesStruct.Attachment, caseId string) error {
	czrmToken, err := em.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES-> error getting czrm token", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error getting czrm token"))
		return err
	}
	sdcToken, err := em.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		em.logger.Error("MESSAGES-> error getting sdc token", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> error getting sdc token"))
		return err
	}
	if err != nil {
		em.logger.Error("MESSAGES-> AccessAuthTokenRequest from CzRM returned an error", zap.Error(err))
		em.sentryHub.CaptureException(errors.New("MESSAGES-> AccessAuthTokenRequest from CzRM returned an error"))
		return err
	}

	for _, attachment := range Attachments {
		attachmentBase64, err := em.sdcApi.AccessGetAttachment().GetAttachment(sdcToken, attachment.URL)
		if err != nil {
			em.logger.Error("MESSAGES-> error getting attachment attachment", zap.Error(err))
			em.sentryHub.CaptureException(errors.New("MESSAGES-> error getting attachment attachment"))
			return err
		}

		em.czrmApi.AccessAddAttachmentRequest().SetInputValues(em.setAddAttachmentInputValues(attachment, caseId, attachmentBase64))
		externId, err := em.czrmApi.AccessAddAttachmentRequest().AddAttachmentToCase(czrmToken)
		if err != nil {
			em.logger.Error("MESSAGES-> error adding attachment to czrm", zap.Error(err))
			em.sentryHub.CaptureException(errors.New("MESSAGES-> error adding attachment to czrm"))
			return err
		}
		em.sdcApi.AccessUpdateApplicationAttachment().SetInputValues(externId)
		em.sdcApi.AccessUpdateApplicationAttachment().UpdateApplicationAttachment(sdcToken, em.message.RelatedEntityId, attachment.ID)
		if err != nil {
			em.logger.Error("MESSAGES-> error updating attachment to sdc", zap.Error(err))
			em.sentryHub.CaptureException(errors.New("MESSAGES-> error updating attachment to sdc"))
			return err
		}
	}
	return nil
}

func (em *messageEventHandler) setAddAttachmentInputValues(attachmentStruct kafkaMessagesStruct.Attachment, caseId, attachement string) czrmrequeststructs.AddAttachmentInputValues {
	return czrmrequeststructs.AddAttachmentInputValues{
		Title:                  attachmentStruct.OriginalName,
		PathOnClient:           attachmentStruct.OriginalName,
		FirstPublishLocationid: caseId,
		VersionData:            attachement,
	}

}

func (em *messageEventHandler) cleanResources() {
	em.message = kafkaMessagesStruct.MessageMessageStruct{}
}
