package czrmupdatesentities

import (
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICaseEntity interface {
	ProcessCaseEvent(m kafkaMessagesStruct.CzrmUpdatesMessageStruct) error
}

type caseEntity struct {
	logger  *zap.Logger
	conf    *config.Config
	czrmApi czrmapi.ICzrmApi
	message kafkaMessagesStruct.CzrmUpdatesMessageStruct
	sdcApi  sdcapi.ISdcApi
}

func NewCaseEntity(log *zap.Logger, confing *config.Config, czrmapi czrmapi.ICzrmApi, sdcapi sdcapi.ISdcApi) ICaseEntity {
	return &caseEntity{
		logger:  log,
		conf:    confing,
		czrmApi: czrmapi,
		sdcApi:  sdcapi,
	}
}

func (ce *caseEntity) ProcessCaseEvent(m kafkaMessagesStruct.CzrmUpdatesMessageStruct) error {
	ce.message = m
	//cleaning resources
	defer func() { ce.message = kafkaMessagesStruct.CzrmUpdatesMessageStruct{} }()
	sdcAccountInfo := sdcapistructs.UserInfo{}
	czrmAccountInfo := czrmrequeststructs.GetAccountByCzrmIdSuccessResponse{}

	ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> working on case: ", ce.message.ID)
	caseInfo, err := ce.getCase()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> case not found on CzRM ", zap.Error(err))
		return err
	}
	if caseInfo.AccountID == "" {
		ce.logger.Error("CZRM-UPDATES-CASE -> case without account id ", zap.Error(err))
		return err
	}
	czrmAccountInfo, err = ce.getCzrmAccount(caseInfo.AccountID)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving user from CzRM ", zap.Error(err))
		return err
	}
	if ce.shouldCreateAccountOnSdc(&czrmAccountInfo) {
		sdcAccountInfo, err = ce.CreateUserOnSdc(&czrmAccountInfo)
		if err != nil {
			ce.logger.Error("CZRM-UPDATES-CASE -> error creating user on Sdc ", zap.Error(err))
			return err
		}
		ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> created user on SDC: ", sdcAccountInfo.ID)
		ce.updateSegnalaCiIdOnAccountOnCzrm(sdcAccountInfo.ID, czrmAccountInfo.ID)
	} else {
		sdcAccountInfo, err = ce.getAccountFromSdc(czrmAccountInfo.SegnalaCiIDC)
		if err != nil {
			ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving user from Sdc ", zap.Error(err))
			return err
		}
		ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> user found on SDC: ", sdcAccountInfo.ID)
	}
	application, err := ce.getOrCreateApplication(caseInfo, sdcAccountInfo)
	if err != nil || application.ID == "" {
		ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> Failed to retrieve or create application on sdc using case ID.")
		return err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> processing application: ", application.ID)
	/* if ce.isCaseClosedOrNotAssigned(caseInfo) {
		//se la pratica è chiusa o non assegnata non faccio niente e quindi non devo eseguire gli step sucessivi
		ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> case not closed or not assigned yet. Nothing more to do")
		return nil
	} else if caseInfo.StatoFrontEndC == "Chiuso" {
		//setApplicationToAccepted issue 8 e parte della 15
		officeid, err := ce.getOffice(caseInfo)
		if err != nil {
			return err
		}
		ce.changeApplicationStatus(officeid, application.ID)
		if err != nil {
			return err
		}
		ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> changed  status for application ", application.ID)
	} */
	return nil
}

func (ce *caseEntity) getCase() (czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, error) {
	var caseResponse czrmrequeststructs.GetCaseByCzrmIdSuccessResponse
	token, err := ce.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> AccessAuthTokenRequest returned an error ", zap.Error(err))
		return caseResponse, err
	}
	ce.czrmApi.AccessGetCaseByCzrmIdRequest().SetInputValues(ce.getGetCaseByCzrmInputvalues())
	caseResponse, _, err = ce.czrmApi.AccessGetCaseByCzrmIdRequest().GetCaseByCzrmIdRequest(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> GetCaseByCzrmIdRequest returned an error", zap.Error(err))
		return caseResponse, err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> GetCaseByCzrmIdRequest retrieved case correctly: ", caseResponse.ID)
	return caseResponse, nil
}

// todo mettere i valori di input
func (ce *caseEntity) getOrCreateApplication(caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, sdcAccountInfo sdcapistructs.UserInfo) (sdcapistructs.GetApplicationByExternalIdIdSuccessResponse, error) {
	application := sdcapistructs.GetApplicationByExternalIdIdSuccessResponse{}
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return application, err
	}
	application, statuscode, _ := ce.sdcApi.AccessGetApplicationByExternaIdRequest().GetApplicationByExternalIdId(token, caseInfo.SegnalaCiIDC)
	if statuscode == http.StatusNotFound {
		ce.logger.Debug("CZRM-UPDATES-CASE -> application not found on sdc. We need to create it")
		ce.CreateApplication(caseInfo, sdcAccountInfo)
		return application, err
	}
	return application, nil
}

func (ce *caseEntity) shouldCreateAccountOnSdc(accountinfo *czrmrequeststructs.GetAccountByCzrmIdSuccessResponse) bool {
	return ce.getFiscalCodeFromAccount(accountinfo) != "" && accountinfo.SegnalaCiIDC == ""
}

func (ce *caseEntity) getFiscalCodeFromAccount(accountinfo *czrmrequeststructs.GetAccountByCzrmIdSuccessResponse) string {
	if accountinfo.CodiceFiscaleC != "" {
		return accountinfo.CodiceFiscaleC
	}
	if accountinfo.CodiceFiscalePc != "" {
		return accountinfo.CodiceFiscaleC
	}
	return ""
}

// todo chiamare la create application. Dovrei farlo qui??
func (ce *caseEntity) CreateApplication(caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, sdcAccountInfo sdcapistructs.UserInfo) error {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return err
	}
	ce.sdcApi.AccessCreateApplication().SetInputValues(ce.getCreateApplicationInputValues(caseInfo, sdcAccountInfo))
	ce.sdcApi.AccessCreateApplication().CreateApplication(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error creating a new application from case and user", zap.Error(err))
		return err
	}
	return nil
}
func (ce *caseEntity) updateSegnalaCiIdOnAccountOnCzrm(sdcAccountId, czrmAccountId string) error {
	token, err := ce.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving auth token form sdc: ", zap.Error(err))
		return err
	}
	ce.czrmApi.AccessUpdateAccountSegnalaciIdRequest().SetInputValues(ce.getUpdateSegnalaCiIdCzrmAccountInputValues(sdcAccountId))
	_, err = ce.czrmApi.AccessUpdateAccountSegnalaciIdRequest().UpdateAccountSegnalaciId(token, czrmAccountId)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error trying to update segnalaciId__c field on czrm: ", zap.Error(err))
		return err
	}
	return nil
}
func (ce *caseEntity) getUpdateSegnalaCiIdCzrmAccountInputValues(sdcAccountId string) czrmrequeststructs.UpdateAccountInputValues {
	return czrmrequeststructs.UpdateAccountInputValues{
		SdcAccountId: sdcAccountId,
	}
}

func (ce *caseEntity) getAccountFromSdc(sdcAccountId string) (sdcapistructs.UserInfo, error) {
	sdcUserInfo := sdcapistructs.UserInfo{}
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return sdcUserInfo, err
	}
	inputValues := sdcapistructs.GetUserByIdInputValues{
		UserId: sdcAccountId,
	}
	ce.sdcApi.AccessGetuserById().SetInputValues(inputValues)
	sdcUserInfo, err = ce.sdcApi.AccessGetuserById().GetUserById(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc user", zap.Error(err))
		return sdcUserInfo, err
	}
	return sdcUserInfo, nil

}

// todo nominatim??? todo type label
func (ce *caseEntity) getCreateApplicationInputValues(caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, sdcAccountInfo sdcapistructs.UserInfo) sdcapistructs.CreateApplicationInputValues {
	return sdcapistructs.CreateApplicationInputValues{
		ExternalID: caseInfo.ID,
		Service:    "",
		Status:     "2000",
		CreatedAt:  caseInfo.CreatedDate,
		Data: sdcapistructs.Data{
			AddressLat:                caseInfo.IndirizzoLatitudeS,
			AddressLon:                caseInfo.IndirizzoLongitudeS,
			AddressAddressCountry:     caseInfo.IndirizzoC.Country,
			AddressAddressCountryCode: caseInfo.IndirizzoC.CountryCode,
			AddressAddressCounty:      caseInfo.IndirizzoC.State,
			AddressAddressPostcode:    caseInfo.IndirizzoC.PostalCode,
			AddressAddressRoad:        caseInfo.IndirizzoC.Street,
			AddressAddressCity:        caseInfo.IndirizzoC.City,
			//town??
			ApplicantDataEmailAddress:             ce.getFirstNotEmpty(sdcAccountInfo.Email, caseInfo.ContactEmail, caseInfo.AccountEmailSegnalaCiC, caseInfo.SuppliedEmail),
			ApplicantDataPhoneNumber:              ce.getFirstNotEmpty(sdcAccountInfo.Cellulare, caseInfo.ContactPhone, caseInfo.SuppliedPhone),
			ApplicantDataCompletenameDataName:     sdcAccountInfo.Nome,
			ApplicantDataCompletenameDataSurname:  sdcAccountInfo.Cognome,
			ApplicantDataFiscalCodeDataFiscalCode: ce.getFirstNotEmpty(sdcAccountInfo.CodiceFiscale, caseInfo.CodiceFiscaleC, caseInfo.UserC),
			ApplicantDataPersonIdentifier:         ce.getFirstNotEmpty(sdcAccountInfo.CodiceFiscale, caseInfo.CodiceFiscaleC, caseInfo.UserC),
			TypeLabel:                             "",
			TypeValue:                             "",
			Details:                               caseInfo.Description,
			Subject:                               caseInfo.Subject,
		},
		User: sdcAccountInfo.ID,
	}
}
func (ce *caseEntity) getFirstNotEmpty(inputs ...string) string {
	for _, input := range inputs {
		if input != "" {
			return input
		}
	}
	return ""
}
func (ce *caseEntity) isCaseClosedOrNotAssigned(caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse) bool {
	return caseInfo.StatoFrontEndC == "Chiuso" && caseInfo.DirezioneUfficioC != ""
}
func (ce *caseEntity) getCzrmAccount(userId string) (czrmrequeststructs.GetAccountByCzrmIdSuccessResponse, error) {
	var czrmAccountInfo czrmrequeststructs.GetAccountByCzrmIdSuccessResponse
	token, err := ce.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> AccessAuthTokenRequest returned an error for user topic ", zap.Error(err))
		return czrmAccountInfo, err
	}
	var statuscode int

	getaccountInputValues := czrmrequeststructs.GetAccountByCzrmIdInputValues{
		User: userId,
	}
	ce.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().SetInputValues(getaccountInputValues)
	czrmAccountInfo, statuscode, err = ce.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().GetAccountByCzrmIdRequest(token)
	if err != nil || statuscode != http.StatusOK {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving Czrm Account from CzRM: ", zap.Error(err))
		return czrmAccountInfo, err
	}
	return czrmAccountInfo, nil
}

func (ce *caseEntity) CreateUserOnSdc(accountinfo *czrmrequeststructs.GetAccountByCzrmIdSuccessResponse) (sdcapistructs.UserInfo, error) {
	var sdcAccountInfo sdcapistructs.UserInfo
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving auth token form sdc: ", zap.Error(err))
		return sdcAccountInfo, err
	}
	ce.sdcApi.AccessCreateUserRequest().SetInputValues(ce.getCreateSdcUserInputalues(accountinfo))
	sdcAccountInfo, err = ce.sdcApi.AccessCreateUserRequest().CreateUser(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error creating new user on sdc:", zap.Error(err))
		return sdcAccountInfo, err
	}
	return sdcAccountInfo, nil
}

// todo external id
func (ce *caseEntity) getCreateSdcUserInputalues(accountinfo *czrmrequeststructs.GetAccountByCzrmIdSuccessResponse) sdcapistructs.CreateUserInputValues {
	return sdcapistructs.CreateUserInputValues{
		Nome:               accountinfo.FirstName,
		Cognome:            accountinfo.LastName,
		Cellulare:          accountinfo.Phone,
		Email:              accountinfo.PersonEmail,
		CodiceFiscale:      accountinfo.CodiceFiscaleC,
		DataNascita:        accountinfo.PersonBirthdate,
		Telefono:           accountinfo.Phone,
		IndirizzoDomicilio: accountinfo.ShippingStreet,
		CapDomicilio:       accountinfo.ShippingPostalCode,
		CittaDomicilio:     accountinfo.ShippingState,
		ProvinciaDomicilio: accountinfo.ShippingStateCode,
		StatoDomicilio:     accountinfo.ShippingCountry,
		IndirizzoResidenza: accountinfo.BillingAddress.Street,
		CapResidenza:       accountinfo.BillingAddress.PostalCode,
		CittaResidenza:     accountinfo.BillingAddress.City,
		ProvinciaResidenza: accountinfo.BillingAddress.StateCode,
		StatoResidenza:     accountinfo.BillingAddress.Country,
	}
}

// todo
func (ce *caseEntity) setApplicationToAccepted() error {
	return nil
}

// todo cancellare se non usate
func (ce *caseEntity) getOffice(caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse) (string, error) {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return "", err
	}
	userGroupsList, err := ce.sdcApi.AccessGetUserGroups().GetUserGroups(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc user groups list", zap.Error(err))
		return "", err
	}
	userGroupId := ce.getOrCreateOffice(userGroupsList, caseInfo)
	//Eseguo una POST /applications/{SegnalaciId__c}/transition/assign con payload(questa serve a fare il cambio di stato)
	return userGroupId, nil
}
func (ce *caseEntity) getOrCreateOffice(officesList sdcapistructs.GetUserGroupsSuccesResponse, caseInfo czrmrequeststructs.GetCaseByCzrmIdSuccessResponse) string {

	for _, userGroup := range officesList.GetUserGroupSuccesResponse {
		if userGroup.Name == caseInfo.DirezioneUfficioC {
			return userGroup.ID
		}
	}
	//office not present... We need to create it.
	officeId, err := ce.createOffice(caseInfo.DirezioneUfficioC)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error creating new sdc user-group", zap.Error(err))
		return ""
	}
	return officeId
}

func (ce *caseEntity) createOffice(DirezioneUfficioC string) (string, error) {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return "", err
	}
	inputValue := sdcapistructs.CreateUserGroupInputValues{
		Name: DirezioneUfficioC,
	}

	ce.sdcApi.AccessCreateUserGroup().SetInputValues(inputValue)
	userGroupId, err := ce.sdcApi.AccessCreateUserGroup().CreateUserGroup(token)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return "", err
	}
	return userGroupId.ID, nil
}
func (ce *caseEntity) changeApplicationStatus(userGroupId, applicationId string) error {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return err
	}
	inputValues := sdcapistructs.ApplicationChangeStatusInputValues{
		UserGroupID: userGroupId,
	}
	ce.sdcApi.AccessApplicationChangeStatus().SetInputValues(inputValues)
	result, err := ce.sdcApi.AccessApplicationChangeStatus().ApplicationChangeStatus(token, applicationId)
	if err != nil || !result {
		ce.logger.Error("CZRM-UPDATES-CASE -> error changing application status", zap.Error(err))
		return err
	}
	return nil
}
func (ce *caseEntity) getGetCaseByCzrmInputvalues() czrmrequeststructs.GetCaseByCzrmIdInputValues {
	return czrmrequeststructs.GetCaseByCzrmIdInputValues{
		CaseId: ce.message.ID,
	}
}

// todo cancellare se non usate
func (ce *caseEntity) getOffice2(c *czrmrequeststructs.GetCaseByCzrmIdSuccessResponse) (string, error) {
	serviceId, err := ce.getServiceId(c.SegnalaCiIDC)
	if err != nil {
		return "", err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> serviceId case ", serviceId)
	officeName, err := ce.getOfficeName(serviceId)
	if err != nil {
		return "", err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-CASE -> officeName usergroup ", officeName)
	return officeName, nil
}
func (ce *caseEntity) getServiceId(applicationId string) (string, error) {
	var application sdcapistructs.GetApplicationByApplicationIdSuccessResponse

	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return "", err
	}
	ce.sdcApi.AccessGetApplicationByApplicationIdRequest().SetInputValues(ce.getGetApplicationByApplicationIdInputvalues(applicationId))

	application, err = ce.sdcApi.AccessGetApplicationByApplicationIdRequest().GetApplicationByApplicationId(token)

	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error performing GetApplicationByApplicationId", zap.Error(err))
		return "", err
	}
	return application.ServiceID, nil
}
func (ce *caseEntity) getOfficeName(serviceId string) (string, error) {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-CASE -> error retrieving sdc auth token", zap.Error(err))
		return "", err
	}
	ce.sdcApi.AccessGetUserGroupByServiceId().SetInputValues(ce.getGetUserGroupByServiceIdInputvalues(serviceId))
	userGroup, err := ce.sdcApi.AccessGetUserGroupByServiceId().GetUserGroupByServiceId(token)
	if err != nil {
		return "", err
	}
	return userGroup.Name, nil
}

func (ce *caseEntity) getGetUserGroupByServiceIdInputvalues(s string) sdcapistructs.GetUserGroupByServiceIdInputValues {
	return sdcapistructs.GetUserGroupByServiceIdInputValues{
		ServiceId: s,
	}

}
func (ce *caseEntity) getGetApplicationByApplicationIdInputvalues(applicationId string) sdcapistructs.GetApplicationByApplicationIdInputValues {
	return sdcapistructs.GetApplicationByApplicationIdInputValues{
		ApplicationId: applicationId,
	}
}
