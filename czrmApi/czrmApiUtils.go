package czrmapi

import (
	"fmt"
	"strconv"
	"strings"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

//temporary fix for issue #39
/*
	 func (cz *czrmApi) GetLon(strLon string) float64 {
		if strLon == "" {
			return 0
		}
		lon, err := strconv.ParseFloat(strLon, 64)
		if err != nil {
			cz.logger.Error("Error during lon conversion", zap.Error(err))
			return 0
		}
		return lon
	}
*/
/*
	 func (cz *czrmApi) GetLat(strLat string) float64 {
		if strLat == "" {
			return 0
		}
		lat, err := strconv.ParseFloat(strLat, 64)
		if err != nil {
			cz.logger.Error("Error during lat conversion", zap.Error(err))
			return 0
		}
		return lat
	}
*/
func (cz *czrmApi) GetLon(strLon interface{}) float64 {
	switch strLon := strLon.(type) {
	case string:
		lon, err := strconv.ParseFloat(strLon, 64)
		if err != nil {
			cz.logger.Error("Error during lon conversion", zap.Error(err))
			return 0
		}
		return lon
	case float64:
		return strLon
	default:
		cz.logger.Warn(fmt.Sprintf("Invalid  longitude input type: %T", strLon))
		return 0
	}
}
func (cz *czrmApi) GetLat(strLat interface{}) float64 {
	switch strLat := strLat.(type) {
	case string:
		lat, err := strconv.ParseFloat(strLat, 64)
		if err != nil {
			cz.logger.Error("Error during lat conversion", zap.Error(err))
			return 0
		}
		return lat
	case float64:
		return strLat
	default:
		cz.logger.Error(fmt.Sprintf("Invalid latitude input type: %T", strLat))
		return 0
	}
}

func (cz *czrmApi) GetValidOrNullfiscalCode(fiscalCode string) string {
	if len(fiscalCode) != 16 {
		return ""
	}
	return fiscalCode
}
func (cz *czrmApi) GetCategoriaOperator(str string) string {
	// Find the index of ": " in the string
	index := strings.Index(str, ": ")

	// If ": " is not found or it appears at the end of the string, return an empty string
	if index == -1 || index+2 >= len(str) {
		return ""
	}

	// Extract the substring after ": "
	result := str[index+2:]

	return result
}

func (cz *czrmApi) GetMunicipioName(areas []kafkaMessagesStruct.GeographicAreas) string {
	if len(areas) > 0 {
		return areas[0].Name
	} else {
		return ""
	}
}

func (cz *czrmApi) GetUserPhone(mobilephone, phone string) string {
	if mobilephone != "" {
		return mobilephone
	} else if phone != "" {
		return phone
	}
	return ""
}
