package czrmrequeststructs

type GetAccountByCzrmIdInputValues struct {
	User string
}

type GetAccountByCzrmIdResponse struct {
	GetAccountByCzrmIdSuccessResponse GetAccountByCzrmIdSuccessResponse
	GetAccountByCzrmIdBadResponse     []GetAccountByCzrmIdBadResponse
}

type GetAccountByCzrmIdSuccessResponse struct {
	Attributes struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	} `json:"attributes"`
	ID                     string `json:"Id"`
	IsDeleted              bool   `json:"IsDeleted"`
	MasterRecordID         string `json:"MasterRecordId"`
	Name                   string `json:"Name"`
	LastName               string `json:"LastName"`
	FirstName              string `json:"FirstName"`
	Salutation             string `json:"Salutation"`
	MiddleName             string `json:"MiddleName"`
	Suffix                 string `json:"Suffix"`
	Type                   string `json:"Type"`
	RecordTypeID           string `json:"RecordTypeId"`
	ParentID               string `json:"ParentId"`
	BillingStreet          string `json:"BillingStreet"`
	BillingCity            string `json:"BillingCity"`
	BillingState           string `json:"BillingState"`
	BillingPostalCode      string `json:"BillingPostalCode"`
	BillingCountry         string `json:"BillingCountry"`
	BillingStateCode       string `json:"BillingStateCode"`
	BillingCountryCode     string `json:"BillingCountryCode"`
	BillingLatitude        string `json:"BillingLatitude"`
	BillingLongitude       string `json:"BillingLongitude"`
	BillingGeocodeAccuracy string `json:"BillingGeocodeAccuracy"`
	BillingAddress         struct {
		City            string `json:"city"`
		Country         string `json:"country"`
		CountryCode     string `json:"countryCode"`
		GeocodeAccuracy string `json:"geocodeAccuracy"`
		Latitude        string `json:"latitude"`
		Longitude       string `json:"longitude"`
		PostalCode      string `json:"postalCode"`
		State           string `json:"state"`
		StateCode       string `json:"stateCode"`
		Street          string `json:"street"`
	} `json:"BillingAddress"`
	ShippingStreet          string `json:"ShippingStreet"`
	ShippingCity            string `json:"ShippingCity"`
	ShippingState           string `json:"ShippingState"`
	ShippingPostalCode      string `json:"ShippingPostalCode"`
	ShippingCountry         string `json:"ShippingCountry"`
	ShippingStateCode       string `json:"ShippingStateCode"`
	ShippingCountryCode     string `json:"ShippingCountryCode"`
	ShippingLatitude        string `json:"ShippingLatitude"`
	ShippingLongitude       string `json:"ShippingLongitude"`
	ShippingGeocodeAccuracy string `json:"ShippingGeocodeAccuracy"`
	ShippingAddress         struct {
		City            string `json:"city"`
		Country         string `json:"country"`
		CountryCode     string `json:"countryCode"`
		GeocodeAccuracy string `json:"geocodeAccuracy"`
		Latitude        string `json:"latitude"`
		Longitude       string `json:"longitude"`
		PostalCode      string `json:"postalCode"`
		State           string `json:"state"`
		StateCode       string `json:"stateCode"`
		Street          string `json:"street"`
	} `json:"ShippingAddress"`
	Phone                      string `json:"Phone"`
	Website                    string `json:"Website"`
	PhotoURL                   string `json:"PhotoUrl"`
	Industry                   string `json:"Industry"`
	NumberOfEmployees          string `json:"NumberOfEmployees"`
	Description                string `json:"Description"`
	OwnerID                    string `json:"OwnerId"`
	CreatedDate                string `json:"CreatedDate"`
	CreatedByID                string `json:"CreatedById"`
	LastModifiedDate           string `json:"LastModifiedDate"`
	LastModifiedByID           string `json:"LastModifiedById"`
	SystemModstamp             string `json:"SystemModstamp"`
	LastActivityDate           string `json:"LastActivityDate"`
	LastViewedDate             string `json:"LastViewedDate"`
	LastReferencedDate         string `json:"LastReferencedDate"`
	PersonContactID            string `json:"PersonContactId"`
	IsPersonAccount            bool   `json:"IsPersonAccount"`
	PersonMobilePhone          string `json:"PersonMobilePhone"`
	PersonEmail                string `json:"PersonEmail"`
	PersonBirthdate            string `json:"PersonBirthdate"`
	PersonLastCURequestDate    string `json:"PersonLastCURequestDate"`
	PersonLastCUUpdateDate     string `json:"PersonLastCUUpdateDate"`
	PersonEmailBouncedReason   string `json:"PersonEmailBouncedReason"`
	PersonEmailBouncedDate     string `json:"PersonEmailBouncedDate"`
	PersonIndividualID         string `json:"PersonIndividualId"`
	Jigsaw                     string `json:"Jigsaw"`
	JigsawCompanyID            string `json:"JigsawCompanyId"`
	AccountSource              string `json:"AccountSource"`
	SicDesc                    string `json:"SicDesc"`
	OperatingHoursID           string `json:"OperatingHoursId"`
	EmailC                     string `json:"Email__c"`
	CodiceFiscaleC             string `json:"Codice_Fiscale__c"`
	SegnalaCiIDC               string `json:"SegnalaCiId__c"`
	CodiceFiscalePc            string `json:"Codice_Fiscale__pc"`
	SegnalaCiIDPc              string `json:"SegnalaCiId__pc"`
	FlCreaSegnPc               bool   `json:"Fl_Crea_Segn__pc"`
	FlAssegnaSegnPc            bool   `json:"Fl_Assegna_Segn__pc"`
	FlInserisciCommentoPc      bool   `json:"Fl_Inserisci_Commento__pc"`
	FlChiudiSegnPc             bool   `json:"Fl_Chiudi_Segn__pc"`
	FlRiapriSegnPc             bool   `json:"Fl_Riapri_Segn__pc"`
	ConsensoPrivacySegnalaCiPc bool   `json:"ConsensoPrivacySegnalaCi__pc"`
}

type GetAccountByCzrmIdBadResponse struct {
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}
