package czrmrequeststructs

type GetCaseBySegnalaciIdc_cResponse struct {
	GetCaseByApplicationIdSuccessResponse GetCaseByApplicationIdSuccessResponse
	GetCaseByApplicationIdBadResponse     []GetCaseByApplicationIdBadResponse
}

type GetCaseByApplicationIdSuccessResponse struct {
	Attributes struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	} `json:"attributes"`
	ID                        string      `json:"Id"`
	IsDeleted                 bool        `json:"IsDeleted"`
	MasterRecordID            interface{} `json:"MasterRecordId"`
	CaseNumber                string      `json:"CaseNumber"`
	ContactID                 string      `json:"ContactId"`
	AccountID                 string      `json:"AccountId"`
	SuppliedName              interface{} `json:"SuppliedName"`
	SuppliedEmail             interface{} `json:"SuppliedEmail"`
	SuppliedPhone             interface{} `json:"SuppliedPhone"`
	Type                      interface{} `json:"Type"`
	RecordTypeID              string      `json:"RecordTypeId"`
	Status                    string      `json:"Status"`
	Reason                    interface{} `json:"Reason"`
	Origin                    string      `json:"Origin"`
	Subject                   string      `json:"Subject"`
	Description               string      `json:"Description"`
	IsClosed                  bool        `json:"IsClosed"`
	OwnerID                   string      `json:"OwnerId"`
	SLAExitDate               interface{} `json:"SlaExitDate"`
	CreatedDate               string      `json:"CreatedDate"`
	CreatedByID               string      `json:"CreatedById"`
	LastModifiedDate          string      `json:"LastModifiedDate"`
	LastModifiedByID          string      `json:"LastModifiedById"`
	SystemModstamp            string      `json:"SystemModstamp"`
	ContactPhone              string      `json:"ContactPhone"`
	ContactMobile             interface{} `json:"ContactMobile"`
	ContactEmail              string      `json:"ContactEmail"`
	ContactFax                interface{} `json:"ContactFax"`
	Comments                  interface{} `json:"Comments"`
	LastViewedDate            string      `json:"LastViewedDate"`
	LastReferencedDate        string      `json:"LastReferencedDate"`
	CodiceFiscaleC            interface{} `json:"Codice_Fiscale__c"`
	SuppliedFirstNameC        interface{} `json:"SuppliedFirstName__c"`
	PrivacyC                  bool        `json:"Privacy__c"`
	RispostaEsaustivaC        bool        `json:"Risposta_Esaustiva__c"`
	UserC                     interface{} `json:"User__c"`
	CategoriaC                string      `json:"Categoria__c"`
	MacrocategoriaSegnalaCiC  string      `json:"MacrocategoriaSegnalaCi__c"`
	SegnalaCiIDC              string      `json:"SegnalaCiId__c"`
	AppIoNotifiedC            bool        `json:"AppIo_Notified__c"`
	DataAperturaCaseC         interface{} `json:"Data_Apertura_Case__c"`
	DataChiusuraCaseC         interface{} `json:"Data_Chiusura_Case__c"`
	CodaPertinenzaC           interface{} `json:"CodaPertinenza__c"`
	MunicipioC                string      `json:"Municipio__c"`
	IndirizzoStreetS          string      `json:"Indirizzo__Street__s"`
	IndirizzoCityS            string      `json:"Indirizzo__City__s"`
	IndirizzoPostalCodeS      string      `json:"Indirizzo__PostalCode__s"`
	IndirizzoStateCodeS       interface{} `json:"Indirizzo__StateCode__s"`
	IndirizzoCountryCodeS     string      `json:"Indirizzo__CountryCode__s"`
	IndirizzoLatitudeS        float64     `json:"Indirizzo__Latitude__s"`
	IndirizzoLongitudeS       float64     `json:"Indirizzo__Longitude__s"`
	IndirizzoGeocodeAccuracyS interface{} `json:"Indirizzo__GeocodeAccuracy__s"`
	IndirizzoC                struct {
		City            string      `json:"city"`
		Country         string      `json:"country"`
		CountryCode     string      `json:"countryCode"`
		GeocodeAccuracy interface{} `json:"geocodeAccuracy"`
		Latitude        float64     `json:"latitude"`
		Longitude       float64     `json:"longitude"`
		PostalCode      string      `json:"postalCode"`
		State           interface{} `json:"state"`
		StateCode       interface{} `json:"stateCode"`
		Street          string      `json:"street"`
	} `json:"Indirizzo__c"`
	CategoriaDelCittadinoC           interface{} `json:"Categoria_del_Cittadino__c"`
	IsClosedFrom10DaysC              bool        `json:"IsClosedFrom10Days__c"`
	StatoFrontEndC                   string      `json:"Stato_Front_End__c"`
	NumCaseFigliConCommDaPubblicareC float64     `json:"NumCaseFigliConCommDaPubblicare__c"`
}

type GetCaseByApplicationIdBadResponse struct {
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}
