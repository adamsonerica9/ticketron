package czrmrequeststructs

type GetAccountBySdcAccountIdInputValues struct {
	User string
}

type GetAccountBySdcAccountIdResponse struct {
	AccountBySdcAccountIdSuccessResponse GetAccountBySdcAccountIdSuccessResponse
	AccountBySdcAccountIdBadResponse     []GetAccountBySdcAccountIdBadResponse
}

type GetAccountBySdcAccountIdSuccessResponse struct {
	Attributes struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	} `json:"attributes"`
	ID                     string      `json:"Id"`
	IsDeleted              bool        `json:"IsDeleted"`
	MasterRecordID         interface{} `json:"MasterRecordId"`
	Name                   string      `json:"Name"`
	LastName               interface{} `json:"LastName"`
	FirstName              interface{} `json:"FirstName"`
	Salutation             interface{} `json:"Salutation"`
	MiddleName             interface{} `json:"MiddleName"`
	Suffix                 interface{} `json:"Suffix"`
	Type                   interface{} `json:"Type"`
	RecordTypeID           string      `json:"RecordTypeId"`
	ParentID               interface{} `json:"ParentId"`
	BillingStreet          interface{} `json:"BillingStreet"`
	BillingCity            interface{} `json:"BillingCity"`
	BillingState           interface{} `json:"BillingState"`
	BillingPostalCode      interface{} `json:"BillingPostalCode"`
	BillingCountry         string      `json:"BillingCountry"`
	BillingStateCode       interface{} `json:"BillingStateCode"`
	BillingCountryCode     string      `json:"BillingCountryCode"`
	BillingLatitude        interface{} `json:"BillingLatitude"`
	BillingLongitude       interface{} `json:"BillingLongitude"`
	BillingGeocodeAccuracy interface{} `json:"BillingGeocodeAccuracy"`
	BillingAddress         struct {
		City            interface{} `json:"city"`
		Country         string      `json:"country"`
		CountryCode     string      `json:"countryCode"`
		GeocodeAccuracy interface{} `json:"geocodeAccuracy"`
		Latitude        interface{} `json:"latitude"`
		Longitude       interface{} `json:"longitude"`
		PostalCode      interface{} `json:"postalCode"`
		State           interface{} `json:"state"`
		StateCode       interface{} `json:"stateCode"`
		Street          interface{} `json:"street"`
	} `json:"BillingAddress"`
	ShippingStreet          string      `json:"ShippingStreet"`
	ShippingCity            interface{} `json:"ShippingCity"`
	ShippingState           string      `json:"ShippingState"`
	ShippingPostalCode      string      `json:"ShippingPostalCode"`
	ShippingCountry         string      `json:"ShippingCountry"`
	ShippingStateCode       string      `json:"ShippingStateCode"`
	ShippingCountryCode     string      `json:"ShippingCountryCode"`
	ShippingLatitude        interface{} `json:"ShippingLatitude"`
	ShippingLongitude       interface{} `json:"ShippingLongitude"`
	ShippingGeocodeAccuracy interface{} `json:"ShippingGeocodeAccuracy"`
	ShippingAddress         struct {
		City            interface{} `json:"city"`
		Country         string      `json:"country"`
		CountryCode     string      `json:"countryCode"`
		GeocodeAccuracy interface{} `json:"geocodeAccuracy"`
		Latitude        interface{} `json:"latitude"`
		Longitude       interface{} `json:"longitude"`
		PostalCode      string      `json:"postalCode"`
		State           string      `json:"state"`
		StateCode       string      `json:"stateCode"`
		Street          string      `json:"street"`
	} `json:"ShippingAddress"`
	Phone                      interface{} `json:"Phone"`
	Website                    interface{} `json:"Website"`
	PhotoURL                   string      `json:"PhotoUrl"`
	Industry                   interface{} `json:"Industry"`
	NumberOfEmployees          interface{} `json:"NumberOfEmployees"`
	Description                interface{} `json:"Description"`
	OwnerID                    string      `json:"OwnerId"`
	CreatedDate                string      `json:"CreatedDate"`
	CreatedByID                string      `json:"CreatedById"`
	LastModifiedDate           string      `json:"LastModifiedDate"`
	LastModifiedByID           string      `json:"LastModifiedById"`
	SystemModstamp             string      `json:"SystemModstamp"`
	LastActivityDate           interface{} `json:"LastActivityDate"`
	LastViewedDate             string      `json:"LastViewedDate"`
	LastReferencedDate         string      `json:"LastReferencedDate"`
	PersonContactID            interface{} `json:"PersonContactId"`
	IsPersonAccount            bool        `json:"IsPersonAccount"`
	PersonMobilePhone          interface{} `json:"PersonMobilePhone"`
	PersonEmail                interface{} `json:"PersonEmail"`
	PersonBirthdate            interface{} `json:"PersonBirthdate"`
	PersonLastCURequestDate    interface{} `json:"PersonLastCURequestDate"`
	PersonLastCUUpdateDate     interface{} `json:"PersonLastCUUpdateDate"`
	PersonEmailBouncedReason   interface{} `json:"PersonEmailBouncedReason"`
	PersonEmailBouncedDate     interface{} `json:"PersonEmailBouncedDate"`
	PersonIndividualID         interface{} `json:"PersonIndividualId"`
	Jigsaw                     interface{} `json:"Jigsaw"`
	JigsawCompanyID            interface{} `json:"JigsawCompanyId"`
	AccountSource              interface{} `json:"AccountSource"`
	SicDesc                    interface{} `json:"SicDesc"`
	OperatingHoursID           interface{} `json:"OperatingHoursId"`
	EmailC                     string      `json:"Email__c"`
	CodiceFiscaleC             string      `json:"Codice_Fiscale__c"`
	SegnalaCiIDC               string      `json:"SegnalaCiId__c"`
	CodiceFiscalePc            interface{} `json:"Codice_Fiscale__pc"`
	SegnalaCiIDPc              interface{} `json:"SegnalaCiId__pc"`
	FlCreaSegnPc               bool        `json:"Fl_Crea_Segn__pc"`
	FlAssegnaSegnPc            bool        `json:"Fl_Assegna_Segn__pc"`
	FlInserisciCommentoPc      bool        `json:"Fl_Inserisci_Commento__pc"`
	FlChiudiSegnPc             bool        `json:"Fl_Chiudi_Segn__pc"`
	FlRiapriSegnPc             bool        `json:"Fl_Riapri_Segn__pc"`
	ConsensoPrivacySegnalaCiPc bool        `json:"ConsensoPrivacySegnalaCi__pc"`
}

type GetAccountBySdcAccountIdBadResponse struct {
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}
