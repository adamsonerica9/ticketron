package czrmrequeststructs

type GetAccountByFiscalCodeSuccessResponse struct {
	TotalSize int  `json:"totalSize"`
	Done      bool `json:"done"`
	Records   []struct {
		Attributes struct {
			Type string `json:"type"`
			URL  string `json:"url"`
		} `json:"attributes"`
		ID string `json:"Id"`
	} `json:"records"`
}
