package czrmrequeststructs

type AuthTokenResponse struct {
	AuthTokenBadResponse     AuthTokenBadResponse
	AuthTokenSuccessResponse AuthTokenSuccessResponse
}
type AuthTokenBadResponse struct {
	RespErr string `json:"error"`
	Errdesc string `json:"error_description"`
}
type AuthTokenSuccessResponse struct {
	AccessToken string `json:"access_token"`
	InstanceURL string `json:"instance_url"`
	ID          string `json:"id"`
	TokenType   string `json:"token_type"`
	IssuedAt    string `json:"issued_at"`
	Signature   string `json:"signature"`
}

// WSO2
type Wso2AuthTokenSuccessResponse struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
}
