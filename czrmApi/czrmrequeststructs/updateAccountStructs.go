package czrmrequeststructs

type UpdateAccountInputValues struct {
	FirstName          string
	LastName           string
	Fiscalcode         string
	Email              string
	ShippingStreet     string
	ShippingState      string
	ShippingPostalCode string
	ShippingCountry    string
	SdcAccountId       string
	Phone              string
}

type UpdateAccountResponse struct {
	UpdateAccountBadResponse []UpdateAccountBadResponse
}

type UpdateAccountBadResponse struct {
	Message   string        `json:"message"`
	ErrorCode string        `json:"errorCode"`
	Fields    []interface{} `json:"fields"`
}
