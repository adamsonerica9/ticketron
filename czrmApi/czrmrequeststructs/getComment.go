package czrmrequeststructs

type GetCommentSuccessResponse struct {
	Attributes struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	} `json:"attributes"`
	ID                 string `json:"Id"`
	IsDeleted          bool   `json:"IsDeleted"`
	Name               string `json:"Name"`
	RecordTypeID       string `json:"RecordTypeId"`
	CreatedDate        string `json:"CreatedDate"`
	CreatedByID        string `json:"CreatedById"`
	LastModifiedDate   string `json:"LastModifiedDate"`
	LastModifiedByID   string `json:"LastModifiedById"`
	SystemModstamp     string `json:"SystemModstamp"`
	LastViewedDate     string `json:"LastViewedDate"`
	LastReferencedDate string `json:"LastReferencedDate"`
	CaseIDC            string `json:"CaseId__c"`
	DescriptionC       string `json:"Description__c"`
	FonteC             string `json:"Fonte__c"`
	TipoCommentoC      string `json:"Tipo_Commento__c"`
	CommentoOwnerC     string `json:"Commento_Owner__c"`
	AutoreCommentoC    string `json:"AutoreCommento__c"`
	CaseOwnerC         string `json:"CaseOwner__c"`
	CaseSegnalaCiIDC   string `json:"CaseSegnalaCiId__c"`
}
