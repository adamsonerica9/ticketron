package czrmrequeststructs

type GetCaseByCzrmIdInputValues struct {
	CaseId string
}

type GetCaseByCzrmIdResponse struct {
	GetCaseByCzrmIdSuccessResponse GetCaseByCzrmIdSuccessResponse
	GetCaseByCzrmIdBadResponse     []GetCaseByCzrmIdBadResponse
}

type GetCaseByCzrmIdSuccessResponse struct {
	Attributes struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	} `json:"attributes"`
	ID                        string      `json:"Id"`
	IsDeleted                 bool        `json:"IsDeleted"`
	MasterRecordID            interface{} `json:"MasterRecordId"`
	CaseNumber                string      `json:"CaseNumber"`
	ContactID                 interface{} `json:"ContactId"`
	AccountID                 string      `json:"AccountId"`
	AssetID                   interface{} `json:"AssetId"`
	ProductID                 interface{} `json:"ProductId"`
	EntitlementID             interface{} `json:"EntitlementId"`
	SourceID                  interface{} `json:"SourceId"`
	BusinessHoursID           string      `json:"BusinessHoursId"`
	ParentID                  interface{} `json:"ParentId"`
	SuppliedName              interface{} `json:"SuppliedName"`
	SuppliedEmail             string      `json:"SuppliedEmail"`
	SuppliedPhone             string      `json:"SuppliedPhone"`
	SuppliedCompany           interface{} `json:"SuppliedCompany"`
	Type                      interface{} `json:"Type"`
	RecordTypeID              string      `json:"RecordTypeId"`
	Status                    string      `json:"Status"`
	Reason                    interface{} `json:"Reason"`
	Origin                    string      `json:"Origin"`
	Language                  interface{} `json:"Language"`
	Subject                   string      `json:"Subject"`
	Priority                  interface{} `json:"Priority"`
	Description               string      `json:"Description"`
	IsClosed                  bool        `json:"IsClosed"`
	ClosedDate                interface{} `json:"ClosedDate"`
	IsEscalated               bool        `json:"IsEscalated"`
	OwnerID                   string      `json:"OwnerId"`
	IsClosedOnCreate          bool        `json:"IsClosedOnCreate"`
	SLAStartDate              interface{} `json:"SlaStartDate"`
	SLAExitDate               interface{} `json:"SlaExitDate"`
	IsStopped                 bool        `json:"IsStopped"`
	StopStartDate             interface{} `json:"StopStartDate"`
	CreatedDate               string      `json:"CreatedDate"`
	CreatedByID               string      `json:"CreatedById"`
	LastModifiedDate          string      `json:"LastModifiedDate"`
	LastModifiedByID          string      `json:"LastModifiedById"`
	SystemModstamp            string      `json:"SystemModstamp"`
	ContactPhone              string      `json:"ContactPhone"`
	ContactMobile             string      `json:"ContactMobile"`
	ContactEmail              string      `json:"ContactEmail"`
	ContactFax                interface{} `json:"ContactFax"`
	Comments                  interface{} `json:"Comments"`
	LastViewedDate            string      `json:"LastViewedDate"`
	LastReferencedDate        string      `json:"LastReferencedDate"`
	ServiceContractID         interface{} `json:"ServiceContractId"`
	MilestoneStatus           string      `json:"MilestoneStatus"`
	CodiceFiscaleC            string      `json:"Codice_Fiscale__c"`
	OrigineCaseC              interface{} `json:"OrigineCase__c"`
	OwnerNotMeC               bool        `json:"OwnerNotMe__c"`
	SuppliedFirstNameC        interface{} `json:"SuppliedFirstName__c"`
	MacrocategoriaC           interface{} `json:"Macrocategoria__c"`
	PrivacyC                  bool        `json:"Privacy__c"`
	SottofascicoloC           interface{} `json:"Sottofascicolo__c"`
	ServizioC                 interface{} `json:"Servizio__c"`
	CustomLookupFieldC        float64     `json:"Custom_Lookup_Field__c"`
	NoteCasoC                 interface{} `json:"Note_Caso__c"`
	TecnicalCodeC             string      `json:"Tecnical_Code__c"`
	TechnicalCaseOwnerC       string      `json:"Technical_Case_Owner__c"`
	TecNameC                  interface{} `json:"TecName__c"`
	RispostaEsaustivaC        bool        `json:"Risposta_Esaustiva__c"`
	TipologiaRichiestaC       interface{} `json:"TipologiaRichiesta__c"`
	UserC                     string      `json:"User__c"`
	CategoriaC                string      `json:"Categoria__c"`
	MacrocategoriaSegnalaCiC  interface{} `json:"MacrocategoriaSegnalaCi__c"`
	SegnalaCiIDC              string      `json:"SegnalaCiId__c"`
	AppIoNotifiedC            bool        `json:"AppIo_Notified__c"`
	DataAperturaCaseC         interface{} `json:"Data_Apertura_Case__c"`
	DataChiusuraCaseC         interface{} `json:"Data_Chiusura_Case__c"`
	CodaPertinenzaC           interface{} `json:"CodaPertinenza__c"`
	MunicipioC                interface{} `json:"Municipio__c"`
	IndirizzoStreetS          interface{} `json:"Indirizzo__Street__s"`
	IndirizzoCityS            interface{} `json:"Indirizzo__City__s"`
	IndirizzoPostalCodeS      interface{} `json:"Indirizzo__PostalCode__s"`
	IndirizzoStateCodeS       interface{} `json:"Indirizzo__StateCode__s"`
	IndirizzoCountryCodeS     string      `json:"Indirizzo__CountryCode__s"`
	IndirizzoLatitudeS        float64     `json:"Indirizzo__Latitude__s"`
	IndirizzoLongitudeS       float64     `json:"Indirizzo__Longitude__s"`
	IndirizzoGeocodeAccuracyS interface{} `json:"Indirizzo__GeocodeAccuracy__s"`
	IndirizzoC                struct {
		City            string      `json:"city"`
		Country         string      `json:"country"`
		CountryCode     string      `json:"countryCode"`
		GeocodeAccuracy interface{} `json:"geocodeAccuracy"`
		Latitude        float64     `json:"latitude"`
		Longitude       float64     `json:"longitude"`
		PostalCode      string      `json:"postalCode"`
		State           string      `json:"state"`
		StateCode       string      `json:"stateCode"`
		Street          string      `json:"street"`
	} `json:"Indirizzo__c"`
	CategoriaDelCittadinoC        string      `json:"Categoria_del_Cittadino__c"`
	IsClosedFrom10DaysC           bool        `json:"IsClosedFrom10Days__c"`
	StatoFrontEndC                string      `json:"Stato_Front_End__c"`
	NumCommDaPubblicareC          float64     `json:"NumCommDaPubblicare__c"`
	AccountEmailSegnalaCiC        string      `json:"Account_Email_SegnalaCi__c"`
	DirezioneUfficioC             string      `json:"Direzione_Ufficio__c"`
	CategoriaBackendC             string      `json:"Categoria_Backend__c"`
	TrovaMunicipioC               interface{} `json:"Trova_Municipio__c"`
	RiepilogoCaseMigratoC         interface{} `json:"Riepilogo_Case_Migrato__c"`
	CategoriaDelCittadinoBackendC string      `json:"Categoria_del_Cittadino_Backend__c"`
}

type GetCaseByCzrmIdBadResponse struct {
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}
