package czrmrequeststructs

type CreateCommentInputValues struct {
	CaseIDC           string `json:"CaseId__c"`
	DescriptionC      string `json:"Description__c"`
	AutoreCommento__c string `json:"AutoreCommento__c"`
}

type CreateCommentResponse struct {
	CreateCommentSuccessResponse CreateCommentSuccessResponse
	CreateCommentBadResponse     []CreateCommentBadResponse
}

type CreateCommentSuccessResponse struct {
	ID      string        `json:"id"`
	Success bool          `json:"success"`
	Errors  []interface{} `json:"errors"`
}
type CreateCommentBadResponse struct {
	Message   string        `json:"message"`
	ErrorCode string        `json:"errorCode"`
	Fields    []interface{} `json:"fields"`
}
