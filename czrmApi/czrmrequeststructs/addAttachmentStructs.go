package czrmrequeststructs

type AddAttachmentInputValues struct {
	Title                  string `json:"Title"`
	PathOnClient           string `json:"PathOnClient"`
	FirstPublishLocationid string `json:"firstPublishLocationid"`
	VersionData            string `json:"VersionData"`
}

type AddAttachmentResponse struct {
	AddAttachmentSuccessResponse AddAttachmentSuccessResponse
	AddAttachmentBadResponse     []AddAttachmentBadResponse
}

type AddAttachmentSuccessResponse struct {
	ID      string        `json:"id"`
	Success bool          `json:"success"`
	Errors  []interface{} `json:"errors"`
}
type AddAttachmentBadResponse struct {
	Message   string        `json:"message"`
	ErrorCode string        `json:"errorCode"`
	Fields    []interface{} `json:"fields"`
}
