package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type ICreateCaseRequest interface {
	SetInputValues(input requeststructs.CreateCaseinputValues)
	buildRequestByOperator(accountId string) (*http.Request, error)
	buildRequestByCittadino(accountId string) (*http.Request, error)
	buildRequestWithoutCategories(accountId string) (*http.Request, error)
	doCreateCaseRequest(accountId string) (int, error)
	handleCreateCaseResponse(resp *http.Response) error
	CreateNewCase(t, accountId string) (string, int, error)
}

type createCaseRequest struct {
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.CreateCaseResponse
	accessToken    string
	inputValues    requeststructs.CreateCaseinputValues
}

func NewCreateCase(log *zap.Logger, config *config.Config) ICreateCaseRequest {
	cc := &createCaseRequest{
		logger: log,
		conf:   config,
	}
	cc.client = &http.Client{}
	return cc
}

func (cc *createCaseRequest) SetInputValues(input requeststructs.CreateCaseinputValues) {
	cc.inputValues = input
}
func (cc *createCaseRequest) buildRequestByOperator(accountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Case"
	endpoint := cc.conf.Czrm.CzrmBaseName
	version := cc.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]interface{}{
		"Origin":      cc.inputValues.Origin,
		"Subject":     cc.inputValues.Subject,
		"Description": cc.inputValues.Description,
		"Privacy__c":  cc.inputValues.PrivacyC,
		"Categoria__r": map[string]string{
			"Name": cc.inputValues.CategoriaR,
		},
		"AccountId":                 accountId,
		"SegnalaCiId__C":            cc.inputValues.ApplicationId,
		"Municipio__c":              cc.inputValues.Municipio,
		"Indirizzo__Street__s":      cc.inputValues.Address,
		"Indirizzo__City__s":        cc.inputValues.City,
		"Indirizzo__PostalCode__s":  cc.inputValues.PostCode,
		"Indirizzo__Latitude__s":    cc.inputValues.IndirizzoLatitudeS,
		"Indirizzo__Longitude__s":   cc.inputValues.IndirizzoLongitudeS,
		"Data_Apertura_Case__c":     cc.inputValues.CreatedAt,
		"Type":                      "segnalazione",
		"Riepilogo_Case_Migrato__c": cc.inputValues.PdfLink,
		"ContactId":                 cc.inputValues.ContactId,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		cc.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	cc.logger.Sugar().Debug("createCaseRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		cc.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", cc.accessToken))
	return req, nil
}
func (cc *createCaseRequest) buildRequestByCittadino(accountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Case"
	endpoint := cc.conf.Czrm.CzrmBaseName
	version := cc.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]interface{}{
		"Origin":      cc.inputValues.Origin,
		"Subject":     cc.inputValues.Subject,
		"Description": cc.inputValues.Description,
		"Privacy__c":  cc.inputValues.PrivacyC,
		"Categoria_del_Cittadino__r": map[string]string{
			"Name": cc.inputValues.CategoriaCittadino,
		},
		"AccountId":                 accountId,
		"SegnalaCiId__C":            cc.inputValues.ApplicationId,
		"Municipio__c":              cc.inputValues.Municipio,
		"Indirizzo__Street__s":      cc.inputValues.Address,
		"Indirizzo__City__s":        cc.inputValues.City,
		"Indirizzo__PostalCode__s":  cc.inputValues.PostCode,
		"Indirizzo__Latitude__s":    cc.inputValues.IndirizzoLatitudeS,
		"Indirizzo__Longitude__s":   cc.inputValues.IndirizzoLongitudeS,
		"Data_Apertura_Case__c":     cc.inputValues.CreatedAt,
		"Type":                      "segnalazione",
		"Riepilogo_Case_Migrato__c": cc.inputValues.PdfLink,
		"ContactId":                 cc.inputValues.ContactId,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		cc.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	cc.logger.Sugar().Debug("createCaseRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		cc.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", cc.accessToken))
	return req, nil
}
func (cc *createCaseRequest) buildRequestWithoutCategories(accountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Case"
	endpoint := cc.conf.Czrm.CzrmBaseName
	version := cc.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]interface{}{
		"Origin":                    cc.inputValues.Origin,
		"Subject":                   cc.inputValues.Subject,
		"Description":               cc.inputValues.Description,
		"Privacy__c":                cc.inputValues.PrivacyC,
		"AccountId":                 accountId,
		"SegnalaCiId__C":            cc.inputValues.ApplicationId,
		"Municipio__c":              cc.inputValues.Municipio,
		"Indirizzo__Street__s":      cc.inputValues.Address,
		"Indirizzo__City__s":        cc.inputValues.City,
		"Indirizzo__PostalCode__s":  cc.inputValues.PostCode,
		"Indirizzo__Latitude__s":    cc.inputValues.IndirizzoLatitudeS,
		"Indirizzo__Longitude__s":   cc.inputValues.IndirizzoLongitudeS,
		"Data_Apertura_Case__c":     cc.inputValues.CreatedAt,
		"Type":                      "segnalazione",
		"Riepilogo_Case_Migrato__c": cc.inputValues.PdfLink,
		"ContactId":                 cc.inputValues.ContactId,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		cc.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	cc.logger.Sugar().Debug("createCaseRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		cc.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", cc.accessToken))
	return req, nil
}
func (cc *createCaseRequest) doCreateCaseRequest(accountId string) (int, error) {
	var req *http.Request
	var err error

	if cc.inputValues.CategoriaR != "" {
		req, err = cc.buildRequestByOperator(accountId)
	} else if cc.inputValues.CategoriaCittadino != "" {
		req, err = cc.buildRequestByCittadino(accountId)
	} else if cc.inputValues.CategoriaR == "" && cc.inputValues.CategoriaCittadino == "" {
		req, err = cc.buildRequestWithoutCategories(accountId)
	}
	if err != nil {
		return -1, err
	}
	// make the request
	resp, err := cc.client.Do(req)

	if err != nil {
		cc.logger.Error("error performing  CreateCase request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = cc.handleCreateCaseResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (cc *createCaseRequest) handleCreateCaseResponse(resp *http.Response) error {
	cc.logger.Sugar().Debug("CreateCase status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			cc.logger.Error("error decoding body CreateCase response: ", zap.Error(err))
		}

		cc.logger.Sugar().Debug("bad CreateCase response: ", string(bodyBytes))
		return errors.New("CreateCase bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&cc.responseValues.CreateCaseSuccessResponse)
	if err != nil {
		cc.logger.Error("error decoding body CreateCase response: ", zap.Error(err))
		return err
	}
	return nil
}
func (cc *createCaseRequest) CreateNewCase(t, accountId string) (string, int, error) {
	cc.accessToken = t
	statuscode, err := cc.doCreateCaseRequest(accountId)
	if err != nil {
		return "", statuscode, err
	}
	defer cc.cleanResources()
	return cc.responseValues.CreateCaseSuccessResponse.ID, statuscode, nil
}

func (cc *createCaseRequest) cleanResources() {
	cc.logger.Sugar().Debug("createCaseRequest: cleaning resources after response")
	cc.responseValues = requeststructs.CreateCaseResponse{}
}

func (cc *createCaseRequest) isCategoryWrong(category string) bool {

	category = strings.ReplaceAll(category, " ", "")
	parts := strings.Split(category, ":")
	if len(parts) != 2 {
		return false
	}
	macrocategory := parts[0]
	microcategory := parts[1]
	return macrocategory == microcategory
}
