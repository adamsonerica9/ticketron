package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IUpdateAccountSegnalaciIdRequest interface {
	SetAccesstoken(a string)
	UpdateAccountSegnalaciId(t string, czrmAccountId string) (int, error)
	SetInputValues(input requeststructs.UpdateAccountInputValues)
	buildRequest(czrmAccountId string) (*http.Request, error)
	doUpdateAccountSegnalaciId(czrmAccountId string) (int, error)
	handleUpdateAccountSegnalaciIdResponse(resp *http.Response) error
}

type updateAccountSegnalaciIdRequest struct {
	inputValues    requeststructs.UpdateAccountInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.UpdateAccountResponse
	accessToken    string
}

func NewUpdateAccountSegnalaciIdRequest(log *zap.Logger, config *config.Config) IUpdateAccountSegnalaciIdRequest {
	ca := &updateAccountSegnalaciIdRequest{
		logger: log,
		conf:   config,
	}
	ca.client = &http.Client{}
	return ca
}

func (ua *updateAccountSegnalaciIdRequest) SetAccesstoken(a string) {
	ua.accessToken = a
}
func (ua *updateAccountSegnalaciIdRequest) SetInputValues(input requeststructs.UpdateAccountInputValues) {
	ua.inputValues = input
}
func (ua *updateAccountSegnalaciIdRequest) UpdateAccountSegnalaciId(t string, czrmAccountId string) (int, error) {
	ua.accessToken = t
	statuscode, err := ua.doUpdateAccountSegnalaciId(czrmAccountId)
	if err != nil {
		return statuscode, err
	}
	return statuscode, nil
}
func (ua *updateAccountSegnalaciIdRequest) buildRequest(czrmAccountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := ua.conf.Czrm.CzrmBaseName
	version := ua.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, czrmAccountId)
	data := map[string]interface{}{
		"SegnalaCiId__c":  ua.inputValues.SdcAccountId,
		"SegnalaCiId__pc": ua.inputValues.SdcAccountId,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		ua.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	ua.logger.Sugar().Debug("updateAccountSegnalaciIdRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		ua.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ua.accessToken))
	return req, nil
}

func (ua *updateAccountSegnalaciIdRequest) doUpdateAccountSegnalaciId(czrmAccountId string) (int, error) {

	req, err := ua.buildRequest(czrmAccountId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := ua.client.Do(req)
	if err != nil {
		ua.logger.Error("error performing  UpdateAccountSegnalaciId request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = ua.handleUpdateAccountSegnalaciIdResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (ua *updateAccountSegnalaciIdRequest) handleUpdateAccountSegnalaciIdResponse(resp *http.Response) error {
	if resp.StatusCode != http.StatusNoContent {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ua.logger.Error("error decoding body UpdateAccountSegnalaciId response: ", zap.Error(err))
			return err
		}
		ua.logger.Sugar().Debug("bad UpdateAccountSegnalaciId response: ", string(bodyBytes))

		return errors.New("UpdateAccountSegnalaciId bad error response")
	}
	ua.logger.Sugar().Debug("updated account with segnalaciId: , ", ua.inputValues.SdcAccountId)
	return nil
}
