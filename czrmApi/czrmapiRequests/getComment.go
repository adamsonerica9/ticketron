package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetCommentRequest interface {
	GetCommentRequest(token, commentId string) (czrmrequeststructs.GetCommentSuccessResponse, int, error)
	buildRequest(token, commentId string) (*http.Request, error)
	doGetCommentRequest(token, commentId string) (int, error)
	handleGetCommentResponse(resp *http.Response) error
}
type GetCommentRequest struct {
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues czrmrequeststructs.GetCommentSuccessResponse
}

func NewGetCommentRequest(log *zap.Logger, config *config.Config) IGetCommentRequest {
	r := &GetCommentRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return r
}
func (r *GetCommentRequest) GetCommentRequest(token, commentId string) (czrmrequeststructs.GetCommentSuccessResponse, int, error) {
	statuscode, err := r.doGetCommentRequest(token, commentId)
	if err != nil {
		return r.responseValues, statuscode, err
	}
	defer r.cleanResources()
	return r.responseValues, statuscode, nil
}

func (r *GetCommentRequest) doGetCommentRequest(token, commentId string) (int, error) {

	req, err := r.buildRequest(token, commentId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  GetCommentBySdcAccountId request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleGetCommentResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (r *GetCommentRequest) buildRequest(token, commentId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Commento__c"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, commentId)
	r.logger.Sugar().Debug("GetComment url: ", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+token)
	return req, nil
}

func (r *GetCommentRequest) handleGetCommentResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("getComment resonse status code: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		r.logger.Sugar().Debug("bad GetComment response: ", string(bodyBytes))
		return errors.New("GetComment bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues)
	if err != nil {
		r.logger.Error("error decoding body GetComment response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *GetCommentRequest) cleanResources() {
	r.logger.Sugar().Debug("GetCommentRequest: cleaning resources after response")
	r.responseValues = requeststructs.GetCommentSuccessResponse{}
}
