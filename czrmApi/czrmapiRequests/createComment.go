package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type ICreateCommentRequest interface {
	SetAccesstoken(a string)
	CreateNewComment(t string) (string, error)
	SetInputValues(input requeststructs.CreateCommentInputValues)
	buildRequest() (*http.Request, error)
	doCreateComment() (int, error)
	handleCreateCommentResponse(resp *http.Response) error
}

type createCommentRequest struct {
	inputValues    requeststructs.CreateCommentInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.CreateCommentResponse
	accessToken    string
}

func NewCreateCommentRequest(log *zap.Logger, config *config.Config) ICreateCommentRequest {
	r := &createCommentRequest{
		logger: log,
		conf:   config,
	}
	r.client = &http.Client{}
	return r
}
func (r *createCommentRequest) SetAccesstoken(a string) {
	r.accessToken = a
}
func (r *createCommentRequest) CreateNewComment(t string) (string, error) {
	r.accessToken = t
	_, err := r.doCreateComment()
	if err != nil {
		return "", err
	}
	defer r.cleanResources()
	return r.responseValues.CreateCommentSuccessResponse.ID, nil
}
func (r *createCommentRequest) SetInputValues(input requeststructs.CreateCommentInputValues) {
	r.inputValues = input
}
func (r *createCommentRequest) buildRequest() (*http.Request, error) {
	SOBJECT_API_NAME := "Commento__c"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := "56.0"
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]string{
		"CaseId__c":         r.inputValues.CaseIDC,
		"Description__c":    r.inputValues.DescriptionC,
		"AutoreCommento__c": r.inputValues.AutoreCommento__c,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("createComment payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", r.accessToken))
	return req, nil
}

func (r *createCommentRequest) doCreateComment() (int, error) {

	req, err := r.buildRequest()
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  CreateComment request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleCreateCommentResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (r *createCommentRequest) handleCreateCommentResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("CreateComment status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body CreateComment response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad CreateComment response: ", string(bodyBytes))
		return errors.New("CreateComment bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.CreateCommentSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body CreateComment response: ", zap.Error(err))
		return err
	}
	r.logger.Sugar().Debug("new comment created on the CzRm. czrm comment id: ", r.responseValues.CreateCommentSuccessResponse.ID)
	return nil
}

func (r *createCommentRequest) cleanResources() {
	r.logger.Sugar().Debug("createCommentRequest: cleaning resources after response")
	r.responseValues = requeststructs.CreateCommentResponse{}
}
