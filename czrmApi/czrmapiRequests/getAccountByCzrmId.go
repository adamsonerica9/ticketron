package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetAccountByCzrmIdRequest interface {
	SetInputValues(input czrmrequeststructs.GetAccountByCzrmIdInputValues)
	SetAccesstoken(a string)
	GetAccountByCzrmIdRequest(t string) (czrmrequeststructs.GetAccountByCzrmIdSuccessResponse, int, error)
	buildRequest() (*http.Request, error)
	doGetAccountByCzrmIdRequest() (int, error)
	handleGetAccountByCzrmIdResponse(resp *http.Response) error
}
type GetAccountByCzrmIdRequest struct {
	inputValues    czrmrequeststructs.GetAccountByCzrmIdInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues czrmrequeststructs.GetAccountByCzrmIdResponse
	accessToken    string
}

func NewGetAccountByCzrmIdRequest(log *zap.Logger, config *config.Config) IGetAccountByCzrmIdRequest {
	r := &GetAccountByCzrmIdRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return r
}
func (r *GetAccountByCzrmIdRequest) GetAccountByCzrmIdRequest(t string) (czrmrequeststructs.GetAccountByCzrmIdSuccessResponse, int, error) {
	r.accessToken = t
	statuscode, err := r.doGetAccountByCzrmIdRequest()
	if err != nil {
		return r.responseValues.GetAccountByCzrmIdSuccessResponse, statuscode, err
	}
	defer r.cleanResources()
	return r.responseValues.GetAccountByCzrmIdSuccessResponse, statuscode, nil
}
func (r *GetAccountByCzrmIdRequest) SetAccesstoken(t string) {
	r.accessToken = t
}
func (r *GetAccountByCzrmIdRequest) SetInputValues(input czrmrequeststructs.GetAccountByCzrmIdInputValues) {
	r.inputValues = input
}
func (r *GetAccountByCzrmIdRequest) buildRequest() (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, r.inputValues.User)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+r.accessToken)
	return req, nil
}
func (r *GetAccountByCzrmIdRequest) doGetAccountByCzrmIdRequest() (int, error) {

	req, err := r.buildRequest()
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  GetAccountBySdcAccountId request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleGetAccountByCzrmIdResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (r *GetAccountByCzrmIdRequest) handleGetAccountByCzrmIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("GetAccountBySdcAccountId status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetAccountByCzrmId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetAccountByCzrmId response: ", string(bodyBytes))
		return errors.New("GetAccountByCzrmId bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.GetAccountByCzrmIdSuccessResponse)
	if err != nil {
		r.logger.Error("error decoding body GetAccountByCzrmId response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *GetAccountByCzrmIdRequest) cleanResources() {
	r.logger.Sugar().Debug("GetAccountByCzrmIdRequest: cleaning resources after response")
	r.responseValues = czrmrequeststructs.GetAccountByCzrmIdResponse{}
}
