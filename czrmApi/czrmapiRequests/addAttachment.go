package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IAddAttachmentRequest interface {
	SetAccesstoken(a string)
	AddAttachmentToCase(t string) (string, error)
	SetInputValues(input requeststructs.AddAttachmentInputValues)
	buildRequest() (*http.Request, error)
	doAddAttachment() (int, error)
	handleAddAttachmentResponse(resp *http.Response) error
}

type AddAttachmentRequest struct {
	inputValues    requeststructs.AddAttachmentInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.AddAttachmentResponse
	accessToken    string
	statusCode     int
}

func NewAddAttachmentRequest(log *zap.Logger, config *config.Config) IAddAttachmentRequest {
	r := &AddAttachmentRequest{
		logger: log,
		conf:   config,
	}
	r.client = &http.Client{}
	return r
}
func (r *AddAttachmentRequest) SetAccesstoken(a string) {
	r.accessToken = a
}
func (r *AddAttachmentRequest) AddAttachmentToCase(t string) (string, error) {
	r.accessToken = t
	_, err := r.doAddAttachment()
	if err != nil {
		return "", err
	}
	defer r.cleanResources()
	return r.responseValues.AddAttachmentSuccessResponse.ID, nil
}
func (r *AddAttachmentRequest) SetInputValues(input requeststructs.AddAttachmentInputValues) {
	r.inputValues = input
}
func (r *AddAttachmentRequest) buildRequest() (*http.Request, error) {
	SOBJECT_API_NAME := "ContentVersion"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]string{
		"Title":                  r.inputValues.Title,
		"PathOnClient":           r.inputValues.PathOnClient,
		"firstPublishLocationid": r.inputValues.FirstPublishLocationid,
		"VersionData":            r.inputValues.VersionData,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", r.accessToken))
	return req, nil
}

func (r *AddAttachmentRequest) doAddAttachment() (int, error) {

	req, err := r.buildRequest()
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  AddAttachment request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleAddAttachmentResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (r *AddAttachmentRequest) handleAddAttachmentResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("AddAttachment status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body AddAttachment response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad AddAttachment response: ", string(bodyBytes))
		return errors.New("AddAttachment bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.AddAttachmentSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body AddAttachment response: ", zap.Error(err))
		return err
	}
	r.logger.Sugar().Debug("new Attachment added on the CzRm. case id: ", r.responseValues.AddAttachmentSuccessResponse.ID)
	return nil
}

func (r *AddAttachmentRequest) cleanResources() {
	r.logger.Sugar().Debug("AddAttachmentRequest: cleaning resources after response")
	r.responseValues = requeststructs.AddAttachmentResponse{}
}
