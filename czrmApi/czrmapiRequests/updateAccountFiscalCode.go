package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IUpdateAccountFiscalCodeRequest interface {
	SetAccesstoken(a string)
	UpdateAccountFiscalCode(t string, czrmAccountId string) (int, error)
	SetInputValues(input requeststructs.UpdateAccountInputValues)
	buildRequest(czrmAccountId string) (*http.Request, error)
	doUpdateAccountFiscalCode(czrmAccountId string) (int, error)
	handleUpdateAccountFiscalCodeResponse(resp *http.Response) error
}

type updateAccountFiscalCodeRequest struct {
	inputValues    requeststructs.UpdateAccountInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.UpdateAccountResponse
	accessToken    string
}

func NewUpdateAccountFiscalCodeRequest(log *zap.Logger, config *config.Config) IUpdateAccountFiscalCodeRequest {
	ca := &updateAccountFiscalCodeRequest{
		logger: log,
		conf:   config,
	}
	ca.client = &http.Client{}
	return ca
}

func (ua *updateAccountFiscalCodeRequest) SetAccesstoken(a string) {
	ua.accessToken = a
}
func (ua *updateAccountFiscalCodeRequest) SetInputValues(input requeststructs.UpdateAccountInputValues) {
	ua.inputValues = input
}
func (ua *updateAccountFiscalCodeRequest) UpdateAccountFiscalCode(t string, czrmAccountId string) (int, error) {
	ua.accessToken = t
	statuscode, err := ua.doUpdateAccountFiscalCode(czrmAccountId)
	if err != nil {
		return statuscode, err
	}
	return statuscode, nil
}
func (ua *updateAccountFiscalCodeRequest) buildRequest(czrmAccountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := ua.conf.Czrm.CzrmBaseName
	version := ua.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, czrmAccountId)
	data := map[string]interface{}{
		"Codice_Fiscale__c":  ua.inputValues.Fiscalcode,
		"Codice_Fiscale__pc": ua.inputValues.Fiscalcode,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		ua.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	ua.logger.Sugar().Debug("updateAccountFiscalCodeRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		ua.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ua.accessToken))
	return req, nil
}

func (ua *updateAccountFiscalCodeRequest) doUpdateAccountFiscalCode(czrmAccountId string) (int, error) {

	req, err := ua.buildRequest(czrmAccountId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := ua.client.Do(req)
	if err != nil {
		ua.logger.Error("error performing  UpdateAccountFiscalCode request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = ua.handleUpdateAccountFiscalCodeResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (ua *updateAccountFiscalCodeRequest) handleUpdateAccountFiscalCodeResponse(resp *http.Response) error {
	ua.logger.Sugar().Debug("UpdateAccountFiscalCode status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusNoContent {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ua.logger.Error("error decoding body UpdateAccountFiscalCode response: ", zap.Error(err))
		}

		ua.logger.Sugar().Debug("bad CreateAccount response: ", string(bodyBytes))
		return errors.New("UpdateAccount bad error response")
	}
	ua.logger.Sugar().Debug("account updated on the CzRm ")
	return nil
}
