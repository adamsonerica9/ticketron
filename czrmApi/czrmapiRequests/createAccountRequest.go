package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type ICreateAccountRequest interface {
	SetAccesstoken(a string)
	CreateNewAccount(t string) (string, int, error)
	SetInputValues(input requeststructs.CreateAccountInputValues)
	buildRequest() (*http.Request, error)
	doCreateAccount() (int, error)
	handleCreateAccountResponse(resp *http.Response) error
}

type createAccountRequest struct {
	inputValues    requeststructs.CreateAccountInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.CreateAccountResponse
	accessToken    string
}

func NewCreateAccountRequest(log *zap.Logger, config *config.Config) ICreateAccountRequest {
	ca := &createAccountRequest{
		logger: log,
		conf:   config,
	}
	ca.client = &http.Client{}
	return ca
}
func (ca *createAccountRequest) SetAccesstoken(a string) {
	ca.accessToken = a
}
func (ca *createAccountRequest) CreateNewAccount(t string) (string, int, error) {
	ca.accessToken = t
	statuscode, err := ca.doCreateAccount()
	if err != nil {
		return "", statuscode, err
	}
	defer ca.cleanResources()
	return ca.responseValues.CreateAccountSuccessResponse.ID, statuscode, nil
}
func (ca *createAccountRequest) SetInputValues(input requeststructs.CreateAccountInputValues) {
	ca.inputValues = input
}
func (ca *createAccountRequest) buildRequest() (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := ca.conf.Czrm.CzrmBaseName
	version := ca.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s", endpoint, version, SOBJECT_API_NAME)
	data := map[string]interface{}{
		"FirstName":          ca.inputValues.FirstName,
		"LastName":           ca.inputValues.LastName,
		"Codice_Fiscale__c":  ca.inputValues.Fiscalcode,
		"Codice_Fiscale__pc": ca.inputValues.Fiscalcode,
		"Email__c":           ca.inputValues.Email,
		"PersonEmail":        ca.inputValues.Email,
		"SegnalaCiId__c":     ca.inputValues.SdcAccountId,
		"SegnalaCiId__pc":    ca.inputValues.SdcAccountId,
		"Phone":              ca.inputValues.Phone,
		"PersonMobilePhone":  ca.inputValues.Phone,
		"RecordType":         map[string]string{"Name": "Account personale"},
	}

	payload, err := json.Marshal(data)
	if err != nil {
		ca.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	ca.logger.Sugar().Debug("createAccountRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		ca.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ca.accessToken))
	return req, nil
}
func (ca *createAccountRequest) doCreateAccount() (int, error) {

	req, err := ca.buildRequest()
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := ca.client.Do(req)
	if err != nil {
		ca.logger.Error("error performing  CreateAccount request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = ca.handleCreateAccountResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (ca *createAccountRequest) handleCreateAccountResponse(resp *http.Response) error {
	ca.logger.Sugar().Debug("CreateAccount status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ca.logger.Error("error decoding body CreateAccount response: ", zap.Error(err))
		}

		ca.logger.Sugar().Debug("bad CreateAccount response: ", string(bodyBytes))
		return errors.New("CreateAccount bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&ca.responseValues.CreateAccountSuccessResponse)
	if err != nil {
		ca.logger.Sugar().Error("error decoding body CreateAccount response: ", zap.Error(err))
		return err
	}
	ca.logger.Sugar().Debug("new account created on the CzRm. czrm account id: ", ca.responseValues.CreateAccountSuccessResponse.ID)
	return nil
}

func (ca *createAccountRequest) cleanResources() {
	ca.logger.Sugar().Debug("createAccountRequest: cleaning resources after response")
	ca.responseValues = requeststructs.CreateAccountResponse{}
}
