package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetCaseByCzrmIdRequest interface {
	SetInputValues(input czrmrequeststructs.GetCaseByCzrmIdInputValues)
	SetAccesstoken(a string)
	GetCaseByCzrmIdRequest(t string) (czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, int, error)
	buildRequest() (*http.Request, error)
	doGetCaseByCzrmIdRequest() (int, error)
	handleGetCaseByCzrmIdResponse(resp *http.Response) error
}
type GetCaseByCzrmIdRequest struct {
	inputValues    czrmrequeststructs.GetCaseByCzrmIdInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues czrmrequeststructs.GetCaseByCzrmIdResponse
	accessToken    string
}

func NewGetCaseByCzrmIdRequest(log *zap.Logger, config *config.Config) IGetCaseByCzrmIdRequest {
	r := &GetCaseByCzrmIdRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return r
}
func (r *GetCaseByCzrmIdRequest) GetCaseByCzrmIdRequest(t string) (czrmrequeststructs.GetCaseByCzrmIdSuccessResponse, int, error) {
	r.accessToken = t
	statuscode, err := r.doGetCaseByCzrmIdRequest()
	if err != nil {
		return r.responseValues.GetCaseByCzrmIdSuccessResponse, statuscode, err
	}
	defer r.cleanResources()
	return r.responseValues.GetCaseByCzrmIdSuccessResponse, statuscode, nil
}
func (r *GetCaseByCzrmIdRequest) SetAccesstoken(a string) {
	r.accessToken = a
}
func (r *GetCaseByCzrmIdRequest) SetInputValues(input czrmrequeststructs.GetCaseByCzrmIdInputValues) {
	r.inputValues = input
}
func (r *GetCaseByCzrmIdRequest) buildRequest() (*http.Request, error) {
	SOBJECT_API_NAME := "case"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, r.inputValues.CaseId)
	r.logger.Sugar().Debug("GetCaseByCzrmId url: ", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+r.accessToken)
	return req, nil
}
func (r *GetCaseByCzrmIdRequest) doGetCaseByCzrmIdRequest() (int, error) {

	req, err := r.buildRequest()
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  GetCaseByCzrmIdRequest request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleGetCaseByCzrmIdResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (r *GetCaseByCzrmIdRequest) handleGetCaseByCzrmIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("CreateAccount status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetCaseByCzrmId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetCaseByCzrmId response: ", string(bodyBytes))
		return errors.New("GetCaseByCzrmId bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.GetCaseByCzrmIdSuccessResponse)
	if err != nil {
		r.logger.Error("error decoding body GetCaseByCzrmId response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *GetCaseByCzrmIdRequest) cleanResources() {
	r.logger.Sugar().Debug("GetCaseByCzrmIdRequest: cleaning resources after response")
	r.responseValues = requeststructs.GetCaseByCzrmIdResponse{}
}
