package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetCaseByApplicationIdRequest interface {
	GetCaseByApplicationId(t, SdcAccountId string) (string, int, error)
	buildRequest(SdcAccountId string) (*http.Request, error)
}

type getCaseByApplicationIdRequest struct {
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues requeststructs.GetCaseBySegnalaciIdc_cResponse
	accessToken    string
}

func NewGetCaseByApplicationIdRequest(log *zap.Logger, config *config.Config) IGetCaseByApplicationIdRequest {
	gc := &getCaseByApplicationIdRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return gc
}
func (gc *getCaseByApplicationIdRequest) GetCaseByApplicationId(t, SdcAccountId string) (string, int, error) {
	gc.accessToken = t
	statuscode, err := gc.doGetCaseByApplicationIdRequestRequest(SdcAccountId)
	if err != nil {
		return "", statuscode, err
	}
	defer gc.cleanResources()
	return gc.responseValues.GetCaseByApplicationIdSuccessResponse.ID, statuscode, nil
}

func (gc *getCaseByApplicationIdRequest) buildRequest(SdcAccountId string) (*http.Request, error) {
	endpoint := gc.conf.Czrm.CzrmBaseName
	version := gc.conf.Czrm.ApiVersion
	SOBJECT_API_NAME := "Case"
	FIELD_NAME := "SegnalaciId__c"
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s/%s", endpoint, version, SOBJECT_API_NAME, FIELD_NAME, SdcAccountId)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		gc.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+gc.accessToken)
	return req, nil
}
func (gc *getCaseByApplicationIdRequest) doGetCaseByApplicationIdRequestRequest(SdcAccountId string) (int, error) {

	req, err := gc.buildRequest(SdcAccountId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := gc.client.Do(req)
	if err != nil {
		gc.logger.Error("error performing  get request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = gc.handleGetCaseByApplicationIdRequestResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (gc *getCaseByApplicationIdRequest) handleGetCaseByApplicationIdRequestResponse(resp *http.Response) error {
	gc.logger.Sugar().Debug("GetCaseByApplicationId status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		gc.logger.Sugar().Debug("bad GetCaseByApplicationIdRequest response: ", string(bodyBytes))
		return errors.New("GetCaseByApplicationIdRequest bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&gc.responseValues.GetCaseByApplicationIdSuccessResponse)
	if err != nil {
		gc.logger.Error("error decoding body GetCaseByApplicationIdRequest response: ", zap.Error(err))
		return err
	}
	gc.logger.Sugar().Debug("Case found")
	return nil
}

func (r *getCaseByApplicationIdRequest) cleanResources() {
	r.logger.Sugar().Debug("GetCaseByApplicationIdRequest: cleaning resources after response")
	r.responseValues = requeststructs.GetCaseBySegnalaciIdc_cResponse{}
}
