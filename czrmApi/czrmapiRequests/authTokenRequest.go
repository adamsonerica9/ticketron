package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"mime/multipart"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IAuthTokenRequest interface {
	GetNewAuthToken() (string, error)
	buildRequest() (*http.Request, error)
	handleAuthTokenResponse(resp *http.Response) error
	buildWSO2Request() (*http.Request, error)
	handleWSO2AuthTokenResponse(resp *http.Response) error
}

type authTokenRequest struct {
	client             *http.Client
	logger             *zap.Logger
	conf               *config.Config
	responseValues     requeststructs.AuthTokenResponse
	responseWso2Values requeststructs.Wso2AuthTokenSuccessResponse
}

func NewAuthTokenRequest(log *zap.Logger, config *config.Config) IAuthTokenRequest {
	atr := &authTokenRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}

	return atr
}
func (at *authTokenRequest) GetNewAuthToken() (string, error) {
	if at.conf.Czrm.CzrmAuthType == "SF" {
		err := at.doGetAuthTokenRequest()
		if err != nil {
			return "", err
		}
		return at.responseValues.AuthTokenSuccessResponse.AccessToken, nil
	} else if at.conf.Czrm.CzrmAuthType == "WSO2" {
		err := at.doGetWSO2AuthTokenRequest()
		if err != nil {
			return "", err
		}
		return at.responseWso2Values.AccessToken, nil
	}
	defer at.cleanResources()
	at.logger.Sugar().Error("invalid auth type selected: check the value of CZRM_AUTH_TYPE environment variable ")
	return "", errors.New("invalid auth type selected")
}

// SF authentication
func (at *authTokenRequest) doGetAuthTokenRequest() error {

	req, err := at.buildRequest()
	if err != nil {
		return err
	}

	// make the request
	resp, err := at.client.Do(req)
	if err != nil {
		at.logger.Sugar().Error("error performing  GetAuthToken request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = at.handleAuthTokenResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (at *authTokenRequest) buildRequest() (*http.Request, error) {
	// create a new multipart writer with a unique boundary
	var b bytes.Buffer
	writer := multipart.NewWriter(&b)
	boundary := writer.Boundary()
	// add the form data fields
	writer.WriteField("username", at.conf.Czrm.AuthTokenUser)
	writer.WriteField("password", at.conf.Czrm.AuthTokenPassword)
	writer.WriteField("grant_type", at.conf.Czrm.AuthTokenGrantType)
	writer.WriteField("client_id", at.conf.Czrm.AuthTokenClientId)
	writer.WriteField("client_secret", at.conf.Czrm.AuthTokenClientSecret)
	writer.Close()

	req, err := http.NewRequest(http.MethodPost, at.conf.Czrm.AuthTokenEndpoint, &b)
	if err != nil {
		at.logger.Sugar().Error("error creating new auth request: ", zap.Error(err))
		return nil, err
	}
	// set the content type header and boundary
	req.Header.Set("Content-Type", "multipart/form-data; boundary="+boundary)
	return req, nil
}
func (at *authTokenRequest) handleAuthTokenResponse(resp *http.Response) error {
	at.logger.Sugar().Debug("AuthToken status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			at.logger.Error("error decoding body AuthToken response: ", zap.Error(err))
		}

		at.logger.Sugar().Debug("bad AuthToken response: ", string(bodyBytes))
		return errors.New("auth token bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&at.responseValues.AuthTokenSuccessResponse)
	if err != nil {
		at.logger.Sugar().Error("error decoding body auth response: ", zap.Error(err))
		return err
	}

	return nil
}

// WSO2 autentication
func (at *authTokenRequest) doGetWSO2AuthTokenRequest() error {

	req, err := at.buildWSO2Request()
	if err != nil {
		return err
	}

	// make the request
	resp, err := at.client.Do(req)
	if err != nil {
		at.logger.Sugar().Error("error performing  GetAuthToken request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = at.handleWSO2AuthTokenResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (at *authTokenRequest) buildWSO2Request() (*http.Request, error) {

	data := map[string]string{
		"Key":    at.conf.Czrm.AuthTokenClientId,
		"Secret": at.conf.Czrm.AuthTokenClientSecret,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		at.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, at.conf.Czrm.AuthTokenEndpoint, bytes.NewBuffer(payload))
	if err != nil {
		at.logger.Sugar().Error("error creating new auth request: ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}
func (at *authTokenRequest) handleWSO2AuthTokenResponse(resp *http.Response) error {
	if resp.StatusCode != http.StatusOK {
		at.logger.Sugar().Error("bad WSO2 Auth Token response. status code: ", resp.StatusCode)
		return errors.New("auth token bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&at.responseWso2Values)
	if err != nil {
		at.logger.Sugar().Error("error decoding body WSO2 auth response: ", zap.Error(err))
		return err
	}
	return nil
}

func (at *authTokenRequest) cleanResources() {
	at.logger.Sugar().Debug("authTokenRequest: cleaning resources after response")
	at.responseValues = requeststructs.AuthTokenResponse{}
	at.responseWso2Values = requeststructs.Wso2AuthTokenSuccessResponse{}
}
