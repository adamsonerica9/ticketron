package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetCzrmAccountBySdcAccountIdRequest interface {
	SetInputValues(input czrmrequeststructs.GetAccountBySdcAccountIdInputValues)
	GetCzrmAccountIdBySdcAccountIdRequest(t, sdcAccountId string) (string, int, error)
	GetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId string) (czrmrequeststructs.GetAccountBySdcAccountIdSuccessResponse, int, error)
	buildRequest(t, sdcAccountId string) (*http.Request, error)
	doGetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId string) (int, error)
	handleGetCzrmAccountBySdcAccountIdResponse(resp *http.Response) error
}
type getCzrmAccountBySdcAccountIdRequest struct {
	inputValues    czrmrequeststructs.GetAccountBySdcAccountIdInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues czrmrequeststructs.GetAccountBySdcAccountIdResponse
}

func NewAccountBySdcAccountIdRequest(log *zap.Logger, config *config.Config) IGetCzrmAccountBySdcAccountIdRequest {
	r := &getCzrmAccountBySdcAccountIdRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return r
}
func (r *getCzrmAccountBySdcAccountIdRequest) GetCzrmAccountIdBySdcAccountIdRequest(t, sdcAccountId string) (string, int, error) {
	statuscode, err := r.doGetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId)
	if err != nil {
		return "", statuscode, err
	}
	defer r.cleanResources()
	return r.responseValues.AccountBySdcAccountIdSuccessResponse.ID, statuscode, nil
}

func (r *getCzrmAccountBySdcAccountIdRequest) GetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId string) (czrmrequeststructs.GetAccountBySdcAccountIdSuccessResponse, int, error) {
	statuscode, err := r.doGetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId)
	if err != nil {
		return r.responseValues.AccountBySdcAccountIdSuccessResponse, statuscode, err
	}
	defer r.cleanResources()
	return r.responseValues.AccountBySdcAccountIdSuccessResponse, statuscode, nil
}

func (r *getCzrmAccountBySdcAccountIdRequest) SetInputValues(input czrmrequeststructs.GetAccountBySdcAccountIdInputValues) {
	r.inputValues = input
}
func (r *getCzrmAccountBySdcAccountIdRequest) buildRequest(t, sdcAccountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	FIELD_NAME := "SegnalaCiId__c"
	FIELD_VALUE := sdcAccountId
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s/%s", endpoint, version, SOBJECT_API_NAME, FIELD_NAME, FIELD_VALUE)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+t)
	return req, nil
}
func (r *getCzrmAccountBySdcAccountIdRequest) doGetCzrmAccountBySdcAccountIdRequest(t, sdcAccountId string) (int, error) {

	req, err := r.buildRequest(t, sdcAccountId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  GetAccountBySdcAccountId request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleGetCzrmAccountBySdcAccountIdResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (r *getCzrmAccountBySdcAccountIdRequest) handleGetCzrmAccountBySdcAccountIdResponse(resp *http.Response) error {

	r.logger.Sugar().Debug("GetCzrmAccountBySdcAccountId response status code : ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetCzrmAccountBySdcAccountId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetCzrmAccountBySdcAccountId response: ", string(bodyBytes))
		return errors.New("GetCzrmAccountBySdcAccountId bad response")
	}
	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.AccountBySdcAccountIdSuccessResponse)
	if err != nil {
		r.logger.Error("error decoding body GetCzrmAccountBySdcAccountId response: ", zap.Error(err))
		return err
	}
	r.logger.Sugar().Debug("GetCzrmAccountBySdcAccountId: we found the following czrm account id: ", r.responseValues.AccountBySdcAccountIdSuccessResponse.ID)
	return nil
}

func (r *getCzrmAccountBySdcAccountIdRequest) cleanResources() {
	r.logger.Sugar().Debug("getCzrmAccountBySdcAccountIdRequest: cleaning resources after response")
	r.responseValues = czrmrequeststructs.GetAccountBySdcAccountIdResponse{}
}
