package czrmapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IGetAccountByFiscalCodeRequest interface {
	SetAccesstoken(a string)
	GetAccountByFiscalCodeRequest(t, fiscalCode string) (string, int, error)
	buildRequest(fiscalCode string) (*http.Request, error)
	doGetAccountByFiscalCodeRequest(fiscalCode string) (int, error)
	handleGetAccountByFiscalCodeResponse(resp *http.Response) error
}
type GetAccountByFiscalCodeRequest struct {
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues czrmrequeststructs.GetAccountByFiscalCodeSuccessResponse
	accessToken    string
}

func NewGetAccountByFiscalCodeRequest(log *zap.Logger, config *config.Config) IGetAccountByFiscalCodeRequest {
	r := &GetAccountByFiscalCodeRequest{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
	return r
}
func (r *GetAccountByFiscalCodeRequest) GetAccountByFiscalCodeRequest(t, fiscalCode string) (string, int, error) {
	r.accessToken = t
	statuscode, err := r.doGetAccountByFiscalCodeRequest(fiscalCode)
	if err != nil {
		return "", statuscode, nil
	}
	defer r.cleanResources()
	if len(r.responseValues.Records) != 0 {
		return r.responseValues.Records[0].ID, statuscode, nil
	} else {
		return "", statuscode, nil
	}
}
func (r *GetAccountByFiscalCodeRequest) SetAccesstoken(a string) {
	r.accessToken = a
}

func (r *GetAccountByFiscalCodeRequest) buildRequest(fiscalCode string) (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := r.conf.Czrm.CzrmBaseName
	version := r.conf.Czrm.ApiVersion
	query := url.QueryEscape(fmt.Sprintf("SELECT Id FROM %s WHERE Codice_Fiscale__c='%s'", SOBJECT_API_NAME, fiscalCode))
	url := fmt.Sprintf("%s/services/data/v%s/query?q=%s", endpoint, version, query)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return req, err
	}

	req.Header.Set("Authorization", "Bearer "+r.accessToken)
	return req, nil
}
func (r *GetAccountByFiscalCodeRequest) doGetAccountByFiscalCodeRequest(fiscalCode string) (int, error) {

	req, err := r.buildRequest(fiscalCode)
	if err != nil {
		return -1, err
	}

	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Error("error performing  GetAccountBySdcAccountId request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = r.handleGetAccountByFiscalCodeResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}
func (r *GetAccountByFiscalCodeRequest) handleGetAccountByFiscalCodeResponse(resp *http.Response) error {
	r.logger.Sugar().Debug(" GetAccountByFiscalCode status code response : ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetAccountByFiscalCode response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetAccountByFiscalCode response: ", string(bodyBytes))
		return errors.New("GetAccountByFiscalCode bad response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues)
	if err != nil {
		r.logger.Error("error decoding body GetAccountByFiscalCode response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *GetAccountByFiscalCodeRequest) cleanResources() {
	r.logger.Sugar().Debug("GetAccountByFiscalCodeRequest: cleaning resources after response")
	r.responseValues = czrmrequeststructs.GetAccountByFiscalCodeSuccessResponse{}
}
