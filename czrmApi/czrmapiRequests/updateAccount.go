package czrmapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	requeststructs "opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
)

type IUpdateAccountRequest interface {
	SetAccesstoken(a string)
	UpdateAccount(t string, czrmAccountId string) (int, error)
	SetInputValues(input requeststructs.UpdateAccountInputValues)
	buildRequest(czrmAccountId string) (*http.Request, error)
	doUpdateAccount(czrmAccountId string) (int, error)
	handleUpdateAccountResponse(resp *http.Response) error
}

type updateAccountRequest struct {
	inputValues requeststructs.UpdateAccountInputValues
	client      *http.Client
	logger      *zap.Logger
	conf        *config.Config
	accessToken string
}

func NewUpdateAccountRequest(log *zap.Logger, config *config.Config) IUpdateAccountRequest {
	ca := &updateAccountRequest{
		logger: log,
		conf:   config,
	}
	ca.client = &http.Client{}
	return ca
}

func (ua *updateAccountRequest) SetAccesstoken(a string) {
	ua.accessToken = a
}
func (ua *updateAccountRequest) SetInputValues(input requeststructs.UpdateAccountInputValues) {
	ua.inputValues = input
}
func (ua *updateAccountRequest) UpdateAccount(t string, czrmAccountId string) (int, error) {
	ua.accessToken = t
	statuscode, err := ua.doUpdateAccount(czrmAccountId)
	if err != nil {
		return statuscode, err
	}
	return statuscode, nil
}
func (ua *updateAccountRequest) buildRequest(czrmAccountId string) (*http.Request, error) {
	SOBJECT_API_NAME := "Account"
	endpoint := ua.conf.Czrm.CzrmBaseName
	version := ua.conf.Czrm.ApiVersion
	url := fmt.Sprintf("%s/services/data/v%s/sobjects/%s/%s", endpoint, version, SOBJECT_API_NAME, czrmAccountId)
	data := map[string]interface{}{
		"FirstName":          ua.inputValues.FirstName,
		"LastName":           ua.inputValues.LastName,
		"Codice_Fiscale__c":  ua.inputValues.Fiscalcode,
		"Codice_Fiscale__pc": ua.inputValues.Fiscalcode,
		"Email__c":           ua.inputValues.Email,
		"PersonEmail":        ua.inputValues.Email,
		"SegnalaCiId__c":     ua.inputValues.SdcAccountId,
		"SegnalaCiId__pc":    ua.inputValues.SdcAccountId,
		"Phone":              ua.inputValues.Phone,
		"PersonMobilePhone":  ua.inputValues.Phone,
		"RecordType":         map[string]string{"Name": "Account personale"},
	}
	payload, err := json.Marshal(data)
	if err != nil {
		ua.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	ua.logger.Sugar().Debug("updateAccountRequest payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		ua.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ua.accessToken))
	return req, nil
}

func (ua *updateAccountRequest) doUpdateAccount(czrmAccountId string) (int, error) {

	req, err := ua.buildRequest(czrmAccountId)
	if err != nil {
		return -1, err
	}

	// make the request
	resp, err := ua.client.Do(req)
	if err != nil {
		ua.logger.Error("error performing  UpdateAccount request: ", zap.Error(err))
		return -1, err
	}
	defer resp.Body.Close()
	err = ua.handleUpdateAccountResponse(resp)
	if err != nil {
		return resp.StatusCode, err
	}

	return resp.StatusCode, nil
}

func (ua *updateAccountRequest) handleUpdateAccountResponse(resp *http.Response) error {
	ua.logger.Sugar().Debug("UpdateAccount status code response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusNoContent {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ua.logger.Error("error decoding body UpdateAccount response: ", zap.Error(err))
		}

		ua.logger.Sugar().Debug("bad UpdateAccount response: ", string(bodyBytes))
		return errors.New("UpdateAccount bad error response")
	}
	ua.logger.Sugar().Debug("account updated on the CzRm ")
	return nil
}
