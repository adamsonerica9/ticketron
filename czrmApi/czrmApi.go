package czrmapi

import (
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapirequests "opencitylabs.it/ticketron/czrmApi/czrmapiRequests"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type ICzrmApi interface {
	AccessAuthTokenRequest() czrmapirequests.IAuthTokenRequest
	AccessGetCzrmAccountBySdcAccountIdRequest() czrmapirequests.IGetCzrmAccountBySdcAccountIdRequest
	AccessGetCzrmAccountByCzrmIdRequest() czrmapirequests.IGetAccountByCzrmIdRequest
	AccessGetCzrmAccountByFiscalCodeRequest() czrmapirequests.IGetAccountByFiscalCodeRequest
	AccessCreateAccountRequest() czrmapirequests.ICreateAccountRequest
	AccessUpdateAccountRequest() czrmapirequests.IUpdateAccountRequest
	AccessUpdateAccountFiscalCodeRequest() czrmapirequests.IUpdateAccountFiscalCodeRequest
	AccessUpdateAccountSegnalaciIdRequest() czrmapirequests.IUpdateAccountSegnalaciIdRequest
	AccessGetCaseByApplicationIdRequest() czrmapirequests.IGetCaseByApplicationIdRequest
	AccessGetCaseByCzrmIdRequest() czrmapirequests.IGetCaseByCzrmIdRequest
	AccessCreateCaseRequest() czrmapirequests.ICreateCaseRequest
	AccessCreateCommentRequest() czrmapirequests.ICreateCommentRequest
	AccessAddAttachmentRequest() czrmapirequests.IAddAttachmentRequest
	AccessGetCommentRequest() czrmapirequests.IGetCommentRequest
	//utils
	GetLat(strLat any) float64
	GetLon(strLon any) float64
	GetValidOrNullfiscalCode(fiscalCode string) string
	GetCategoriaOperator(str string) string
	GetMunicipioName(areas []kafkaMessagesStruct.GeographicAreas) string
	GetUserPhone(mobilephone, phone string) string
}

type czrmApi struct {
	logger                              *zap.Logger
	conf                                *config.Config
	authTokenRequest                    czrmapirequests.IAuthTokenRequest
	getCzrmAccountBySdcAccountIdRequest czrmapirequests.IGetCzrmAccountBySdcAccountIdRequest
	getCzrmAccountByczrmId              czrmapirequests.IGetAccountByCzrmIdRequest
	getCzrmAccountByFiscalcode          czrmapirequests.IGetAccountByFiscalCodeRequest
	createAccountRequest                czrmapirequests.ICreateAccountRequest
	updateAccountRequest                czrmapirequests.IUpdateAccountRequest
	updateAccountFiscalCodeRequest      czrmapirequests.IUpdateAccountFiscalCodeRequest
	updateAccountSegnalaciIdRequest     czrmapirequests.IUpdateAccountSegnalaciIdRequest
	getCaseByApplicationIdRequest       czrmapirequests.IGetCaseByApplicationIdRequest
	getCaseByCzrmIdRequest              czrmapirequests.IGetCaseByCzrmIdRequest
	createCaseRequest                   czrmapirequests.ICreateCaseRequest
	createCommentRequest                czrmapirequests.ICreateCommentRequest
	addAttachmentRequest                czrmapirequests.IAddAttachmentRequest
	getComment                          czrmapirequests.IGetCommentRequest
}

func NewCzrmApi(log *zap.Logger, config *config.Config) ICzrmApi {

	cz := &czrmApi{
		logger:                              log,
		conf:                                config,
		authTokenRequest:                    czrmapirequests.NewAuthTokenRequest(log, config),
		getCzrmAccountBySdcAccountIdRequest: czrmapirequests.NewAccountBySdcAccountIdRequest(log, config),
		getCzrmAccountByczrmId:              czrmapirequests.NewGetAccountByCzrmIdRequest(log, config),
		getCzrmAccountByFiscalcode:          czrmapirequests.NewGetAccountByFiscalCodeRequest(log, config),
		createAccountRequest:                czrmapirequests.NewCreateAccountRequest(log, config),
		getCaseByApplicationIdRequest:       czrmapirequests.NewGetCaseByApplicationIdRequest(log, config),
		getCaseByCzrmIdRequest:              czrmapirequests.NewGetCaseByCzrmIdRequest(log, config),
		createCaseRequest:                   czrmapirequests.NewCreateCase(log, config),
		updateAccountRequest:                czrmapirequests.NewUpdateAccountRequest(log, config),
		updateAccountFiscalCodeRequest:      czrmapirequests.NewUpdateAccountFiscalCodeRequest(log, config),
		updateAccountSegnalaciIdRequest:     czrmapirequests.NewUpdateAccountSegnalaciIdRequest(log, config),
		createCommentRequest:                czrmapirequests.NewCreateCommentRequest(log, config),
		addAttachmentRequest:                czrmapirequests.NewAddAttachmentRequest(log, config),
		getComment:                          czrmapirequests.NewGetCommentRequest(log, config),
	}
	return cz
}
func (cz *czrmApi) AccessAuthTokenRequest() czrmapirequests.IAuthTokenRequest {
	return cz.authTokenRequest
}
func (cz *czrmApi) AccessGetCzrmAccountBySdcAccountIdRequest() czrmapirequests.IGetCzrmAccountBySdcAccountIdRequest {
	return cz.getCzrmAccountBySdcAccountIdRequest
}
func (cz *czrmApi) AccessCreateAccountRequest() czrmapirequests.ICreateAccountRequest {
	return cz.createAccountRequest
}
func (cz *czrmApi) AccessGetCaseByApplicationIdRequest() czrmapirequests.IGetCaseByApplicationIdRequest {
	return cz.getCaseByApplicationIdRequest
}
func (cz *czrmApi) AccessGetCaseByCzrmIdRequest() czrmapirequests.IGetCaseByCzrmIdRequest {
	return cz.getCaseByCzrmIdRequest
}
func (cz *czrmApi) AccessGetCzrmAccountByCzrmIdRequest() czrmapirequests.IGetAccountByCzrmIdRequest {
	return cz.getCzrmAccountByczrmId
}
func (cz *czrmApi) AccessGetCzrmAccountByFiscalCodeRequest() czrmapirequests.IGetAccountByFiscalCodeRequest {
	return cz.getCzrmAccountByFiscalcode
}
func (cz *czrmApi) AccessCreateCaseRequest() czrmapirequests.ICreateCaseRequest {
	return cz.createCaseRequest
}
func (cz *czrmApi) AccessCreateCommentRequest() czrmapirequests.ICreateCommentRequest {
	return cz.createCommentRequest
}
func (cz *czrmApi) AccessUpdateAccountRequest() czrmapirequests.IUpdateAccountRequest {
	return cz.updateAccountRequest
}
func (cz *czrmApi) AccessUpdateAccountFiscalCodeRequest() czrmapirequests.IUpdateAccountFiscalCodeRequest {
	return cz.updateAccountFiscalCodeRequest
}
func (cz *czrmApi) AccessUpdateAccountSegnalaciIdRequest() czrmapirequests.IUpdateAccountSegnalaciIdRequest {
	return cz.updateAccountSegnalaciIdRequest
}
func (cz *czrmApi) AccessAddAttachmentRequest() czrmapirequests.IAddAttachmentRequest {
	return cz.addAttachmentRequest
}
func (cz *czrmApi) AccessGetCommentRequest() czrmapirequests.IGetCommentRequest {
	return cz.getComment
}
