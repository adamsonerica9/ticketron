package sdcapi

import (
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapirequests "opencitylabs.it/ticketron/sdcApi/sdcApiRequests"
)

type ISdcApi interface {
	AccessAuthTokenRequest() sdcapirequests.IGetAuthToken
	AccessGetApplicationByApplicationIdRequest() sdcapirequests.IGetApplicationByApplicationId
	AccessCreateUserRequest() sdcapirequests.ICreateUser
	AccessGetUserGroupByServiceId() sdcapirequests.IGetUserGroupByServiceId
	AccessGetUserGroups() sdcapirequests.IGetUserGroups
	AccessCreateUserGroup() sdcapirequests.ICreateUserGroup
	AccessUpdateUserbyId() sdcapirequests.IUpdateUserById
	AccessGetAttachment() sdcapirequests.IGetAttachment
	AccessUpdateApplicationAttachment() sdcapirequests.IUpdateApplicationAttachment
	AccessUpdateExternaIdApplication() sdcapirequests.IUpdateExternalIdApplication
	AccessGetApplicationByExternaIdRequest() sdcapirequests.IGetApplicationByExternalIdId
	AccessApplicationChangeStatus() sdcapirequests.IApplicationChangeStatus
	AccessCreateMessage() sdcapirequests.ICreateMessage
	AccessUpdateExternalIdComment() sdcapirequests.IUpdateExternalIdComment
	AccessGetCategories() sdcapirequests.IGetCategories
	AccessGetuserById() sdcapirequests.IGetUserById
	AccessCreateApplication() sdcapirequests.ICreateApplication
}

type sdcApi struct {
	logger                        *zap.Logger
	conf                          *config.Config
	authTokenRequest              sdcapirequests.IGetAuthToken
	getApplicationByApplicationId sdcapirequests.IGetApplicationByApplicationId
	createUser                    sdcapirequests.ICreateUser
	getUserGroupByServiceId       sdcapirequests.IGetUserGroupByServiceId
	getUserGroups                 sdcapirequests.IGetUserGroups
	createUserGroup               sdcapirequests.ICreateUserGroup
	updateuserBySdcAccount        sdcapirequests.IUpdateUserById
	getAttachement                sdcapirequests.IGetAttachment
	updateApplicationAttachment   sdcapirequests.IUpdateApplicationAttachment
	updateExternalIdApplication   sdcapirequests.IUpdateExternalIdApplication
	getApplicationByExternalId    sdcapirequests.IGetApplicationByExternalIdId
	applicationChangeStatus       sdcapirequests.IApplicationChangeStatus
	createMessage                 sdcapirequests.ICreateMessage
	updateExternalIdComment       sdcapirequests.IUpdateExternalIdComment
	getCategories                 sdcapirequests.IGetCategories
	getUser                       sdcapirequests.IGetUserById
	createApplication             sdcapirequests.ICreateApplication
}

func NewSdcApi(log *zap.Logger, config *config.Config) ISdcApi {
	return &sdcApi{
		logger:                        log,
		conf:                          config,
		authTokenRequest:              sdcapirequests.NewGetAuthToken(log, config),
		getApplicationByApplicationId: sdcapirequests.NewGetApplicationByApplicationId(log, config),
		createUser:                    sdcapirequests.NewCreateUser(log, config),
		getUserGroupByServiceId:       sdcapirequests.NewGetUserGroupByServiceId(log, config),
		getUserGroups:                 sdcapirequests.NewGetUserGroups(log, config),
		createUserGroup:               sdcapirequests.NewCreateUserGroup(log, config),
		updateuserBySdcAccount:        sdcapirequests.NewUpdateUserById(log, config),
		getAttachement:                sdcapirequests.NewGetAttachment(log, config),
		updateApplicationAttachment:   sdcapirequests.NewUpdateApplicationAttachment(log, config),
		updateExternalIdApplication:   sdcapirequests.NewUpdateExternalIdApplication(log, config),
		getApplicationByExternalId:    sdcapirequests.NewGetApplicationByExternalIdId(log, config),
		applicationChangeStatus:       sdcapirequests.NewApplicationChangeStatus(log, config),
		createMessage:                 sdcapirequests.NewCreateMessage(log, config),
		updateExternalIdComment:       sdcapirequests.NewUpdateExternalIdComment(log, config),
		getCategories:                 sdcapirequests.NewGetCategories(log, config),
		getUser:                       sdcapirequests.NewGetUserById(log, config),
		createApplication:             sdcapirequests.NewCreateApplication(log, config),
	}
}

func (s *sdcApi) AccessAuthTokenRequest() sdcapirequests.IGetAuthToken {
	return s.authTokenRequest
}

func (s *sdcApi) AccessGetApplicationByApplicationIdRequest() sdcapirequests.IGetApplicationByApplicationId {
	return s.getApplicationByApplicationId
}
func (s *sdcApi) AccessGetApplicationByExternaIdRequest() sdcapirequests.IGetApplicationByExternalIdId {
	return s.getApplicationByExternalId
}

func (s *sdcApi) AccessCreateUserRequest() sdcapirequests.ICreateUser {
	return s.createUser
}

func (s *sdcApi) AccessGetUserGroupByServiceId() sdcapirequests.IGetUserGroupByServiceId {
	return s.getUserGroupByServiceId
}
func (s *sdcApi) AccessGetUserGroups() sdcapirequests.IGetUserGroups {
	return s.getUserGroups
}
func (s *sdcApi) AccessCreateUserGroup() sdcapirequests.ICreateUserGroup {
	return s.createUserGroup
}

func (s *sdcApi) AccessUpdateUserbyId() sdcapirequests.IUpdateUserById {
	return s.updateuserBySdcAccount
}

func (s *sdcApi) AccessGetAttachment() sdcapirequests.IGetAttachment {
	return s.getAttachement
}

func (s *sdcApi) AccessUpdateApplicationAttachment() sdcapirequests.IUpdateApplicationAttachment {
	return s.updateApplicationAttachment
}

func (s *sdcApi) AccessUpdateExternaIdApplication() sdcapirequests.IUpdateExternalIdApplication {
	return s.updateExternalIdApplication
}
func (s *sdcApi) AccessApplicationChangeStatus() sdcapirequests.IApplicationChangeStatus {
	return s.applicationChangeStatus
}

func (s *sdcApi) AccessCreateMessage() sdcapirequests.ICreateMessage {
	return s.createMessage
}

func (s *sdcApi) AccessUpdateExternalIdComment() sdcapirequests.IUpdateExternalIdComment {
	return s.updateExternalIdComment
}

func (s *sdcApi) AccessGetCategories() sdcapirequests.IGetCategories {
	return s.getCategories
}

func (s *sdcApi) AccessGetuserById() sdcapirequests.IGetUserById {
	return s.getUser
}
func (s *sdcApi) AccessCreateApplication() sdcapirequests.ICreateApplication {
	return s.createApplication
}
