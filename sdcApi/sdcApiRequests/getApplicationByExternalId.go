package sdcapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetApplicationByExternalIdId interface {
	SetInputValues(input sdcapistructs.GetApplicationByExternalIdIdInputValues)
	GetApplicationByExternalIdId(token, externaId string) (sdcapistructs.GetApplicationByExternalIdIdSuccessResponse, int, error)
}

type getApplicationByExternalIdId struct {
	inputValues    sdcapistructs.GetApplicationByExternalIdIdInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues sdcapistructs.GetApplicationByExternalIdIdResponse
	statusCode     int
}

func NewGetApplicationByExternalIdId(log *zap.Logger, config *config.Config) IGetApplicationByExternalIdId {
	return &getApplicationByExternalIdId{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (r *getApplicationByExternalIdId) SetInputValues(input sdcapistructs.GetApplicationByExternalIdIdInputValues) {
	r.inputValues = input
}

func (r *getApplicationByExternalIdId) GetApplicationByExternalIdId(token, externaId string) (sdcapistructs.GetApplicationByExternalIdIdSuccessResponse, int, error) {
	err := r.doGetApplicationByExternalIdIdRequest(token, externaId)

	return r.responseValues.GetApplicationByExternalIdIdSuccessResponse, r.statusCode, err
}

func (r *getApplicationByExternalIdId) doGetApplicationByExternalIdIdRequest(token, externaId string) error {

	req, err := r.buildRequest(token, externaId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing GetApplicationByExternalIdId request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleGetApplicationByExternalIdIdResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (r *getApplicationByExternalIdId) buildRequest(token, externaId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/byexternal-id/" + externaId
	r.logger.Sugar().Debug(" getApplicationByExternalIdId url: ", url)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (r *getApplicationByExternalIdId) handleGetApplicationByExternalIdIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code getApplicationByExternalIdId: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body getApplicationByExternalIdId response: ", zap.Error(err))
		}
		r.logger.Sugar().Debug("bad getApplicationByExternalIdId response: ", string(bodyBytes))
		return errors.New("getApplicationByExternalIdI bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.GetApplicationByExternalIdIdSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body getApplicationByExternalIdI response: ", zap.Error(err))
		return err
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		r.logger.Error("Failed to read request body", zap.Error(err))

	}

	// Convert the body content to a string
	bodyString := string(bodyBytes)

	// Log the body content using the Zap logger
	r.logger.Sugar().Debug("MESSAGES-> Request Body ", bodyString)
	r.statusCode = resp.StatusCode
	return nil
}

func (r *getApplicationByExternalIdId) cleanResources() {
	r.logger.Sugar().Debug("getApplicationByExternalIdId: cleaning resources after response")
	r.responseValues = sdcapistructs.GetApplicationByExternalIdIdResponse{}
	r.inputValues = sdcapistructs.GetApplicationByExternalIdIdInputValues{}

}
