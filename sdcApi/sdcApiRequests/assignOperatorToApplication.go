package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IAssignOperatorToApplication interface {
	SetInputValues(input sdcapistructs.AssignOperatorToApplicationInputValues)
	AssignOperatorToApplication(token string) (sdcapistructs.AssignOperatorToApplicationSuccessResponse, error)
}

type AssignOperatorToApplication struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.AssignOperatorToApplicationInputValues
	responseValues sdcapistructs.AssignOperatorToApplicationResponse
}

func NewAssignOperatorToApplication(log *zap.Logger, config *config.Config) IAssignOperatorToApplication {
	return &AssignOperatorToApplication{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *AssignOperatorToApplication) SetInputValues(input sdcapistructs.AssignOperatorToApplicationInputValues) {
	r.inputValues = input
}
func (r *AssignOperatorToApplication) AssignOperatorToApplication(token string) (sdcapistructs.AssignOperatorToApplicationSuccessResponse, error) {
	err := r.doAssignOperatorToApplicationRequest(token)
	if err != nil {
		return r.responseValues.AssignOperatorToApplicationSuccessResponse, err
	}
	defer r.cleanResources()
	return r.responseValues.AssignOperatorToApplicationSuccessResponse, nil
}

func (r *AssignOperatorToApplication) doAssignOperatorToApplicationRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing CreateUser request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleAssignOperatorToApplicationResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *AssignOperatorToApplication) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "users"

	data := map[string]interface{}{
		"topic_id": "",
		/* 	"manager_id":        r.inputValues.ManagerID,
		"core_location_id":  r.inputValues.CoreLocationID,
		"calendar_id":       r.inputValues.CalendarID,
		"name":              r.inputValues.Name,
		"short_description": r.inputValues.ShortDescription,
		"main_function":     r.inputValues.MainFunction,
		"more_info":         r.inputValues.MoreInfo,
		"core_contact_point": map[string]interface{}{
			"name":         r.inputValues.CoreContactPoint.Name,
			"email":        r.inputValues.CoreContactPoint.Email,
			"url":          r.inputValues.CoreContactPoint.URL,
			"phone_number": r.inputValues.CoreContactPoint.PhoneNumber,
			"pec":          r.inputValues.CoreContactPoint.Pec,
		},*/
	}

	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *AssignOperatorToApplication) handleAssignOperatorToApplicationResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code AssignOperatorToApplication: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		err := json.NewDecoder(resp.Body).Decode(&r.responseValues.AssignOperatorToApplicationBadResponse)
		if err != nil {
			r.logger.Error("error decoding body AssignOperatorToApplication response: ", zap.Error(err))
			return err
		}
		r.logger.Sugar().Debug("bad AssignOperatorToApplication response: ", r.responseValues.AssignOperatorToApplicationBadResponse)
		r.logger.Sugar().Debug("bad CreateUser status code : ", resp.StatusCode)
		return errors.New("CreateUser bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.AssignOperatorToApplicationSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body AssignOperatorToApplication response: ", zap.Error(err))
		return err
	}

	return nil
}

func (r *AssignOperatorToApplication) cleanResources() {
	r.logger.Sugar().Debug("AddAttachmentRequest: cleaning resources after response")
	r.responseValues = sdcapistructs.AssignOperatorToApplicationResponse{}
}
