package sdcapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetUserById interface {
	SetInputValues(input sdcapistructs.GetUserByIdInputValues)
	GetUserById(token string) (sdcapistructs.UserInfo, error)
	IsUserFound() bool
}

type getUserById struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.GetUserByIdInputValues
	responseValues sdcapistructs.GetUserByIdResponse
	userFound      bool
}

func NewGetUserById(log *zap.Logger, config *config.Config) IGetUserById {
	return &getUserById{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *getUserById) SetInputValues(input sdcapistructs.GetUserByIdInputValues) {
	r.inputValues = input
}
func (r *getUserById) GetUserById(token string) (sdcapistructs.UserInfo, error) {
	err := r.doGetUserByIdRequest(token)
	defer r.cleanResources()
	return r.responseValues.UserInfo, err
}
func (r *getUserById) IsUserFound() bool {
	return r.userFound
}
func (r *getUserById) doGetUserByIdRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing GetUserById request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleGetApplicationByApplicationIdResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *getUserById) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "users/" + r.inputValues.UserId

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *getUserById) handleGetApplicationByApplicationIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code getUserById: ", resp.StatusCode)
	if resp.StatusCode == http.StatusNotFound {
		r.userFound = false
	}
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetApplicationByApplicationId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetApplicationByApplicationId response: ", string(bodyBytes))
		return errors.New("getUserById bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.UserInfo)
	if err != nil {
		r.logger.Sugar().Error("error decoding body getUserById response: ", zap.Error(err))
		return err
	}
	r.userFound = true
	return nil
}

func (r *getUserById) cleanResources() {
	r.logger.Sugar().Debug("applicationChangeStatus: cleaning resources after response")
	r.responseValues = sdcapistructs.GetUserByIdResponse{}
	r.inputValues = sdcapistructs.GetUserByIdInputValues{}

}
