package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetAuthToken interface {
	GetAuthToken() (string, error)
}

type getAuthToken struct {
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues sdcapistructs.GetAuthenticationTokenResponse
}

func NewGetAuthToken(log *zap.Logger, config *config.Config) IGetAuthToken {
	return &getAuthToken{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (at *getAuthToken) GetAuthToken() (string, error) {
	err := at.doGetAuthTokenRequest()
	if err != nil {
		return "", err
	}
	return at.responseValues.GetAuthenticationTokenSuccessResponse.Token, nil
}

func (at *getAuthToken) doGetAuthTokenRequest() error {

	req, err := at.buildRequest()
	if err != nil {
		return err
	}

	// make the request
	resp, err := at.client.Do(req)
	if err != nil {
		at.logger.Sugar().Error("error performing sdcGetAuthToken request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = at.handleAuthTokenResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (at *getAuthToken) buildRequest() (*http.Request, error) {
	url := at.conf.Sdc.SdcBaseName + "auth"
	data := map[string]string{
		"username": at.conf.Sdc.AuthTokenUser,
		"password": at.conf.Sdc.AuthTokenPassword,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		at.logger.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		at.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func (at *getAuthToken) handleAuthTokenResponse(resp *http.Response) error {

	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			at.logger.Error("error decoding body AuthToken response: ", zap.Error(err))
		}

		at.logger.Sugar().Debug("bad AuthToken response: ", string(bodyBytes))
		return errors.New("auth token bad error response")
	}
	err := json.NewDecoder(resp.Body).Decode(&at.responseValues.GetAuthenticationTokenSuccessResponse)
	if err != nil {
		at.logger.Sugar().Error("error decoding body auth response: ", zap.Error(err))
		return err
	}
	return nil
}
