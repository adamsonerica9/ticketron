package sdcapirequests

import (
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type Category struct {
	Label string `json:"label"`
	Value string `json:"value"`
}

type IGetCategories interface {
	GetCategories() ([]Category, error)
}

type getCategories struct {
	client       *http.Client
	logger       *zap.Logger
	conf         *config.Config
	categoryList []Category
}

func NewGetCategories(log *zap.Logger, config *config.Config) IGetCategories {
	return &getCategories{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (at *getCategories) GetCategories() ([]Category, error) {
	err := at.doGetCategoriesRequest()
	if err != nil {
		return at.categoryList, err
	}
	defer at.cleanResources()
	return at.categoryList, nil
}

func (at *getCategories) doGetCategoriesRequest() error {

	req, err := at.buildRequest()
	if err != nil {
		return err
	}

	// make the request
	resp, err := at.client.Do(req)
	if err != nil {
		at.logger.Sugar().Error("error performing sdcGetCategories request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = at.handleGetCategoriesResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (at *getCategories) buildRequest() (*http.Request, error) {

	req, err := http.NewRequest(http.MethodGet, at.conf.Sdc.CategoriesBaseName, nil)
	if err != nil {
		at.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}
	return req, nil
}

func (at *getCategories) handleGetCategoriesResponse(resp *http.Response) error {
	if resp.StatusCode != http.StatusOK {
		at.logger.Sugar().Error("unexpected status code: ", zap.Int("status", resp.StatusCode))
		return errors.New("unexpected status code" + string(resp.StatusCode))
	}

	// Read the binary file from the response body
	err := json.NewDecoder(resp.Body).Decode(&at.categoryList)
	if err != nil {
		at.logger.Sugar().Error("error reading response body: ", zap.Error(err))
		return err
	}

	return nil
}

func (r *getCategories) cleanResources() {
	r.logger.Sugar().Debug("getCategories: cleaning resources after response")
	r.categoryList = []Category{}

}
