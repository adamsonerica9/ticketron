package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IUpdateUserById interface {
	SetInputValues(input sdcapistructs.UpdateUserByIdInputValues)
	UpdateUserById(token, accountId string) (sdcapistructs.UpdateUserByIdBadResponse, error)
}

type UpdateUserById struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.UpdateUserByIdInputValues
	responseValues sdcapistructs.UpdateUserByIdResponse
}

func NewUpdateUserById(log *zap.Logger, config *config.Config) IUpdateUserById {
	return &UpdateUserById{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *UpdateUserById) SetInputValues(input sdcapistructs.UpdateUserByIdInputValues) {
	r.inputValues = input
}
func (r *UpdateUserById) UpdateUserById(token, accountId string) (sdcapistructs.UpdateUserByIdBadResponse, error) {
	err := r.doUpdateUserByIdRequest(token, accountId)
	defer r.cleanResources()
	return r.responseValues.UpdateUserByIdBadResponse, err
}

func (r *UpdateUserById) doUpdateUserByIdRequest(token, accountId string) error {

	req, err := r.buildRequest(token, accountId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing UpdateUserById request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleUpdateUserByIdResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *UpdateUserById) buildRequest(token, accountId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "users/" + accountId

	data := map[string]interface{}{
		"nome":           r.inputValues.Nome,
		"cognome":        r.inputValues.Cognome,
		"cellulare":      r.inputValues.Cellulare,
		"email":          r.inputValues.Email,
		"codice_fiscale": r.inputValues.CodiceFiscale,
		"telefono":       r.inputValues.Telefono,
	}
	payload, err := json.Marshal(data)
	r.logger.Sugar().Debug("UpdateUserById payload: ", data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("UpdateUserById payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *UpdateUserById) handleUpdateUserByIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code UpdateUserById: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNotFound {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body UpdateUserById response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad UpdateUserById response: ", string(bodyBytes))
		return errors.New("UpdateUserById bad error response")
	}
	if resp.StatusCode == http.StatusNotFound {
		r.logger.Sugar().Debug("UpdateUserById object not found : ")
		return nil
	}
	r.logger.Sugar().Debug("UpdateUserById object correctly updated on sdc ")

	return nil
}

func (r *UpdateUserById) cleanResources() {
	r.logger.Sugar().Debug("applicationChangeStatus: cleaning resources after response")
	r.responseValues = sdcapistructs.UpdateUserByIdResponse{}
	r.inputValues = sdcapistructs.UpdateUserByIdInputValues{}

}
