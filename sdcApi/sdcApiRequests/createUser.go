package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICreateUser interface {
	SetInputValues(input sdcapistructs.CreateUserInputValues)
	CreateUser(token string) (sdcapistructs.UserInfo, error)
}

type createUser struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.CreateUserInputValues
	responseValues sdcapistructs.CreateUserResponse
}

func NewCreateUser(log *zap.Logger, config *config.Config) ICreateUser {
	return &createUser{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *createUser) SetInputValues(input sdcapistructs.CreateUserInputValues) {
	r.inputValues = input
}
func (r *createUser) CreateUser(token string) (sdcapistructs.UserInfo, error) {
	err := r.doCreateUserRequest(token)
	if err != nil {
		return r.responseValues.CreateUserSuccessResponse, err
	}
	defer r.cleanResources()
	return r.responseValues.CreateUserSuccessResponse, nil
}

func (r *createUser) doCreateUserRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing CreateUser request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleCreateUserResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *createUser) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "users"

	data := map[string]interface{}{
		"nome":                r.inputValues.Nome,
		"cognome":             r.inputValues.Cognome,
		"cellulare":           r.inputValues.Cellulare,
		"email":               r.inputValues.Email,
		"codice_fiscale":      r.inputValues.CodiceFiscale,
		"data_nascita":        r.inputValues.DataNascita,
		"telefono":            r.inputValues.Telefono,
		"indirizzo_domicilio": r.inputValues.IndirizzoDomicilio,
		"cap_domicilio":       r.inputValues.CapDomicilio,
		"citta_domicilio":     r.inputValues.CittaDomicilio,
		"provincia_domicilio": r.inputValues.ProvinciaDomicilio,
		"stato_domicilio":     r.inputValues.StatoDomicilio,
		"indirizzo_residenza": r.inputValues.IndirizzoResidenza,
		"cap_residenza":       r.inputValues.CapResidenza,
		"citta_residenza":     r.inputValues.CittaResidenza,
		"provincia_residenza": r.inputValues.ProvinciaResidenza,
		"stato_residenza":     r.inputValues.StatoResidenza,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("createuser payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *createUser) handleCreateUserResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code CreateUser: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body CreateAccount response: ", zap.Error(err))
		}
		r.logger.Sugar().Debug("bad CreateUser response: ", string(bodyBytes))
		return errors.New("CreateUser bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.CreateUserSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body CreateUser response: ", zap.Error(err))
		return err
	}

	return nil
}

func (r *createUser) cleanResources() {
	r.logger.Sugar().Debug("createUser: cleaning resources after response")
	r.responseValues = sdcapistructs.CreateUserResponse{}
	r.inputValues = sdcapistructs.CreateUserInputValues{}

}
