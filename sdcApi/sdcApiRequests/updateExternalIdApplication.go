package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type IUpdateExternalIdApplication interface {
	SetInputValues(externalID string)
	UpdateExternalIdApplication(token, applicationId, externalId string) error
}

type updateExternalIdApplication struct {
	logger      *zap.Logger
	conf        *config.Config
	client      *http.Client
	inputValues string
}

func NewUpdateExternalIdApplication(log *zap.Logger, config *config.Config) IUpdateExternalIdApplication {
	return &updateExternalIdApplication{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *updateExternalIdApplication) SetInputValues(externalID string) {
	r.inputValues = externalID
}
func (r *updateExternalIdApplication) UpdateExternalIdApplication(token, applicationId, externalId string) error {
	err := r.doUpdateExternalIdApplicationRequest(token, applicationId, externalId)
	defer r.cleanResources()
	return err
}

func (r *updateExternalIdApplication) doUpdateExternalIdApplicationRequest(token, applicationId, externalId string) error {

	req, err := r.buildRequest(token, applicationId, externalId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing UpdateExternalIdApplication request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleUpdateExternalIdApplicationResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *updateExternalIdApplication) buildRequest(token, applicationId, externalId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/" + applicationId

	data := map[string]interface{}{
		"external_id": externalId,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("UpdateExternalIdApplication payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *updateExternalIdApplication) handleUpdateExternalIdApplicationResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code UpdateExternalIdApplication: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNotFound {
		r.logger.Sugar().Debug("bad UpdateExternalIdApplication status code resp.StatusCode")
		return errors.New("UpdateExternalIdApplication bad error response")
	}
	r.logger.Sugar().Debug("UpdateExternalIdApplication object correctly updated on sdc ")

	return nil
}

func (r *updateExternalIdApplication) cleanResources() {
	r.logger.Sugar().Debug("updateExternalIdApplication: cleaning resources after response")
	r.inputValues = ""

}
