package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICreateUserGroup interface {
	SetInputValues(input sdcapistructs.CreateUserGroupInputValues)
	CreateUserGroup(token string) (sdcapistructs.CreateUserGroupSuccessResponse, error)
}

type CreateUserGroup struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.CreateUserGroupInputValues
	responseValues sdcapistructs.CreateUserGroupResponse
}

func NewCreateUserGroup(log *zap.Logger, config *config.Config) ICreateUserGroup {
	return &CreateUserGroup{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *CreateUserGroup) SetInputValues(input sdcapistructs.CreateUserGroupInputValues) {
	r.inputValues = input
}
func (r *CreateUserGroup) CreateUserGroup(token string) (sdcapistructs.CreateUserGroupSuccessResponse, error) {
	err := r.doCreateUserGroupRequest(token)
	if err != nil {
		return r.responseValues.CreateUserGroupSuccessResponse, err
	}
	defer r.cleanResources()
	return r.responseValues.CreateUserGroupSuccessResponse, nil
}

func (r *CreateUserGroup) doCreateUserGroupRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing CreateUser request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleCreateUserGroupResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *CreateUserGroup) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "users"

	data := map[string]interface{}{
		"name": r.inputValues.Name,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("createUser payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *CreateUserGroup) handleCreateUserGroupResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code CreateUserGroup: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body CreateUserGroup response: ", zap.Error(err))
		}
		r.logger.Sugar().Debug("bad CreateUserGroup response: ", string(bodyBytes))

		return errors.New("CreateUser bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.CreateUserGroupSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body CreateUserGroup response: ", zap.Error(err))
		return err
	}

	return nil
}

func (r *CreateUserGroup) cleanResources() {
	r.logger.Sugar().Debug("applicationChangeStatus: cleaning resources after response")
	r.responseValues = sdcapistructs.CreateUserGroupResponse{}
	r.inputValues = sdcapistructs.CreateUserGroupInputValues{}

}
