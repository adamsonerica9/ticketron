package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICreateApplication interface {
	SetInputValues(input sdcapistructs.CreateApplicationInputValues)
	CreateApplication(token string) error
}

type CreateApplication struct {
	logger      *zap.Logger
	conf        *config.Config
	client      *http.Client
	inputValues sdcapistructs.CreateApplicationInputValues
}

func NewCreateApplication(log *zap.Logger, config *config.Config) ICreateApplication {
	return &CreateApplication{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *CreateApplication) SetInputValues(input sdcapistructs.CreateApplicationInputValues) {
	r.inputValues = input
}
func (r *CreateApplication) CreateApplication(token string) error {
	err := r.doCreateApplicationRequest(token)
	if err != nil {
		return err
	}
	defer r.cleanResources()
	return nil
}

func (r *CreateApplication) doCreateApplicationRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing CreateApplication request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleCreateApplicationResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *CreateApplication) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/"

	/* 	data := map[string]interface{}{
		"message":     r.inputValues.Message,
		"sent_at":     r.inputValues.SentAt,
		"external_id": r.inputValues.ExternalID,
	} */
	r.logger.Sugar().Debug("CreateApplication url: ", url)
	r.logger.Sugar().Debug("CreateApplication payload: ", r.inputValues)
	payload, err := json.Marshal(r.inputValues)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *CreateApplication) handleCreateApplicationResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code CreateApplication: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		r.logger.Sugar().Debug("bad CreaApplication failed. response: ", string(bodyBytes))
		return errors.New("CreateApplication bad error response")
	}
	return nil
}

func (r *CreateApplication) cleanResources() {
	r.logger.Sugar().Debug("CreateApplication: cleaning resources after response")
	r.inputValues = sdcapistructs.CreateApplicationInputValues{}

}
