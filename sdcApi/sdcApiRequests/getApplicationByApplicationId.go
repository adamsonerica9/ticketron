package sdcapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetApplicationByApplicationId interface {
	SetInputValues(input sdcapistructs.GetApplicationByApplicationIdInputValues)
	GetApplicationByApplicationId(token string) (sdcapistructs.GetApplicationByApplicationIdSuccessResponse, error)
}

type getApplicationByApplicationId struct {
	inputValues    sdcapistructs.GetApplicationByApplicationIdInputValues
	client         *http.Client
	logger         *zap.Logger
	conf           *config.Config
	responseValues sdcapistructs.GetApplicationByApplicationIdResponse
}

func NewGetApplicationByApplicationId(log *zap.Logger, config *config.Config) IGetApplicationByApplicationId {
	return &getApplicationByApplicationId{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (r *getApplicationByApplicationId) SetInputValues(input sdcapistructs.GetApplicationByApplicationIdInputValues) {
	r.inputValues = input
}

func (r *getApplicationByApplicationId) GetApplicationByApplicationId(token string) (sdcapistructs.GetApplicationByApplicationIdSuccessResponse, error) {
	err := r.doGetApplicationByApplicationIdRequest(token)
	defer r.cleanResources()
	return r.responseValues.GetApplicationByApplicationIdSuccessResponse, err
}

func (r *getApplicationByApplicationId) doGetApplicationByApplicationIdRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing GetApplicationByApplicationId request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleGetApplicationByApplicationIdResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (r *getApplicationByApplicationId) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/" + r.inputValues.ApplicationId

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (r *getApplicationByApplicationId) handleGetApplicationByApplicationIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code getApplicationByApplicationId: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body getApplicationByApplicationId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad getApplicationByApplicationId response: ", string(bodyBytes))
		return errors.New("getApplicationByApplicationId bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.GetApplicationByApplicationIdSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body getApplicationByApplicationId response: ", zap.Error(err))
		return err
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		r.logger.Error("Failed to read request body", zap.Error(err))

	}

	// Convert the body content to a string
	bodyString := string(bodyBytes)

	// Log the body content using the Zap logger
	r.logger.Sugar().Debug("MESSAGES-> Request Body ", bodyString)
	return nil
}

func (r *getApplicationByApplicationId) cleanResources() {
	r.logger.Sugar().Debug("getApplicationByApplicationId: cleaning resources after response")
	r.responseValues = sdcapistructs.GetApplicationByApplicationIdResponse{}
	r.inputValues = sdcapistructs.GetApplicationByApplicationIdInputValues{}

}
