package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICreateMessage interface {
	SetInputValues(input sdcapistructs.CreateMessageInputValues)
	CreateMessage(token, applicationId string) error
}

type createMessage struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.CreateMessageInputValues
	responseValues sdcapistructs.CreateMessageBadResponse
}

func NewCreateMessage(log *zap.Logger, config *config.Config) ICreateMessage {
	return &createMessage{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *createMessage) SetInputValues(input sdcapistructs.CreateMessageInputValues) {
	r.inputValues = input
}
func (r *createMessage) CreateMessage(token, applicationId string) error {
	err := r.doCreateMessageRequest(token, applicationId)
	if err != nil {
		return err
	}
	defer r.cleanResources()
	return nil
}

func (r *createMessage) doCreateMessageRequest(token, applicationId string) error {

	req, err := r.buildRequest(token, applicationId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing CreateMessage request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleCreateMessageResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *createMessage) buildRequest(token, applicationId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/" + applicationId + "/messages"

	data := map[string]interface{}{
		"message":     r.inputValues.Message,
		"sent_at":     r.inputValues.SentAt,
		"external_id": r.inputValues.ExternalID,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("createMessage payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *createMessage) handleCreateMessageResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code CreateMessage: ", resp.StatusCode)
	if resp.StatusCode != http.StatusCreated {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body CreateAccount response: ", zap.Error(err))
		}
		r.logger.Sugar().Debug("bad CreaMessage failed. response: ", string(bodyBytes))
		return errors.New("CreateMessage bad error response")
	}
	return nil
}

func (r *createMessage) cleanResources() {
	r.logger.Sugar().Debug("createMessage: cleaning resources after response")
	r.inputValues = sdcapistructs.CreateMessageInputValues{}

}
