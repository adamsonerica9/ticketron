package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IUpdateApplicationAttachment interface {
	SetInputValues(externalID string)
	UpdateApplicationAttachment(token, applicationId, attachmentId string) ([]string, error)
}

type updateApplicationAttachment struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    string
	responseValues sdcapistructs.UpdateApplicationAttachmentResponse
}

func NewUpdateApplicationAttachment(log *zap.Logger, config *config.Config) IUpdateApplicationAttachment {
	return &updateApplicationAttachment{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *updateApplicationAttachment) SetInputValues(externalID string) {
	r.inputValues = externalID
}
func (r *updateApplicationAttachment) UpdateApplicationAttachment(token, applicationId, attachmentId string) ([]string, error) {
	err := r.doUpdateApplicationAttachmentRequest(token, applicationId, attachmentId)
	defer r.cleanResources()
	return r.responseValues.Response, err
}

func (r *updateApplicationAttachment) doUpdateApplicationAttachmentRequest(token, applicationId, attachmentId string) error {

	req, err := r.buildRequest(token, applicationId, attachmentId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing UpdateApplicationAttachment request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleUpdateApplicationAttachmentResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *updateApplicationAttachment) buildRequest(token, applicationId, attachmentId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/" + applicationId + "/attachments/" + attachmentId

	data := map[string]interface{}{
		"external_id": r.inputValues,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("UpdateApplicationAttachment payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *updateApplicationAttachment) handleUpdateApplicationAttachmentResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code UpdateApplicationAttachment: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNotFound {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body UpdateApplicationAttachment response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad UpdateApplicationAttachment response: ", string(bodyBytes))
		return errors.New("UpdateApplicationAttachment bad error response")
	}
	if resp.StatusCode == http.StatusNotFound {
		r.logger.Sugar().Debug("UpdateApplicationAttachment object not found : ")
		return nil
	}
	r.logger.Sugar().Debug("UpdateApplicationAttachment object correctly updated on sdc ")

	return nil
}
func (r *updateApplicationAttachment) cleanResources() {
	r.logger.Sugar().Debug("applicationChangeStatus: cleaning resources after response")
	r.responseValues = sdcapistructs.UpdateApplicationAttachmentResponse{}
	r.inputValues = ""

}
