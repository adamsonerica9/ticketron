package sdcapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetUserGroups interface {
	GetUserGroups(token string) (sdcapistructs.GetUserGroupsSuccesResponse, error)
}

type getUserGroups struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	responseValues sdcapistructs.GetUserGroupsSuccesResponse
}

func NewGetUserGroups(log *zap.Logger, config *config.Config) IGetUserGroups {
	return &getUserGroups{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (r *getUserGroups) GetUserGroups(token string) (sdcapistructs.GetUserGroupsSuccesResponse, error) {
	err := r.doGetUserGroupsRequest(token)
	defer r.cleanResources()
	return r.responseValues, err
}

func (r *getUserGroups) doGetUserGroupsRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing GetUserGroup request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleGetUserGroupsResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (r *getUserGroups) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "user-groups"

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (r *getUserGroups) handleGetUserGroupsResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code GetUserGroups response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetUserGroups response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetUserGroups response: ", string(bodyBytes))
		return errors.New("getUserById bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues)
	if err != nil {
		r.logger.Sugar().Error("error decoding body GetUserGroups response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *getUserGroups) cleanResources() {
	r.logger.Sugar().Debug("getUserGroup: cleaning resources after response")
	r.responseValues = sdcapistructs.GetUserGroupsSuccesResponse{}

}
