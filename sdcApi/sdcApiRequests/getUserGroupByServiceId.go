package sdcapirequests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IGetUserGroupByServiceId interface {
	SetInputValues(input sdcapistructs.GetUserGroupByServiceIdInputValues)
	GetUserGroupByServiceId(token string) (sdcapistructs.GetUserGroupByServiceIdSuccessResponse, error)
}

type getUserGroupByServiceId struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.GetUserGroupByServiceIdInputValues
	responseValues sdcapistructs.GetUserGroupByServiceIdResponse
}

func NewGetUserGroupByServiceId(log *zap.Logger, config *config.Config) IGetUserGroupByServiceId {
	return &getUserGroupByServiceId{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (r *getUserGroupByServiceId) SetInputValues(input sdcapistructs.GetUserGroupByServiceIdInputValues) {
	r.inputValues = input
}

func (r *getUserGroupByServiceId) GetUserGroupByServiceId(token string) (sdcapistructs.GetUserGroupByServiceIdSuccessResponse, error) {
	err := r.doGetUserGroupByServiceIdRequest(token)
	defer r.cleanResources()
	return r.responseValues.GetUserGroupByServiceIdSuccessResponse, err
}

func (r *getUserGroupByServiceId) doGetUserGroupByServiceIdRequest(token string) error {

	req, err := r.buildRequest(token)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing GetUserGroupByServiceId request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleGetUserGroupByServiceIdResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (r *getUserGroupByServiceId) buildRequest(token string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "user-groups/" + r.inputValues.ServiceId

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (r *getUserGroupByServiceId) handleGetUserGroupByServiceIdResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code GetUserGroupByServiceId response: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			r.logger.Error("error decoding body GetUserGroupByServiceId response: ", zap.Error(err))
		}

		r.logger.Sugar().Debug("bad GetUserGroupByServiceId response: ", string(bodyBytes))
		return errors.New("getUserById bad error response")
	}

	err := json.NewDecoder(resp.Body).Decode(&r.responseValues.GetUserGroupByServiceIdSuccessResponse)
	if err != nil {
		r.logger.Sugar().Error("error decoding body getUserById response: ", zap.Error(err))
		return err
	}
	return nil
}

func (r *getUserGroupByServiceId) cleanResources() {
	r.logger.Sugar().Debug("getUserGroupByServiceId: cleaning resources after response")
	r.responseValues = sdcapistructs.GetUserGroupByServiceIdResponse{}
	r.inputValues = sdcapistructs.GetUserGroupByServiceIdInputValues{}

}
