package sdcapirequests

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type IGetAttachment interface {
	GetAttachment(token, url string) (string, error)
}

type getAttachment struct {
	client   *http.Client
	logger   *zap.Logger
	conf     *config.Config
	response string
}

func NewGetAttachment(log *zap.Logger, config *config.Config) IGetAttachment {
	return &getAttachment{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}

func (at *getAttachment) GetAttachment(token, url string) (string, error) {
	err := at.doGetAttachmentRequest(token, url)
	if err != nil {
		return "", err
	}
	defer at.cleanResources()
	return at.response, nil
}

func (at *getAttachment) doGetAttachmentRequest(token, url string) error {

	req, err := at.buildRequest(token, url)
	if err != nil {
		return err
	}

	// make the request
	resp, err := at.client.Do(req)
	if err != nil {
		at.logger.Sugar().Error("error performing sdcGetAttachment request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = at.handleGetAttachmentResponse(resp)
	if err != nil {
		return err
	}

	return nil
}

func (at *getAttachment) buildRequest(token, url string) (*http.Request, error) {

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		at.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (at *getAttachment) handleGetAttachmentResponse(resp *http.Response) error {
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			at.logger.Error("error decoding body GetAttachment response: ", zap.Error(err))
		}
		at.logger.Sugar().Debug("bad GetAttachment response: ", string(bodyBytes))
		return errors.New("GetAttachment bad response")
	}

	// Read the binary file from the response body
	fileBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		at.logger.Sugar().Error("error reading response body: ", zap.Error(err))
		return err
	}

	// Convert the file contents to base64
	at.response = base64.StdEncoding.EncodeToString(fileBytes)

	return nil
}

func (r *getAttachment) cleanResources() {
	r.logger.Sugar().Debug("getAttachment: cleaning resources after response")
	r.response = ""

}
