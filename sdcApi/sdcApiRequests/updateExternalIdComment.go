package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type IUpdateExternalIdComment interface {
	UpdateExternalIdComment(token, applicationId, commentId, externalId string) error
}

type updateExternalIdComment struct {
	logger *zap.Logger
	conf   *config.Config
	client *http.Client
}

func NewUpdateExternalIdComment(log *zap.Logger, config *config.Config) IUpdateExternalIdComment {
	return &updateExternalIdComment{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *updateExternalIdComment) UpdateExternalIdComment(token, applicationId, commentId, externalId string) error {
	err := r.doUpdateExternalIdCommentRequest(token, applicationId, commentId, externalId)

	return err
}

func (r *updateExternalIdComment) doUpdateExternalIdCommentRequest(token, applicationId, commentId, externalId string) error {

	req, err := r.buildRequest(token, applicationId, commentId, externalId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing UpdateExternalIdComment request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleUpdateExternalIdCommentResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *updateExternalIdComment) buildRequest(token, applicationId, commentId, externalId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications/" + applicationId + "/messages/" + commentId

	data := map[string]interface{}{
		"external_id": externalId,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *updateExternalIdComment) handleUpdateExternalIdCommentResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code UpdateExternalIdComment: ", resp.StatusCode)
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNotFound {
		r.logger.Sugar().Debug("bad UpdateExternalIdComment status code : ", resp.StatusCode)
	}
	if resp.StatusCode == http.StatusNotFound {
		r.logger.Sugar().Debug("UpdateExternalIdComment object not found : ")
		return nil
	}
	r.logger.Sugar().Debug("UpdateExternalIdComment object correctly updated on sdc ")

	return nil
}
