package sdcapistructs

type GetAuthenticationTokenResponse struct {
	GetAuthenticationTokenSuccessResponse GetAuthenticationTokenSuccessResponse
	GetAuthenticationTokenBadResponse     GetAuthenticationTokenBadResponse
}

type GetAuthenticationTokenSuccessResponse struct {
	Token string `json:"token"`
}

type GetAuthenticationTokenBadResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
