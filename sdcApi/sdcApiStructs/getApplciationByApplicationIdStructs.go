package sdcapistructs

// queste me le recupero chiamando le API
type GetApplicationByApplicationIdInputValues struct {
	ApplicationId string
}

type GetApplicationByApplicationIdResponse struct {
	GetApplicationByApplicationIdSuccessResponse GetApplicationByApplicationIdSuccessResponse
	GetApplicationByApplicationIdBadResponse     GetApplicationByApplicationIdBadResponse
}

type GetApplicationByApplicationIdSuccessResponse struct {
	ID               string        `json:"id"`
	User             string        `json:"user"`
	UserName         string        `json:"user_name"`
	Service          string        `json:"service"`
	ServiceID        string        `json:"service_id"`
	ServiceName      string        `json:"service_name"`
	ServiceGroupName interface{}   `json:"service_group_name"`
	Tenant           string        `json:"tenant"`
	Subject          string        `json:"subject"`
	ExternalID       string        `json:"external_id"`
	GeographicAreas  []interface{} `json:"geographic_areas"`
}

type GetApplicationByApplicationIdBadResponse struct {
	Response []string
}
