package sdcapistructs

type CreateUserInputValues struct {
	Nome               string `json:"nome"`
	Cognome            string `json:"cognome"`
	Cellulare          string `json:"cellulare"`
	Email              string `json:"email"`
	CodiceFiscale      string `json:"codice_fiscale"`
	DataNascita        string `json:"data_nascita"`
	LuogoNascita       string `json:"luogo_nascita"`
	CodiceNascita      string `json:"codice_nascita"`
	ProvinciaNascita   string `json:"provincia_nascita"`
	StatoNascita       string `json:"stato_nascita"`
	Sesso              string `json:"sesso"`
	Telefono           string `json:"telefono"`
	IndirizzoDomicilio string `json:"indirizzo_domicilio"`
	CapDomicilio       string `json:"cap_domicilio"`
	CittaDomicilio     string `json:"citta_domicilio"`
	ProvinciaDomicilio string `json:"provincia_domicilio"`
	StatoDomicilio     string `json:"stato_domicilio"`
	IndirizzoResidenza string `json:"indirizzo_residenza"`
	CapResidenza       string `json:"cap_residenza"`
	CittaResidenza     string `json:"citta_residenza"`
	ProvinciaResidenza string `json:"provincia_residenza"`
	StatoResidenza     string `json:"stato_residenza"`
}

type GetUserByIdInputValues struct {
	UserId string
}

type GetUserByIdResponse struct {
	UserInfo               UserInfo
	GetUserByIdBadResponse GetUserByIdBadResponse
}

type GetUserByIdBadResponse struct {
	Response []string
}

type CreateUserResponse struct {
	CreateUserSuccessResponse UserInfo
	CreateUserBadResponse     CreateUserBadResponse
}

type UserInfo struct {
	ID                 string `json:"id"`
	Role               string `json:"role"`
	Nome               string `json:"nome"`
	Cognome            string `json:"cognome"`
	FullName           string `json:"full_name"`
	Cellulare          string `json:"cellulare"`
	Email              string `json:"email"`
	CreatedAt          string `json:"created_at"`
	UpdatedAt          string `json:"updated_at"`
	CodiceFiscale      string `json:"codice_fiscale"`
	DataNascita        string `json:"data_nascita"`
	LuogoNascita       string `json:"luogo_nascita"`
	CodiceNascita      string `json:"codice_nascita"`
	ProvinciaNascita   string `json:"provincia_nascita"`
	StatoNascita       string `json:"stato_nascita"`
	Sesso              string `json:"sesso"`
	Telefono           string `json:"telefono"`
	IndirizzoDomicilio string `json:"indirizzo_domicilio"`
	CapDomicilio       string `json:"cap_domicilio"`
	CittaDomicilio     string `json:"citta_domicilio"`
	ProvinciaDomicilio string `json:"provincia_domicilio"`
	StatoDomicilio     string `json:"stato_domicilio"`
	IndirizzoResidenza string `json:"indirizzo_residenza"`
	CapResidenza       string `json:"cap_residenza"`
	CittaResidenza     string `json:"citta_residenza"`
	ProvinciaResidenza string `json:"provincia_residenza"`
	StatoResidenza     string `json:"stato_residenza"`
	SpidCode           string `json:"spid_code"`
}
type CreateUserBadResponse struct {
	Type   string   `json:"type"`
	Title  string   `json:"title"`
	Errors []string `json:"errors"`
}
