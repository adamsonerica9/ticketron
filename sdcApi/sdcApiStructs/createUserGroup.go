package sdcapistructs

import "time"

type CreateUserGroupInputValues struct {
	TopicID          string `json:"topic_id"`
	ManagerID        string `json:"manager_id"`
	CoreLocationID   string `json:"core_location_id"`
	CalendarID       string `json:"calendar_id"`
	Name             string `json:"name"`
	ShortDescription string `json:"short_description"`
	MainFunction     string `json:"main_function"`
	MoreInfo         string `json:"more_info"`
	CoreContactPoint struct {
		Name        string `json:"name"`
		Email       string `json:"email"`
		URL         string `json:"url"`
		PhoneNumber string `json:"phone_number"`
		Pec         string `json:"pec"`
	} `json:"core_contact_point"`
}

type CreateUserGroupResponse struct {
	CreateUserGroupSuccessResponse CreateUserGroupSuccessResponse
	CreateUserGroupBadResponse     CreateUserGroupBadResponse
}

type CreateUserGroupSuccessResponse struct {
	TopicID          string    `json:"topic_id"`
	ManagerID        string    `json:"manager_id"`
	UsersCount       int       `json:"users_count"`
	ServicesCount    int       `json:"services_count"`
	CoreLocationID   string    `json:"core_location_id"`
	CalendarID       string    `json:"calendar_id"`
	ID               string    `json:"id"`
	Name             string    `json:"name"`
	ShortDescription string    `json:"short_description"`
	MainFunction     string    `json:"main_function"`
	MoreInfo         string    `json:"more_info"`
	CoreContactPoint string    `json:"core_contact_point"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
}
type CreateUserGroupBadResponse struct {
	Type   string `json:"type"`
	Title  string `json:"title"`
	Errors string `json:"errors"`
}
