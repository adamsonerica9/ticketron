package sdcapistructs

type UpdateApplicationAttachmentInputValues struct {
	ExternalID string `json:"external_id"`
}

type UpdateApplicationAttachmentResponse struct {
	Response []string
}
