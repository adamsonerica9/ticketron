package sdcapistructs

import "time"

type UpdateUserByIdInputValues struct {
	Nome          string `json:"nome"`
	Cognome       string `json:"cognome"`
	Cellulare     string `json:"cellulare"`
	Email         string `json:"email"`
	Telefono      string `json:"telefono"`
	CodiceFiscale string `json:"codice_fiscale"`
}

type UpdateUserByIdResponse struct {
	UpdateUserByIdSuccessResponse UpdateUserByIdSuccessResponse
	UpdateUserByIdBadResponse     UpdateUserByIdBadResponse
}

type UpdateUserByIdSuccessResponse struct {
	ID                 string      `json:"id"`
	Role               string      `json:"role"`
	Nome               string      `json:"nome"`
	Cognome            string      `json:"cognome"`
	FullName           string      `json:"full_name"`
	Cellulare          string      `json:"cellulare"`
	Email              string      `json:"email"`
	CreatedAt          time.Time   `json:"created_at"`
	UpdatedAt          time.Time   `json:"updated_at"`
	CodiceFiscale      string      `json:"codice_fiscale"`
	DataNascita        time.Time   `json:"data_nascita"`
	LuogoNascita       string      `json:"luogo_nascita"`
	CodiceNascita      string      `json:"codice_nascita"`
	ProvinciaNascita   string      `json:"provincia_nascita"`
	StatoNascita       string      `json:"stato_nascita"`
	Sesso              string      `json:"sesso"`
	Telefono           string      `json:"telefono"`
	IndirizzoDomicilio string      `json:"indirizzo_domicilio"`
	CapDomicilio       string      `json:"cap_domicilio"`
	CittaDomicilio     string      `json:"citta_domicilio"`
	ProvinciaDomicilio string      `json:"provincia_domicilio"`
	StatoDomicilio     string      `json:"stato_domicilio"`
	IndirizzoResidenza string      `json:"indirizzo_residenza"`
	CapResidenza       string      `json:"cap_residenza"`
	CittaResidenza     string      `json:"citta_residenza"`
	ProvinciaResidenza string      `json:"provincia_residenza"`
	StatoResidenza     string      `json:"stato_residenza"`
	SpidCode           interface{} `json:"spid_code"`
}
type UpdateUserByIdBadResponse struct {
	Type   string   `json:"type"`
	Title  string   `json:"title"`
	Errors []string `json:"errors"`
}
