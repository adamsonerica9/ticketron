package sdcapistructs

type CreateMessageInputValues struct {
	Message    string `json:"message"`
	SentAt     string `json:"sent_at"`
	ExternalID string `json:"external_id"`
}

type CreateMessageBadResponse struct {
	Type   string   `json:"type"`
	Title  string   `json:"title"`
	Errors []string `json:"errors"`
}
