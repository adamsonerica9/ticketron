package kafkaproducer

import (
	"context"
	"encoding/json"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type KafkaProducer struct {
	KafkaWriter *kafka.Writer
	logger      *zap.Logger
	config      *config.Config
}

// todo passare sentry
func NewKafkaProducer(log *zap.Logger, conf *config.Config, kafkaServer []string, kafkaTopic, producerGroup string) *KafkaProducer {
	ec := &KafkaProducer{
		logger: log,
		config: conf,
	}
	ec.KafkaWriter = ec.getKafkaWriter(kafkaServer, kafkaTopic, producerGroup)

	return ec
}

func (ec *KafkaProducer) getKafkaWriter(kafkaServer []string, kafkaTopic, producerGroup string) *kafka.Writer {
	ec.logger.Sugar().Debug("kafka producer: Connecting to topic: ", kafkaTopic, " from kafka server ", kafkaServer)
	/* 	r := kafka.NewWriter(kafka.WriterConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
	}) */
	r := &kafka.Writer{
		Addr:     kafka.TCP(kafkaServer...),
		Topic:    kafkaTopic,
		Balancer: &kafka.LeastBytes{},
	}

	return r
}

func (ec *KafkaProducer) ProduceMessage(key string, message []byte, ctx context.Context) error {
	err := ec.KafkaWriter.WriteMessages(ctx, kafka.Message{
		Key:   []byte(key),
		Value: message,
	})
	if err != nil {
		ec.logger.Error("could not write message: ", zap.Error(err))
		return err
	}
	return nil
}

func (ec *KafkaProducer) CloseKafkaWriter() {
	err := ec.KafkaWriter.Close()
	if err != nil {
		ec.logger.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	ec.logger.Debug("producer closed")
}

func (ec *KafkaProducer) BuildMessageWithRetryMetaField(m []byte, originalTopic string) []byte {

	// 1 parsare in una mappa
	var message map[string]interface{}
	retryMeta := make(map[string]interface{})

	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &message)
	if err != nil {
		ec.logger.Debug("error unmarshalling message into map")
		return nil
	}
	if _, ok := message["retry_meta"]; !ok {
		ec.logger.Debug("we need to add retry_meta fields")
		retryMeta["original_topic"] = originalTopic
		message["retry_meta"] = retryMeta

		byteMessage, err := json.Marshal(message)
		if err != nil {
			ec.logger.Debug("error unmarshalling message into []byte")
			return nil
		}

		return byteMessage
	}
	return m
}
