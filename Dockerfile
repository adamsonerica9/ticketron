FROM golang:1.19-alpine as builder
RUN apk --no-cache add ca-certificates git
WORKDIR /build

# Fetch dependencies
COPY go.mod ./
RUN go mod download

# Build
COPY . ./
RUN CGO_ENABLED=0 go build -o ticketron

# Create final image
FROM alpine
WORKDIR /
COPY --from=builder /build/ticketron .
CMD ["./ticketron"]

