package kafkaconsumer

import (
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
)

type KafkaConsumer struct {
	KafkaReader *kafka.Reader
	logger      *zap.Logger
	config      *config.Config
}

// todo passare sentry
func NewKafkaConsumer(log *zap.Logger, conf *config.Config, kafkaServer []string, kafkaTopic, consumerGroup string, topicOffset int64) *KafkaConsumer {
	ec := &KafkaConsumer{
		logger: log,
		config: conf,
	}
	ec.KafkaReader = ec.getKafkaReader(kafkaServer, kafkaTopic, consumerGroup, topicOffset)
	//todo remove setting offset. We need to read it from kafka
	//ec.KafkaReader.SetOffset(ec.config.EventConsumer.TopicOffset)
	return ec
}

func (ec *KafkaConsumer) getKafkaReader(kafkaServer []string, kafkaTopic, consumerGroup string, topicOffset int64) *kafka.Reader {
	ec.logger.Sugar().Debug("Connecting to topic: ", kafkaTopic, ":", topicOffset, " from kafka server ", kafkaServer)
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
		GroupID: consumerGroup,
		MaxWait: 3 * time.Second,
		//PartitionWatchInterval: 5 * time.Second,
		//WatchPartitionChanges:  true,
		//StartOffset:            kafka.LastOffset,
		//ReadBackoffMax:         86400 * 10 * time.Second,
		//Logger:                 log.Default(),
		//OffsetOutOfRangeError:  true,
	})
	return r
}

func (ec *KafkaConsumer) CloseKafkaReader() {
	err := ec.KafkaReader.Close()
	if err != nil {
		ec.logger.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	ec.logger.Debug("Consumer closed")
}
