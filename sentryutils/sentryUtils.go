package sentryutils

import (
	"time"

	"github.com/getsentry/sentry-go"
	"opencitylabs.it/ticketron/config"
)

// general sentry configuration
// release ed evironment questi due valori prenderli da config:envronment si predende da env. la realse si prende da un file di config. leggerlo con viper . se non c'è la versione scrivere "no version"
func SentryInit(config *config.Config) error {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              config.Sentry.Dsn,
		TracesSampleRate: 1.0,
		Release:          "your-app-release",
		Environment:      "production",
		AttachStacktrace: true,
	})
	if err != nil {
		return err
	}
	defer sentry.Flush(2 * time.Second)
	return nil
}

// clone the main hub for local use into a goroutine
func InitLocalSentryhub() *sentry.Hub {
	localHub := sentry.CurrentHub().Clone()
	localHub.ConfigureScope(func(scope *sentry.Scope) {
		//todo change or remove it?
		scope.SetTag("secretTag", "go#2")
	})
	return localHub
}
