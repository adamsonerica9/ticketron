package kafkaMessageHandler

import (
	"encoding/json"
	"errors"

	"github.com/go-playground/validator/v10"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type IKafkaMessagesMessageHandler interface {
	GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.MessageMessageStruct, error)
}
type kafkaMessagesMessageHandler struct {
	logger            *zap.Logger
	validator         *validator.Validate
	structuredMessage kafkaMessagesStruct.MessageMessageStruct
}

func NewKafkaMessagesMessageHandler(log *zap.Logger) IKafkaMessagesMessageHandler {
	ev := &kafkaMessagesMessageHandler{
		logger:    log,
		validator: validator.New(),
	}

	return ev
}

func (ev *kafkaMessagesMessageHandler) GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.MessageMessageStruct, error) {
	ev.structuredMessage = kafkaMessagesStruct.MessageMessageStruct{}
	err := ev.validateEventMessage(m)
	if err != nil {
		return ev.structuredMessage, err
	}
	return ev.structuredMessage, nil
}

func (ev *kafkaMessagesMessageHandler) validateEventMessage(m kafka.Message) error {
	err := ev.validateJsonSintax([]byte(m.Value))
	if err != nil {
		return err
	}
	err = ev.validateMessageGrammar([]byte(m.Value))
	if err != nil {
		return err
	}
	return nil
}

func (ev *kafkaMessagesMessageHandler) validateJsonSintax(jsonM []byte) error {

	if json.Valid(jsonM) {
		return nil
	}
	return errors.New("MESSAGES-> bad json received from Kafka")
}

func (ev *kafkaMessagesMessageHandler) validateMessageGrammar(jsonM []byte) error {

	err := ev.jsonToStruct(jsonM)
	if err != nil {
		return err
	}
	err = ev.validator.Struct(ev.structuredMessage)
	if err != nil {
		ev.logger.Error("MESSAGES-> grammar not valid:", zap.Error(err))
		return err
	}
	return nil
}

func (ev *kafkaMessagesMessageHandler) jsonToStruct(jsonM []byte) error {
	err := json.Unmarshal(jsonM, &ev.structuredMessage)
	if err != nil {
		return err
	}
	return nil
}
