package kafkaMessageHandler

import (
	"encoding/json"
	"errors"

	"github.com/go-playground/validator/v10"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type IKafkaCzrmUpdatesMessageHandler interface {
	GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.CzrmUpdatesMessageStruct, error)
}
type kafkaCzrmUpdatesMessageHandler struct {
	logger            *zap.Logger
	validator         *validator.Validate
	structuredMessage kafkaMessagesStruct.CzrmUpdatesMessageStruct
}

func NewkafkaCzrmUpdateMessageHandler(log *zap.Logger) IKafkaCzrmUpdatesMessageHandler {
	ev := &kafkaCzrmUpdatesMessageHandler{
		logger:    log,
		validator: validator.New(),
	}
	return ev
}

// magari qua posso definire il tipo di validatore e ritorna sia error che la struttura
func (ev *kafkaCzrmUpdatesMessageHandler) GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.CzrmUpdatesMessageStruct, error) {
	//cleaning the struct
	ev.structuredMessage = kafkaMessagesStruct.CzrmUpdatesMessageStruct{}
	err := ev.validateEventMessage(m)
	if err != nil {
		return ev.structuredMessage, err
	}
	return ev.structuredMessage, nil
}
func (ev *kafkaCzrmUpdatesMessageHandler) validateEventMessage(m kafka.Message) error {
	err := ev.validateJsonSintax([]byte(m.Value))
	if err != nil {
		return err
	}
	err = ev.validateMessageGrammar([]byte(m.Value))
	if err != nil {
		return err
	}
	return nil
}

func (ev *kafkaCzrmUpdatesMessageHandler) validateJsonSintax(jsonM []byte) error {

	if json.Valid(jsonM) {
		return nil
	}
	return errors.New("CZRM-UPDATES-> bad json received from Kafka")
}

func (ev *kafkaCzrmUpdatesMessageHandler) validateMessageGrammar(jsonM []byte) error {

	err := ev.jsonToStruct(jsonM)
	if err != nil {
		return err
	}
	err = ev.validator.Struct(ev.structuredMessage)
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-> json not valid:", zap.Error(err))
		return err
	}
	return nil
}

func (ev *kafkaCzrmUpdatesMessageHandler) jsonToStruct(jsonM []byte) error {
	err := json.Unmarshal(jsonM, &ev.structuredMessage)
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-> error Unmarshalling into ApplicationMessageStruct:", zap.Error(err))
		return err
	}
	return nil
}
