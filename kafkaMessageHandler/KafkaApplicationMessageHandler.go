package kafkaMessageHandler

import (
	"encoding/json"
	"errors"

	"github.com/go-playground/validator/v10"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type KafkaApplicationMessageHandler interface {
	GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.ApplicationMessageStruct, error)
}
type kafkaApplicationMessageHandler struct {
	logger            *zap.Logger
	validator         *validator.Validate
	structuredMessage kafkaMessagesStruct.ApplicationMessageStruct
}

func NewKafkaApplicationMessageHandler(log *zap.Logger) KafkaApplicationMessageHandler {
	ev := &kafkaApplicationMessageHandler{
		logger:    log,
		validator: validator.New(),
	}
	return ev
}

func (ev *kafkaApplicationMessageHandler) GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.ApplicationMessageStruct, error) {
	//cleaning the struct
	ev.structuredMessage = kafkaMessagesStruct.ApplicationMessageStruct{}
	err := ev.validateEventMessage(m)
	if err != nil {
		return ev.structuredMessage, err
	}
	return ev.structuredMessage, nil
}
func (ev *kafkaApplicationMessageHandler) validateEventMessage(m kafka.Message) error {
	err := ev.validateJsonSintax([]byte(m.Value))
	if err != nil {
		return err
	}
	err = ev.validateMessageGrammar([]byte(m.Value))
	if err != nil {
		return err
	}
	return nil
}

func (ev *kafkaApplicationMessageHandler) validateJsonSintax(jsonM []byte) error {

	if json.Valid(jsonM) {
		return nil
	}
	return errors.New("APPLICATIONS-> bad json received from Kafka")
}

func (ev *kafkaApplicationMessageHandler) validateMessageGrammar(jsonM []byte) error {

	err := ev.jsonToStruct(jsonM)
	if err != nil {
		return err
	}
	err = ev.validator.Struct(ev.structuredMessage)
	if err != nil {
		ev.logger.Error("APPLICATIONS-> missing required fields:", zap.Error(err))
		return err
	}
	return nil
}

func (ev *kafkaApplicationMessageHandler) jsonToStruct(jsonM []byte) error {
	err := json.Unmarshal(jsonM, &ev.structuredMessage)
	if err != nil {
		ev.logger.Error("APPLICATIONS-> error Unmarshalling into ApplicationMessageStruct:", zap.Error(err))
		return err
	}
	return nil
}
