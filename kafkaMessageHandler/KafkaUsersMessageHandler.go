package kafkaMessageHandler

import (
	"encoding/json"
	"errors"

	"github.com/go-playground/validator/v10"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type IKafkaUsersMessageHandler interface {
	GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.UsersMessageStruct, error)
}
type kafkaUsersMessageHandler struct {
	logger            *zap.Logger
	validator         *validator.Validate
	structuredMessage kafkaMessagesStruct.UsersMessageStruct
}

func NewKafkaUsersMessageHandler(log *zap.Logger) IKafkaUsersMessageHandler {
	ev := &kafkaUsersMessageHandler{
		logger:    log,
		validator: validator.New(),
	}

	return ev
}

func (ev *kafkaUsersMessageHandler) GetStructuredMessage(m kafka.Message) (kafkaMessagesStruct.UsersMessageStruct, error) {
	//cleaning the struct
	ev.structuredMessage = kafkaMessagesStruct.UsersMessageStruct{}
	err := ev.validateEventMessage(m)
	if err != nil {
		return ev.structuredMessage, err
	}
	return ev.structuredMessage, nil
}

func (ev *kafkaUsersMessageHandler) validateEventMessage(m kafka.Message) error {
	err := ev.validateJsonSintax([]byte(m.Value))
	if err != nil {
		return err
	}
	err = ev.validateMessageGrammar([]byte(m.Value))
	if err != nil {
		return err
	}
	return nil
}

func (ev *kafkaUsersMessageHandler) validateJsonSintax(jsonM []byte) error {

	if json.Valid(jsonM) {
		return nil
	}
	return errors.New("USERS-> bad json received from Kafka")
}

func (ev *kafkaUsersMessageHandler) validateMessageGrammar(jsonM []byte) error {

	err := ev.jsonToStruct(jsonM)
	if err != nil {
		return err
	}
	err = ev.validator.Struct(ev.structuredMessage)
	if err != nil {
		ev.logger.Error("USERS-> grammar not valid:", zap.Error(err))
		return err
	}
	return nil
}

func (ev *kafkaUsersMessageHandler) jsonToStruct(jsonM []byte) error {
	err := json.Unmarshal(jsonM, &ev.structuredMessage)
	if err != nil {
		return err
	}
	return nil
}
