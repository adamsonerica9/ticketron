package kafkaMessagesStruct

type CzrmUpdatesMessageStruct struct {
	EventId   string `json:"event_id"`
	Version   string `json:"version"`
	UpdatedAt string `json:"updated_at"`
	Entity    string `json:"entity"`
	ID        string `json:"id"`
}
