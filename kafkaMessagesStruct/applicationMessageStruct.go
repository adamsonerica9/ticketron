package kafkaMessagesStruct

type ApplicationMessageStruct struct {
	Data struct {
		AddressAddressCountry                 string      `json:"address.address.country"`
		AddressAddressCountryCode             string      `json:"address.address.country_code"`
		AddressAddressCounty                  string      `json:"address.address.county"`
		AddressAddressPostcode                string      `json:"address.address.postcode"`
		AddressAddressRoad                    string      `json:"address.address.road"`
		AddressAddressTown                    string      `json:"address.address.town"`
		AddressLat                            interface{} `json:"address.lat"`
		AddressLon                            interface{} `json:"address.lon"`
		ApplicantDataCompletenameDataName     string      `json:"applicant.data.completename.data.name"`
		ApplicantDataCompletenameDataSurname  string      `json:"applicant.data.completename.data.surname"`
		ApplicantDataEmailAddress             string      `json:"applicant.data.email_address"`
		ApplicantDataFiscalCodeDataFiscalCode string      `json:"applicant.data.fiscal_code.data.fiscal_code"`
		ApplicantDataPhoneNumber              string      `json:"applicant.data.phone_number"`
		Details                               string      `json:"details"`
		Subject                               string      `json:"subject"`
		MicromacrocategoryLabel               string      `json:"micromacrocategory.label"`
		TypeLabel                             string      `json:"type.label"`
		TypeValue                             string      `json:"type.value"`
		PdfLink                               string      `json:"meta.pdf_link"`
		Images                                []Image     `json:"images"`
		Docs                                  []Doc       `json:"docs"`
	} `json:"data"`
	Attachments     []ApplicationAttachments `json:"attachments"`
	EventId         string                   `json:"event_id"`
	ExternalId      string                   `json:"external_id"`
	CreatedAt       string                   `json:"created_at"`
	GeographicAreas []GeographicAreas        `json:"geographic_areas"`
	ServiceID       string                   `json:"service_id"`
	Id              string                   `json:"id"`
	TenantID        string                   `json:"tenant_id"`
	User            string                   `json:"user"`
	UserName        string                   `json:"user_name"`
	Status          string                   `json:"status"`
	SubmittedAt     string                   `json:"submitted_at"`
}

type GeographicAreas struct {
	Name string `json:"name"`
}

type Image struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
	ExternalId       string `json:"external_id"`
}

type Doc struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
	ExternalId       string `json:"external_id"`
}

type ApplicationAttachments struct {
	ID               string      `json:"id"`
	Name             string      `json:"name"`
	URL              string      `json:"url"`
	OriginalName     string      `json:"originalName"`
	Description      string      `json:"description"`
	CreatedAt        string      `json:"created_at"`
	ProtocolRequired bool        `json:"protocol_required"`
	ProtocolNumber   interface{} `json:"protocol_number"`
	ExternalID       string      `json:"external_id"`
}
